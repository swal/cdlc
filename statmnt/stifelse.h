/*
 * $Log$
 *
 */

/*$Id$*/

#ifndef ST_IF_ELSE_H
#define ST_IF_ELSE_H

#include "../statmnt/statmnt.h"

class StIfElse : public Statement
{
	protected:
		Expression * myCon;
		Statement *myIf, *myElse;
	public:
		StIfElse();
		StIfElse(Object * con, Object * ifstmt, Object * elsestmt);
		virtual void logic(AVariable * cond);
	
	DEFAULT_CONCRETE_METHODS(StIfElse,Statement);
};

#endif
