/*
 * $Log$
 *
 */

/*$Id$*/

#ifndef ST_COMP_H
#define ST_COMP_H

#include "../statmnt/statmnt.h"
#include "../klassen/avariabl.h"

class StComp : public Statement
{
    protected:
        List * myList;
	public:
        StComp();
        StComp(Object * stmtlist);
        virtual void logic(AVariable * cond);
        
	DEFAULT_CONCRETE_METHODS(StComp,Statement);
};

#endif
