/*
 * $Log$
 *
 */

/*$Id$*/

#ifndef ST_FOR_H
#define ST_FOR_H

#include "../statmnt/statmnt.h"
#include "../klassen/avariabl.h"
#include "../klassen/aloopvar.h"

class StFor : public Statement
{
	protected:
		ALoopVarList * loopVar;
		Statement * myStat;
		
	public:
		StFor();
		StFor(Object * loop, Object * stmt);
		
		virtual void logic(AVariable * cond);

	DEFAULT_CONCRETE_METHODS(StFor,Statement);
};

#endif
