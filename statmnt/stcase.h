/*
 * $Log$
 *
 */

/*$Id$*/

#ifndef ST_CASE_H
#define ST_CASE_H

#include "../statmnt/statmnt.h"
#include "../klassen/avariabl.h"

class StCase : public Statement
{
    protected:
        Expression * myCon;
        List * myList;
        Statement * myDefault;
        
	public:
	    StCase();
	    StCase(Object * con, Object * stmtlist, Object * defstmt);
        virtual void logic(AVariable * cond);

	DEFAULT_CONCRETE_METHODS(StCase,Statement);
};

#endif
