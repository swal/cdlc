#include "stassign.h"
#include "../klassen/atbool.h"
#include "../klassen/atrecord.h"
#include "../klassen/atunion.h"
#include "../klassen/atrange.h"
#include "../library/bstring.h"
#include "../library/list.h"
#include "../library/listobj.h"
#include "../express/excell.h"
#include "../grammar/parser.h"


#define SUPERCLASS Statement
INITIALIZE_CONCRETE_CLASS(StAssign, SUPERCLASS);

StAssign::StAssign()
{
    DEBUG_enter("StAssign::StAssign()")
    myL=NULL;
    myR=NULL;
    DEBUG_exit("StAssign::StAssign()")
}

StAssign::StAssign(Object * l, Object * r)
{
    AType * tl, * tr;

    DEBUG_enter("StAssign::StAssign(Object * l, Object * r)")
    
    myL=Pair::cast(l);
    /*
     *  l is Pair (AVariable oder ExCell,List) 
     *  List is: subtype.reference.opt, is a (probably empty) List
     */
    
    if ( (!myL->getLeft()->isKindOf(AVariable::ClassName)) &&
         (!myL->getLeft()->isKindOf(ExCell   ::ClassName)) )
    { error("StAssign: left of pair l.value isn't a variable or an ExCell"); }

    if (!myL->getRight()->isKindOf(List::ClassName))
    { error("StAssign: right of pair l.value isn't a list"); }

    tl=Expression::cast(myL->getLeft())->getType();
    
    myR=Expression::cast(r);

    tr=myR->getType();
    
    if ( 
        (tl->isKindOf(ATypeRecord::ClassName)) ||
        (tl->isKindOf(ATypeUnion::ClassName))
       )

    { }
    else
    {
        if ( !tl->isKindOf(tr->className()) )
        {
            printf("%s\n",BString::cast(tl->className())->content());
            semerror(NULL,"incompatible types in assignment");
        }
    }
    DEBUG_exit("StAssign::StAssign(Object * l, Object * r)")
}


/*
 *  this function, called from StAssign::locig() and recursiv
 *  is necessary to handle record structures
 */

void StAssign::assign( AVariable * l, AVariable * r, AVariable * cond )
{
    BString *ls;
    int i;

    if (l->getType()->isKindOf(ATypeRecord::ClassName))
    {
        i=1;
        while ( NULL!=ATypeRecord::cast(l->getType())->getNameAt(i) )
        {
            assign(
                l->getSubName(ATypeRecord::cast(l->getType())->getNameAt(i)),
                r->getSubName(ATypeRecord::cast(r->getType())->getNameAt(i)),
                cond
            );
            i++;
        }
    }
    else
    {
        if (l->getType()->isKindOf(ATypeRange::ClassName))
        {
            if (!r->getType()->isKindOf(ATypeRange::ClassName))
            {
                semerror(NULL,"incompatible types in assignment");
            }
            else
            {
                if (  (ATypeRange::cast(r->getType())->getUpper()->value()
                     > ATypeRange::cast(l->getType())->getUpper()->value() )
                       ||
                      (ATypeRange::cast(r->getType())->getLower()->value()
                     < ATypeRange::cast(l->getType())->getLower()->value() )
                    )
                 { warning("in assignment: range type gets truncated"); }
            }
        }
    
        if ( NULL==(cond->asConstant()) )     // can't decide to assign or not
        {
            ls=l->getName();
            l->newInstance();
    
        // schade, dass LOG/iC eine Laengenbegrenzung bei der Eingabezeile besitzt
            fprintf(fout,"; assignment\n*BOOLEAN_EQUATIONS\n %s =\n\t  (  %s & (%s) )\n\t+ ( /%s & (%s) );\n\n",
                l   ->getName()->content(),
                cond->getName()->content(),
                r   ->getName( l->getType() )->content(),
                cond->getName()->content(),
                ls  ->content()
            );
        }
        else
        {
            /* _do_ assign */
    
            if (NULL!=r->asConstant())
            {
                l->setConstant(r->asConstant());
            }
            else
            {
                l->newInstance();
                
                fprintf(fout,"; assignment\n*BOOLEAN_EQUATIONS\n %s = %s ;\n\n",
                    l   ->getName()->content(),
                    r   ->getName( l->getType() )->content()
                );
            }
        }
    }
}


void StAssign::logic(AVariable * cond)
{
    AVariable *r, *l;
    AConstant * constCond;
    
    DEBUG_enter("void StAssign::logic(AVariable * cond)")

    DEBUG_ptr( cond )
    DEBUG_ptr( cond->asConstant() )
    
    constCond=cond->asConstant();

    if (NULL!=constCond)    // can decide if to assign or not
    {
        if (0==AConBool::cast(constCond)->value())
        {
            /*
             *   assignment never done
             *   don't mind any further
             */
            DEBUG_exit("void StAssign::logic(AVariable * cond)")
            return;
        }
    }

    r=myR->logic();
    
    if (myL->getLeft()->isKindOf(ExCell::ClassName))  /* writing to the cell */
    {   
        l=AVariable::cast(identifier->getValueAt( new BString("_OUT")));
    }
    else
    {
        if(myL->getLeft()->isKindOf(AVariable::ClassName)) /* writing to a variable */
        {
            l=AVariable::cast(myL->getLeft());
        }
        else
        { fatalError("void StAssign::logic(AVariable * cond): left side unknown"); }
    }

// got a Variable, now, handle Subtype-Ref-List in myL->getRight()
    for ( Iterator i(List::cast(myL->getRight())) ; i ; i++ )
    {
        l=l->getSubName( BString::cast( ListObject::cast(i)->getObject() ) );
    }

// got the correct variable now in l

    assign(l,r,cond);

    DEBUG_exit("void StAssign::logic(AVariable * cond)")
}
