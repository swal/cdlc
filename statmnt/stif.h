/*
 * $Log$
 *
 */

/*$Id$*/

#ifndef ST_IF_H
#define ST_IF_H

#include "../statmnt/statmnt.h"
#include "../klassen/avariabl.h"

class StIf : public Statement
{
    protected:
        Expression* myCon;
        Statement* myStat;
        
	public:
	    StIf();
	    StIf(Object * con, Object * stmt);
        virtual void logic(AVariable * cond);

	DEFAULT_CONCRETE_METHODS(StIf,Statement);
};

#endif
