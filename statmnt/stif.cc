#include "stif.h"
#include "../express/exand.h"
#include "../klassen/acbool.h"

#define SUPERCLASS Statement
INITIALIZE_CONCRETE_CLASS(StIf, SUPERCLASS);

StIf::StIf()
{
    DEBUG_enter("StIf::StIf()")
    myCon=NULL;
    myStat=NULL;
    DEBUG_exit("StIf::StIf()")
}

StIf::StIf(Object * con, Object * stmt)
{
    DEBUG_enter("StIf::StIf(Object * con, Object * stmt)")
    myCon=Expression::cast(con);
    myStat=Statement::cast(stmt);
    DEBUG_exit("StIf::StIf(Object * con, Object * stmt)")
}

void StIf::logic(AVariable * cond)
{
    AVariable * con;
    AVariable * vIf;
    AConstant * cIf;
    
    DEBUG_enter("void StIf::logic(AVariable * cond)")
    con=myCon->logic();
    vIf=(new ExAnd(con,cond))->logic();
    cIf=vIf->asConstant();
    
    if ( NULL!=cIf )
    {
        if ( AConBool::cast(cIf)->value() )
        { myStat->logic(vIf); }
    }
    else
    {
        myStat->logic(vIf);
    }
    DEBUG_exit("void StIf::logic(AVariable * cond)")
}
