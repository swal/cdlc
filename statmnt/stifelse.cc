#include "stifelse.h"
#include "../express/exand.h"
#include "../express/exnot.h"
#include "../klassen/acbool.h"

#define SUPERCLASS Statement
INITIALIZE_CONCRETE_CLASS(StIfElse, SUPERCLASS);

StIfElse::StIfElse()
{
    DEBUG_enter("StIfElse::StIfElse()")
    myCon=NULL;
    myIf=NULL;
    myElse=NULL;
    DEBUG_exit("StIfElse::StIfElse()")
}

StIfElse::StIfElse(Object * con, Object * ifstmt, Object * elsestmt)
{
    DEBUG_enter("StIfElse::StIfElse(Object * con, Object * ifstmt, Object * elsestmt)")
    myCon=Expression::cast(con);
    myIf=Statement::cast(ifstmt);
    myElse=Statement::cast(elsestmt);
    DEBUG_exit("StIfElse::StIfElse(Object * con, Object * ifstmt, Object * elsestmt)")
}

void StIfElse::logic(AVariable * cond)
{
    AVariable *con;
    Expression *cIf, *cElse;
    
    DEBUG_enter("void StIfElse::logic(AVariable * cond)")

    con=myCon->logic();
    
    cIf=new ExAnd( cond ,con);

    if (NULL!=cIf->asConstant())
    {
        if (AConBool::cast(cIf->asConstant())->value())
        {
            myIf->logic( cIf->logic() );
        }
    }
    else
    { myIf->logic( cIf->logic() ); }

    cElse=new ExAnd( cond , (new ExNot(con))->logic() ) ;

    if (NULL!=cElse->asConstant())
    {
        if (AConBool::cast(cElse->asConstant())->value())
        {
            myElse->logic( cElse->logic() );
        }
    }
    else
    { myElse->logic( cElse->logic() ); }

    DEBUG_exit("void StIfElse::logic(AVariable * cond)")
}

