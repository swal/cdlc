/*
 * $Log$
 *
 */

/*$Id$*/

#ifndef STATEMENT_H
#define STATEMENT_H

#include "../library/object.h"
#include "../express/express.h"
#include "../klassen/avariabl.h"
#include "../klassen/syntaxel.h"

class Statement : public SyntaxElement
{
	public:
		virtual void logic(AVariable * cond)=0;

	DEFAULT_ABSTRACT_METHODS(Statement,SyntaxElement);
};

#endif
