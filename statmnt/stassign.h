/*
 * $Log$
 *
 */

/*$Id$*/

#ifndef ST_ASSIGN_H
#define ST_ASSIGN_H

#include "../statmnt/statmnt.h"
#include "../klassen/avariabl.h"
#include "../express/express.h"
#include "../library/pair.h"

class StAssign : public Statement
{
	protected:
		Pair        *myL;
		Expression  *myR;
		virtual void assign( AVariable * l, 
							 AVariable * r, 
							 AVariable * cond 
						   );
		
	public:
		StAssign();
		StAssign(Object * l, Object * r); 
			/* l must be Pair*(AVariable*,List*) a Variable with ist subNames */

		virtual void logic(AVariable * cond);

	DEFAULT_CONCRETE_METHODS(StAssign,Statement);
};

#endif
