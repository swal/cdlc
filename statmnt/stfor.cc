#include "stfor.h"

#define SUPERCLASS Statement
INITIALIZE_CONCRETE_CLASS(StFor, SUPERCLASS);

StFor::StFor()
{
    DEBUG_enter("StFor::StFor()")
    loopVar=NULL;   /* ALoopVarList* */
    myStat=NULL;    /* Statement*    */
    DEBUG_exit("StFor::StFor()")
}

StFor::StFor(Object * loop, Object * stmt)
{
    DEBUG_enter("StFor::StFor(Object * loop, Object * stmt)")
    if (!loop->isKindOf(List::ClassName))
    { error("the loop-var-list of for() isnot a list"); }
    if (!stmt->isKindOf(Statement::ClassName))
    { error("the argument of for() isnot a statement"); }
    
    loopVar=ALoopVarList::cast(loop);
    myStat=Statement::cast(stmt);

    DEBUG_exit("StFor::StFor(Object * loop, Object * stmt)")
}

void StFor::logic(AVariable * cond)
{
    int i;
    DEBUG_enter("void StFor::logic(AVariable * cond)")

    i=1;
    while( 0!=loopVar->setToIteration(i) )
    {
        myStat->logic(cond);
        i++;
    }
    
    DEBUG_exit("void StFor::logic(AVariable * cond)")
}
