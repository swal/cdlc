#include "stcomp.h"

#define SUPERCLASS Statement
INITIALIZE_CONCRETE_CLASS(StComp, SUPERCLASS);

StComp::StComp()
{
    DEBUG_enter("StComp::StComp()")
    myList=NULL;
    DEBUG_exit("StComp::StComp()")
}

StComp::StComp(Object * stmtlist)
{
    DEBUG_enter("StComp::StComp(Object * stmtlist)")
    myList=List::cast(stmtlist);
    DEBUG_exit("StComp::StComp(Object * stmtlist)")
}

void StComp::logic(AVariable * cond)
{
    DEBUG_enter("void StComp::logic(AVariable * cond)")
    for(Iterator i(myList);i;i++)
    {
        Statement::cast(ListObject::cast(i)->getObject())->logic(cond);
    }
    DEBUG_exit("void StComp::logic(AVariable * cond)")
}
