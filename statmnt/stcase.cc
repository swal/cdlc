#include "stcase.h"

#include "../express/exnot.h"
#include "../express/exin.h"
#include "../express/exand.h"
#include "../library/pair.h"
#include "../library/list.h"
#include "../library/listobj.h"
#include "../klassen/avariabl.h"
#include "../klassen/agroup.h"


#define SUPERCLASS Statement
INITIALIZE_CONCRETE_CLASS(StCase, SUPERCLASS);

StCase::StCase()
{
    DEBUG_enter("StCase::StCase()")
    myCon=NULL;
    myList=NULL;
    DEBUG_exit("StCase::StCase()")
}

StCase::StCase(Object * con, Object * stmtlist, Object * defstmt)
{
    DEBUG_enter("StCase::StCase(Object * con, Object * stmtlist, Object * defstmt)")
    myCon=Expression::cast(con);         /* expression */
    myList=List::cast(stmtlist);   /* Liste mit Paaren(group,statement) */    
    myDefault=Statement::cast(defstmt); /* statement oder NULL */
    DEBUG_exit("StCase::StCase(Object * con, Object * stmtlist, Object * defstmt)")
}

void StCase::logic(AVariable * cond)
{
    AVariable * conVar;
    ListObject * i;

    DEBUG_enter("void StCase::logic(AVariable * cond)")

/*
 * es wird einfach die Liste rueckwaerts durchgegangen. Die erste gueltige
 * Zuweisung von vorne ist somit wirksam
 */

/* noch hinter der Liste steht evtl. der otherwise */ 


    if (NULL!=myDefault)
    {
        myDefault->logic(cond);
    }

    conVar = myCon->logic();

    i=ListObject::cast(myList->last());
    
    while(NULL!=i)
    {
        Pair *p;
        AGroup *g;
        AVariable * enable;
        AVariable * enableThis;

        DEBUG_msg("case-branch")

        p = Pair::cast( ListObject::cast(i)->getObject() );
        g = AGroup::cast( p->getLeft() );

        enable=(new ExIn(conVar,
                        Pair::cast(
                            ListObject::cast(i)->getObject()
                        )->getLeft()
                    )
                )->logic();

        enableThis=(new ExAnd(enable,cond))->logic();

        Statement::cast(
            Pair::cast(ListObject::cast(i)->getObject())->getRight()
        )->logic(enable);

        i=ListObject::cast(myList->previous(i));
        
    }
    
 
    DEBUG_exit("void StCase::logic(AVariable * cond)")    
}
