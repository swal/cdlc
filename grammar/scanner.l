%{

/* file: scanner.l,
 * becomes yylex.c (yy.lex.c under UNIX) after processing by FLEX  
 */

/*
 * $Log: scanner.l%v $
 * Revision 1.1  1995/03/16  17:28:26  waldsch
 * Initial revision
 *
 * Revision 1.4  1994/05/28  18:43:31  waldsch
 * jetzt zur neuen Grammatik von Yacc.
 * einigermassen vollstaendig.
 * Verbesserte Fehler-Positions-Erkennung
 * durch inputfilename
 * Erkennen von Praeprozessor line-file-
 * Angaben
 *
 * Revision 1.3  1994/05/24  21:42:55  waldsch
 * jetzt praeprozessor line anweisung und
 * inputfilename-Variable
 *
 * Revision 1.2  1994/05/21  18:10:42  waldsch
 * besser aufgeraeumt, scanner.h neu
 *
 */
 
char scannerRCSID [] = "$Id: scanner.l%v 1.1 1995/03/16 17:28:26 waldsch Exp waldsch $";

#include <stdio.h>
#include <stdlib.h> /* for calloc */
#include <string.h> /* for strlen strcpy */

#include "scanner.h" 
#include "types.h"
#include "../main/debug.h"
#include "../klassen/token.h"


/* 
 * now include the YACC generated definitions based on parser input 
 */  

#ifdef MSDOS
#include "y_tab.h"     /* for MS-DOS Systems */
#else
#include "y.tab.h"   /* for UNIX-like Systems */
#endif


int inputlinenumber;
char inputfilename[255];

#define WHITE_RETURN(x)    /* do nothing */
#define KEYWORD_RETURN(x)  RETURN_VAL(x)            /* Keyword */
#define ASCIIOP_RETURN(x)  RETURN_VAL((int)*yytext) /* a single character operator */


#define RETURN_VAL(x) \
		yylval=new Token(yytext,inputlinenumber,inputfilename); \
		DEBUG_code( printf("`%s'",yytext); ) \
		return(x);


%}

%START INCOMMENT

comment         "/*"([^"*"]*)"*"

h_tab           [\011]
form_feed       [\014]
v_tab           [\013]
c_return        [\015]

h_white         [ ]|{h_tab}

pp_line         #{h_white}*[0-9]+{h_white}*\"[^\"]*\"[^\n]*\n 


%%

{h_white}           { WHITE_RETURN(' ');}

({v_tab}|{c_return}|{form_feed}|"\n")  { 
	VERBOSE( printf("[%u]",inputlinenumber); )
	inputlinenumber++; 
	WHITE_RETURN(' ');
	}


"/*"                    { BEGIN INCOMMENT; }
<INCOMMENT>("*/")       { BEGIN 0; }
<INCOMMENT>("\n")       {
				VERBOSE( printf("[%u]",inputlinenumber); )
				inputlinenumber++; 
			}
<INCOMMENT>("*"|([^"*""\n"]+))  { /* still in Comment */ }


{pp_line}           { DEBUG_msg(line found); 
					  sscanf(yytext,"# %u \"%s ",
									   &inputlinenumber,
									   inputfilename); 
					  inputfilename[strlen(inputfilename)-1]='\0';
					  VERBOSE( printf("\"%s\"",inputfilename); )
					  WHITE_RETURN(' '); 
					}


"ALL"|all                   {KEYWORD_RETURN(ALL);}   
"AND"|and                   {KEYWORD_RETURN(AND);}   
"AUTOMATON"|automaton       {KEYWORD_RETURN(AUTOMATON);}
"BEGIN"|begin               {KEYWORD_RETURN(BBEGIN);}
"BOOLEAN"|boolean           {KEYWORD_RETURN(BOOLEAN);}
"BORDER"|border             {KEYWORD_RETURN(BORDER);}
"CASE"|case                 {KEYWORD_RETURN(CASE);}
"CELLULAR"|cellular         {KEYWORD_RETURN(CELLULAR);}
"COLOR"|color               {KEYWORD_RETURN(COLOR);}
"COLOUR"|colour             {KEYWORD_RETURN(COLOR);}
"CONST"|const               {KEYWORD_RETURN(CONST);}
"DIV"|div                   {KEYWORD_RETURN(DIV);}
"DO"|do                     {KEYWORD_RETURN(DO);}
"ELEMENT"|element           {KEYWORD_RETURN(ELEMENT);}
"ELSE"|else                 {KEYWORD_RETURN(ELSE);}
"END"|end                   {KEYWORD_RETURN(END);}
"FALSE"|false               {KEYWORD_RETURN(FALSE);}
"FLOAT"|float               {KEYWORD_RETURN(FLOAT);}
"FOR"|for                   {KEYWORD_RETURN(FOR);}
"GROUP"|group               {KEYWORD_RETURN(GROUP);}
"IF"|if                     {KEYWORD_RETURN(IF);}
"IN"|in                     {KEYWORD_RETURN(IN);}
"INTEGER"|integer           {KEYWORD_RETURN(INTEGER);}
"MOD"|mod                   {KEYWORD_RETURN(MOD);}
"NOT"|not                   {KEYWORD_RETURN(NOT);}
"NUM"|num                   {KEYWORD_RETURN(NUMBER);}
"OF"|of                     {KEYWORD_RETURN(OF);}
"ONE"|one                   {KEYWORD_RETURN(ONE);}
"OR"|or                     {KEYWORD_RETURN(OR);}   
"OTHERWISE"|otherwise       {KEYWORD_RETURN(OTHERWISE);}
"RECORD"|record             {KEYWORD_RETURN(RECORD);}
"RULE"|rule                 {KEYWORD_RETURN(RULE);}
"THEN"|then                 {KEYWORD_RETURN(THEN);}
"TRUE"|true                 {KEYWORD_RETURN(TRUE);}
"TYPE"|type                 {KEYWORD_RETURN(TYPE);}
"UNION"|union               {KEYWORD_RETURN(UNION);}
"VALUE"|value               {KEYWORD_RETURN(VALUE);}
"VAR"|var                   {KEYWORD_RETURN(VAR);}
"XOR"|xor                   {KEYWORD_RETURN(XOR);}   

".."                {KEYWORD_RETURN(POINTPOINT);}
":="                {KEYWORD_RETURN(ASSIGN);}
"<="                {KEYWORD_RETURN(LE);}
">="                {KEYWORD_RETURN(GE);}
"!="                {KEYWORD_RETURN(NE);}
"<>"                {KEYWORD_RETURN(NE);}


"="                 {ASCIIOP_RETURN(OP);}
"%"                 {ASCIIOP_RETURN(OP);}
"~"                 {ASCIIOP_RETURN(OP);}
";"                 {ASCIIOP_RETURN(OP);}
","                 {ASCIIOP_RETURN(OP);}
"("                 {ASCIIOP_RETURN(OP);}
")"                 {ASCIIOP_RETURN(OP);}
":"                 {ASCIIOP_RETURN(OP);}
"<"                 {ASCIIOP_RETURN(OP);}
">"                 {ASCIIOP_RETURN(OP);}
"["                 {ASCIIOP_RETURN(OP);}
"]"                 {ASCIIOP_RETURN(OP);}
"."                 {ASCIIOP_RETURN(OP);}
"*"                 {ASCIIOP_RETURN(OP);}
"/"                 {ASCIIOP_RETURN(OP);}
"+"                 {ASCIIOP_RETURN(OP);}
"-"                 {ASCIIOP_RETURN(OP);}
"&"                 {ASCIIOP_RETURN(OP);}
"{"                 {ASCIIOP_RETURN(OP);}
"}"                 {ASCIIOP_RETURN(OP);}
"'"                 {ASCIIOP_RETURN(OP);}


[0-9]+\.([0-9]+)[eE]([+-]?)([0-9]+)     {KEYWORD_RETURN(FNUM);} 
[0-9]+[eE]([+-]?)([0-9]+)               {KEYWORD_RETURN(FNUM);} 
[0-9]+\.[0-9]+                          {KEYWORD_RETURN(FNUM);}
[0-9]+                                  {KEYWORD_RETURN(NUM);}
[a-zA-Z_]([a-zA-Z0-9_]*)                 {KEYWORD_RETURN(IDENTIFIER);} 

.                                       {KEYWORD_RETURN(SCANERROR);}

%%

/* Hier koennen c-Funktionen stehen, die im vorigen Teil verwendet werden */

