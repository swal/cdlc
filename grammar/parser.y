%{ 

/* file: parser.y,
 * will become y.tab.c after processing by YACC
 * ( or parser.tab.c or y_tab.c or parser_t.c )
 */

/*
 * $Log: parser.y%v $
 * Revision 1.1  1995/03/16  17:28:33  waldsch
 * Initial revision
 *
 * Revision 1.5  1994/05/28  18:41:00  waldsch
 * kann jetzt fast vollstaendige neue Grammatik
 * parsen. Hat aber noch grosse Loecher, z.B.
 * bei float-Konstanten
 *
 * Revision 1.4  1994/05/24  21:45:10  waldsch
 * neu: yyerror mit filename. Aussgabe nach
 * UNIX-Konvention file:line:message
 *
 * Revision 1.3  1994/05/21  18:11:22  waldsch
 * debug error und parser.h neu
 *
 * Revision 1.2  1994/05/01  17:38:20  waldsch
 * now with version and log by RCS
 *
 */

char parserRCSID[] = "$Id: parser.y%v 1.1 1995/03/16 17:28:33 waldsch Exp waldsch $";


#ifdef  DEBUG
#define YYDEBUG 1
#endif

#ifdef  YYDEBUG
#undef  YYDEBUG
#define YYDEBUG 1
#define YYERROR_VERBOSE 
#endif


#include <stdio.h>
#include <stdlib.h>

#include "types.h"
#include "scanner.h"
#include "parser.h"

#include "../main/error.h"
#include "../main/debug.h"

#include "../library/dictnary.h"
#include "../library/bstring.h"
/* #include "../library/set.h" */
#include "../library/integer.h"
#include "../library/listobj.h"
#include "../library/list.h"
#include "../library/pair.h"

#include "../klassen/atype.h"
#include "../klassen/atbool.h"
#include "../klassen/atenum.h"
#include "../klassen/atrecord.h"
#include "../klassen/atunion.h"
#include "../klassen/atrange.h"
#include "../klassen/atint.h"
#include "../klassen/aconstnt.h"
#include "../klassen/acint.h"
#include "../klassen/acbool.h"
#include "../klassen/acenum.h"
#include "../klassen/acrecord.h"
#include "../klassen/acunion.h"
#include "../klassen/avariabl.h"
#include "../klassen/agroup.h"
#include "../klassen/aloopvar.h"
#include "../klassen/token.h"

#include "../statmnt/stcomp.h"
#include "../statmnt/stassign.h"
#include "../statmnt/stif.h"
#include "../statmnt/stifelse.h"
#include "../statmnt/stcase.h"
#include "../statmnt/stfor.h"

#include "../express/express.h"
#include "../express/exor.h"
#include "../express/excast.h"
#include "../express/exxor.h"
#include "../express/exand.h"
#include "../express/exeq.h"
#include "../express/exlt.h"
#include "../express/exgt.h"
#include "../express/exin.h"
#include "../express/exadd.h"
#include "../express/exmult.h"
#include "../express/exdiv.h"
#include "../express/exmod.h"
#include "../express/exminus.h"
#include "../express/exborder.h"
#include "../express/exnot.h"
#include "../express/exrecord.h"
#include "../express/excell.h"
#include "../express/exfunc.h"
#include "../express/exall.h"
#include "../express/exone.h"
#include "../express/exnum.h"
#include "../express/excomp.h"
#include "../express/fnelemen.h"



/* global variables: */

	/* the list of Pair of expression representing the color-part */
List * color; 

	/* the token containing the program name */
Token * gProgName;

	/* the rule statement */
Statement * gRule;

	/* the identifier dictionary */
class Dictionary * identifier;  



/* error handling functions */

/* the error message for floats */
#define NOFLOAT "float numbers are not supptorted"


static void yyerror(char * s)
{
  char temps[255];
  
  sprintf(temps,"\n%s:%u: ERROR at `'%s'':%s\n",inputfilename,inputlinenumber,yytext,s);
  error(temps);
}


void semerror(class Object *token, char *s )
{
	char temps[255];
	char * tokenclass;

	DEBUG_enter("semerror-function ")
	if (NULL!=token)
	{

		tokenclass= BString::cast( token->className() ) -> content();

		if (token->isKindOf(Token::ClassName))   
		{ 
			DEBUG_msg("token of Token-Class")
			sprintf(
				temps,"\n%s:%u: ERROR at `'%s'':%s\n",
				   Token::cast(token)->getFileName(),
				   Token::cast(token)->getLineNumber(),
				   Token::cast(token)->content(),
				   s
			);
		}
		else 
		if (token->isKindOf(BString::ClassName))   
		{
			sprintf(
				temps,"\n%s:%u: ERROR at `'%s'':%s\n",
				   inputfilename,
				   inputlinenumber,
				   BString::cast(token)->content(),
				   s
			);
		}
		else
		{ 
			DEBUG_msg("token of unknown class")
			sprintf(temps,"error in object of class:%s\n%s ",tokenclass,s);
		}
	}
	else
	{
		sprintf(temps,"\n%s:%u: ERROR : %s\n",
				   inputfilename,
				   inputlinenumber,
				   s
		);
	}
//    warning(temps);
	error(temps);
	DEBUG_exit("semerror-function")
}

/* bildet das Kreuzprodukt (Record) aus der Liste von Konstantenlisten */
AGroup * makeRecordGroup(List * l)
{
	AGroup *g;
	List * indexList;
	int wrap;

	DEBUG_enter("AGroup * makeRecordGroup(List * l)")

	/* teste Struktur in l */

	for ( Iterator tl(l) ; tl ; tl++ )
	{
		Object *o;

/*        
		if (!tl.isKindOf(ListObject::ClassName))
		{ error("makeRecordGroup: list1 has other Elements then ListObjects"); }
*/

		o=ListObject::cast(tl)->getObject();

		if (!o->isKindOf(List::ClassName))
		{ 
			warning("makeRecordGroup: member of i is not a list");
		}

		for ( Iterator t2(List::cast(o)) ; t2 ; t2++ )
		{
			Object *p;

			p=ListObject::cast(t2)->getObject();
			if (!p->isKindOf(AConstant::ClassName))
			{ error("makeRecordGroup: member is not Constant"); }
		}
	}

	DEBUG_msg("AGroup * makeRecordGroup(List * l): structure is OK");


	g=new AGroup();
	indexList=new List();
	/* setze alle indexe auf die erste Konstante */ 
	for ( int i=1 ; i<=l->getSize() ; i++ )
	{ indexList->add( new ListObject( new Integer(1) ) );   }
	
	do
	{
		List * rlist;

		rlist=new List();
		wrap=1;

		for ( int comp=1 ; comp<=l->getSize() ; comp++ )
		{
			Integer * ind;

			ind=Integer::cast(
				ListObject::cast(indexList->at(comp))->getObject()
			);
			
			rlist->add(
				new ListObject(
					ListObject::cast(
						List::cast(
							ListObject::cast(
								l->at(comp)
							)->getObject()
						)->at(ind->value())
					)->getObject()
				)
			);

			if (wrap)
			{
				ind->setValue(
					(ind->value())+1
					);
	
				if (
					ind->value() >
					List::cast(
						ListObject::cast(l->at(comp))->getObject()
					)->getSize()
				)
				{
					ind->setValue(1);
					wrap=1;
				}
				else
				{
					wrap=0;
				}
			}    /* if (wrap) */
		}    /* for (comp) */

		g->add(new ListObject(new AConRecord(rlist)));
	}
	while (!wrap);

	DEBUG_exit("AGroup * makeRecordGroup(List * l)")

	return g;
}



void parserinit(void)
{

#ifdef YYDEBUG  
  if (getenv("DEBUG")) yydebug=6;
#endif

  inputlinenumber=1;
  identifier=new Dictionary();
  identifier->addAssoc(new BString("colortype"),new ATypeRecord() );
}



%}

%left  PRECLO
%left  PRECHI

%left  NOPOINTPOINT
/* %left POINTPOINT */

/* %left CONSTANT */

%left  NOELSE
%left  ELSE

%left  '\''
%left  OR
%left  XOR
%left  AND
%left  POINTPOINT  /* must be higher then AND */
%left  CONSTANT    /* must be higher then AND */
%left  '>' '<' LE GE NE '=' IN
%left  '+' '-'
%left  DIV MOD '*' '/'
%right NOT SIGN
%left  '.'
%right CELLREF /* '*' */



%token  FNUM
%token  NUM
%token  IDENTIFIER

%token  SCANERROR         /* the token for an unknown char */

 /* now the keywords: */
%token  CELLULAR AUTOMATON TYPE RULE INTEGER FLOAT BOOLEAN 
%token  RECORD UNION COLOR TRUE FALSE
%token  DO OTHERWISE IN OF CONST GROUP VAR IF THEN ELSE CASE FOR  
%token  BBEGIN  /*  stand for 'begin' (BEGIN is predefined in YACC) */
%token  END
%token  BORDER
%token  NEXT PREV ELEMENT VALUE ONE ALL NUMBER NOT DIV MOD
%token  AND OR XOR

 /* now the multicharacter operators: */
%token  POINTPOINT ASSIGN  LE GE NE     /*  ..  :=  <=  >=  != */

%%

input: 
	name.declaration cgt.declaration color.declaration.opt 
	var.declaration.opt rule.declaration { $$=$1; }
  | SCANERROR        
	{ 
		yyerror("unrecogniced character found"); 
		YYABORT; 
	}
  ;


name.declaration:
  CELLULAR AUTOMATON IDENTIFIER ';' 
	{   
		gProgName=Token::cast($3);

		VERBOSE(
			DEBUG_str( BString::cast($3->className())->content() )
			DEBUG_msg("prog name")
			printf("\nreading automaton: %s\n", gProgName->content() );
		)  

		$$=NULL;
	}


cgt.declaration:
  /* nothing */                                             { $$=NULL; }
  | c.or.g.or.t.declaration.list                            { $$=NULL; }
  ;

c.or.g.or.t.declaration.list:
  c.or.g.or.t.declaration                                   { $$=NULL; }
  | c.or.g.or.t.declaration.list c.or.g.or.t.declaration    { $$=NULL; }
  ;

c.or.g.or.t.declaration:
  type.declaration                                          { $$=NULL; }
  | const.declaration                                       { $$=NULL; }
  | group.declaration                                       { $$=NULL; }
  ;

/********** TYPE **********/ 

type.declaration:
  TYPE type.definition.list                                 
	{ $$=NULL; }
  ;

type.definition.list:
  type.definition                                           { $$=NULL; }
  | type.definition.list type.definition                    { $$=NULL; }
  ;

type.definition:
  IDENTIFIER '=' a.type ';'
	{ 
		if (identifier->includes ($1)) semerror($1,"identifier redeclared");
		else
		{ 
			identifier->addAssoc($1,$3); 
			$$=NULL;
		}
	}
  ;

a.type:
  INTEGER       { $$ = new ATypeInt(); }
  | FLOAT       { semerror($1,NOFLOAT); $$ = NULL; }
  | BOOLEAN     { $$ = new ATypeBool(); }
/*  | CELLADDRESS wird durch IDENTIFIER abgedeckt */
/*  | COLORTYPE    *** predefined ***  */
  | enum.type   { $$=$1; }
  | record.type { $$=$1; }
  | union.type  { $$=$1; }
  | a.constant POINTPOINT a.constant
	{ 
	  DEBUG_enter("a.type: RANGE")
	  if (!$1->isKindOf(AConInt::ClassName))
	  { semerror($2,"lower bound isn't constant integer"); }
	  if (!$3->isKindOf(AConInt::ClassName))
	  { semerror($2,"upper bound isn't constant integer"); }

	  DEBUG_msg("a.type: calc range")
	  {
		int lower,  upper;

		lower=AConInt::cast($1)->value();   DEBUG_int(lower)    
		upper=AConInt::cast($3)->value();   DEBUG_int(upper)

		if (lower > upper)
		{ semerror($2,"lower bound of range greater then upper bound"); }
	 
		{ $$ = new ATypeRange( new Integer(lower), new Integer(upper) ); }
	  }
	  DEBUG_exit("a.type: RANGE")
	}
  | IDENTIFIER
	{
	  if ( ! identifier->includes($1)  ) 
	  { semerror($1,"undeclared identifier"); }
	  else
	  {
		Object * tmp;
		tmp=identifier->getValueAt($1);
		if (!tmp->isKindOf(AType::ClassName))
		{ semerror($1,"isn't a type"); }
		else
		{ $$ = tmp; }
	  }
	}
  ;


enum.type:
  '(' identifier.list ',' IDENTIFIER ')' 
	/* die identifier muessen neu sein. Ein Enum mit nur einem Element 
	   ist verboten; dass ist noetig wegen einem r/r-Konflikt mit (expression) */
	{
		ATypeEnum * myType;
			
		DEBUG_enter("enum.type")
		myType=new ATypeEnum();
		List::cast($2)->add(new ListObject($4) );
		for ( Iterator i(List::cast($2)); i; i++ )
		{
			BString * name =BString::cast(ListObject::cast(i)->getObject());
			if (identifier->includes( name ))
			{ semerror(name,"identifier redeclared"); }
			else
			{ identifier->addAssoc(name,new AConEnum(name,myType)); }
		}
		myType->setList(List::cast($2));
		$$=myType; 
		DEBUG_exit("enum.type")
	}
  ;


identifier.list:
  IDENTIFIER                     
	{ 
		$$ = new List(); 
		List::cast($$)->add( new ListObject($1) ); 
	}
  | identifier.list ',' IDENTIFIER
	{
		List::cast($1)->add( new ListObject($3) );
		$$=$1;
	}
  ;


record.type:
  RECORD comp.list  END
	{
		$$=new ATypeRecord($2);
	}
  ;

union.type:
  UNION comp.list  END
	{
		$$=new ATypeUnion($2);
	}
  ;

comp.list: /* List */
  comp
	{
		$$=$1;
	}
  | comp.list comp
	{
		/* List::cast($1)->addAll( List::cast($2) ); */
		for (Iterator i(List::cast($2));i;i++)
		{
		   List::cast($1)->add(new ListObject(ListObject::cast(i)->getObject()));
		}
		$$=$1;
	}
  ;

comp: /* List */
  comp.selector.list ':' a.type ';'
	{
		for ( Iterator i(List::cast($1)) ; i ; i++ )
		{
			Pair::cast(ListObject::cast(i)->getObject())->setRight($3);
		}
		$$=$1;
	}
  ;

comp.selector.list: /* List */
  comp.range {$$=$1; }
  | comp.name
	{
		List *l;
		l=new List();
		List::cast(l)->add( new ListObject( new Pair($1,NULL) ) );
		$$=l;
	}
  | comp.selector.list ',' comp.name
	{
		List::cast($1)->add( new ListObject( new Pair($3,NULL) ) );
		$$=$1;
	}
  | comp.selector.list ',' comp.range
	{
		/* List::cast($1)->addAll(List::cast($3)); */
		for (Iterator i(List::cast($3));i;i++)
		{
		   List::cast($1)->add(new ListObject(ListObject::cast(i)->getObject()));
		}
		$$=$1;
	}   
  ;

comp.range:
  num.comp.selector POINTPOINT num.comp.selector 
	{
		List *l;
		l= new List();
		for (int i=AConInt::cast($1)->value();i<=AConInt::cast($3)->value();i++)
		{
			l->add(new ListObject(
				new Pair( new AConInt(new Integer(i)) , NULL )
			   ));
		}
		$$=l;
	}
	
comp.name:
  comp.selector { $$=$1; }
  ;

comp.selector:
  IDENTIFIER { $$=$1; }
  | num.comp.selector  {$$=$1;}
  ;
  

num.comp.selector:
  '%' IDENTIFIER /* IDENT ist eine bereits deklarierte Int-Konstante */
	{
	  if (!identifier->includes($2)) 
	  { semerror($2,"identifier undeclared"); }
	  else
	  { 
		Object * o;
		o=identifier->getValueAt($2);
		if (!o->isKindOf(AConInt::ClassName))
		{ semerror($2,"isn't an integer constant"); }
		else
		{
/*
			char s[100];
			sprintf(s,"%u",AConInt::cast(o)->value());
			$$=new BString(s);
*/
			$$=o;
		}
	  }
	}
  | '%' NUM   /* { $$=BString::cast($2); } */
	{
	  $$=new AConInt(new Integer( atoi(Token::cast($1)->content()) ));
	}
  ;


/********** CONST **********/ 

const.declaration:
  CONST const.definition.list 
	{ 
	  int dimension,distance;
	  Object * o;
	  List * l;

	  if 
	  (
		( identifier->includes(new BString("dimension"))) &&
		( identifier->includes(new BString("distance"))) &&
		(!identifier->includes(new BString("celladdress")))
	  )
	  { 
		o=identifier->getValueAt(new BString("dimension"));
		if (o->isKindOf(AConInt::ClassName))
		dimension=AConInt::cast(o)->value();
		distance=AConInt::cast(identifier->getValueAt(new BString("distance")))->value(); 
		
		l=new List();
		l->add(new ListObject(new Pair(new BString("x"),new ATypeRange(new Integer(-distance),new Integer(distance)))));
		l->add(new ListObject(new Pair(new BString("y"),new ATypeRange(new Integer(-distance),new Integer(distance)))));

		identifier->addAssoc(new BString("celladdress"),new ATypeRecord(l));
	  }
	  $$=NULL;  
	}
  ;

const.definition.list:
  const.definition                                          { $$=NULL; }
  | const.definition.list const.definition                  { $$=NULL; }
  ;

const.definition:
  IDENTIFIER '=' a.constant ';'
	{ 
	  if (identifier->includes ($1))
	  { semerror($1,"identifier redeclared"); }
	  else
	  { 
		identifier->addAssoc($1,$3 ); 
		$$=NULL;
	  }
	}
  ;  

a.constant:
  expression %prec CONSTANT  
	{ 
	  Object * c;
	  DEBUG_enter("a.constant")

	  if ($1->isKindOf(AConstant::ClassName) )
	  { c=AConstant::cast($1); }
	  else
	  {
		if ($1->isKindOf(Expression::ClassName) )
		{ c=Expression::cast($1)->asConstant(); }
		else
		{ 
		  if ($1->isKindOf(AGroup::ClassName))
		  { c=$1; } 
		  else 
		  { c=NULL; } 
		}
	  }

	  if (NULL==c)
	  { semerror($1,"isn't constant"); }
	  else
	  { $$=c; }
	  
	  DEBUG_exit("a.constant")
	}
  ;


/********** GROUP **********/ 

group.declaration:
  GROUP group.definition.list                               { $$=NULL; }
  ;

group.definition.list:
  group.definition                                          { $$=NULL; }
  | group.definition.list group.definition                  { $$=NULL; }
  ;

group.definition:
  IDENTIFIER '=' group ';'
	{ 
	  if (identifier->includes ($1))
	  { semerror($1,"identifier redeclared"); }
	  else
	  { identifier->addAssoc($1,$3); }
	}   
  ;

group:
  a.constant %prec NOPOINTPOINT /* kann eine Konstante oder eine Gruppe sein */
	  {
		if ($1->isKindOf(AGroup::ClassName))
		{
			/* man muss eine Kopie der Gruppe machen */
			AGroup *g;
			g=new AGroup();
			for (Iterator i(AGroup::cast($1)) ; i ; i++ )
			{
				g->add(new ListObject(ListObject::cast(i)->getObject()));
			}
			$$=g;
		}
		else 
		{
			if ($1->isKindOf(AConstant::ClassName))
			{
				AGroup *l;
				l=new AGroup();
				l->add(new ListObject($1));
				$$=l;
			}
			else
			{ error("unknown constant in group"); }
		}
	  }

/*  | IDENTIFIER schon durch a.constant behandelt */
  | true.group   
	  { $$=$1; }
  ;

group.list:
	group  
	  { $$=$1; }
  | group.list ',' group 
	{ 
		/* AGroup::cast($1)->addAll(AGroup::cast($3)); */
		for ( Iterator i(AGroup::cast($3));i;i++)
		{
			AGroup::cast($1)->add(
				new ListObject(
					ListObject::cast(i)->getObject()
				)
			);
		}
		$$=$1; 
	  }
  ;

true.group:
	a.constant POINTPOINT a.constant
	  { 
		AGroup * g;
		g=new AGroup();
		if (!$1->isKindOf(AConInt::ClassName))
		{ semerror($1,"lower bound of range isn't int"); }
		if (!$3->isKindOf(AConInt::ClassName))
		{ semerror($3,"upper bound of range isn't int"); }
		for( int i=AConInt::cast($1)->value() ; 
			 i<=AConInt::cast($3)->value() ;
			 i++)
		{ g->add(new ListObject(new AConInt(i))); }
		$$=g;
	  }
  | '{' group.list '}'                
	  { $$=$2; }
  | '[' true.group.expression.list.0 ']' 
		{ $$=makeRecordGroup(List::cast($2)); }

  | '[' true.group.expression.list.1 ']' 
		{ $$=makeRecordGroup(List::cast($2)); }

/*  | '\'' IDENTIFIER '\'' a.constant POINTPOINT a.constant */
	  /* hier gibt es Probleme mit der Eindeutigkeit der Grammatik */
  | '\'' IDENTIFIER '\'' '{' group.list '}'     { $$=$5; }
  | '\'' IDENTIFIER '\'' '[' true.group.expression.list.0 ']' 
		{ $$=makeRecordGroup(List::cast($5)); }

  | '\'' IDENTIFIER '\'' '[' true.group.expression.list.1 ']' 
		{ $$=makeRecordGroup(List::cast($5)); }
  ;

true.group.expression.list.0:        /* endet auf true.group */
	true.group
	  { List *l; l=new List(); l->add(new ListObject($1)); $$=l; }
  | expression.list ',' true.group 
	  {
		List *l;
		
		l=new List();

		for ( Iterator i(List::cast($1)) ; i ; i++ )
		{
			Expression * ex;
			AConstant *c;
			List * l2;

			ex=Expression::cast(ListObject::cast(i)->getObject());
			c=ex->asConstant();

			if (NULL==c)
			{ semerror(ex,"element of group isn't constant"); }
			
			l2= new List();
			l2->add( new ListObject(c) );

			l->add(new ListObject(l2));
		}

		l->add(new ListObject($3));
		$$=l;
	  }
  | true.group.expression.list.0 ',' true.group
	  { List::cast($1)->add(new ListObject($3)); $$=$1; }
  | true.group.expression.list.1 ',' true.group 
	  { List::cast($1)->add(new ListObject($3)); $$=$1; }
  ;

true.group.expression.list.1:
	true.group.expression.list.0 ',' a.constant
	  { 
		List *l;
		l=new List();
		l->add(new ListObject($3));
		List::cast($1)->add(new ListObject(l)); 
		$$=$1; 
	  }
  | true.group.expression.list.1 ',' a.constant
	  { 
		List *l;
		l=new List();
		l->add(new ListObject($3));
		List::cast($1)->add(new ListObject(l)); 
		$$=$1; 
	  }

  ;

/********** COLOR **********/ 

color.declaration.opt:
  /* nothing */                                             { $$=NULL; }
  | COLOR color.definition.list                             { $$=NULL; }
  ;

color.definition.list:
  color.definition
	{ 
		$$=NULL; 
		color=new List(); 
		color->add($1); 
	}
  | color.definition.list color.definition
	{ 
		$$=NULL;
		color->add($2); 
	}
  ;

color.definition:
  expression '~' expression ';'   { $$= new ListObject( new Pair($1,$3) ); }
  ;

/********** VAR **********/ 

var.declaration.opt:
  /* nothing */                                             { $$=NULL; }
  | VAR var.definition.list                                 { $$=NULL; }
  ;

var.definition.list:
  var.definition                                            { $$=NULL; }
  | var.definition.list var.definition                      { $$=NULL; }
  ;

var.definition:
  identifier.list ':' a.type ';' 
	{ 
	  DEBUG_enter("var.definition")
	  for ( Iterator i(List::cast($1)) ; i ; i++ )
	  {
		if (identifier->includes(ListObject::cast(i)->getObject()))
		{ semerror(i,"identifier redefined"); }
		else
		{ identifier->addAssoc(ListObject::cast(i)->getObject(),new AVariable(BString::cast(ListObject::cast(i)->getObject()),AType::cast($3)) ); }
	  }
	  $$=NULL;
	  DEBUG_exit("var.definition")
	}
  ;


/********** RULE **********/ 

rule.declaration:
  RULE statement ';'
	{
		/* test for dimension, distance and celltype */
		
		gRule=Statement::cast($2);
		$$=NULL;

		DEBUG_code(
			printf("\nall defined identifiers:\n");
			for (Iterator i(identifier);i;i++)
			{
				printf("\t%-15s\t%-15s\n",
					BString::cast(i)->content(),
					BString::cast( identifier->getValueAt(i)->className() )->content()
				);   
			}
		)
	}
  ;

/********** STATEMENTS **********/

statement:
  compound.statement    { $$=$1; }
  | assign.statement    { $$=$1; }
  | if.statement        { $$=$1; }
  | case.statement      { $$=$1; }
  | for.statement       { $$=$1; }
  ;

compound.statement:
  BBEGIN statement.list ';' END  { $$=new StComp($2); }
  ;

statement.list:
  statement
	{
		$$=new List();
		List::cast($$)->add( new ListObject($1) );
	}
  | statement.list ';' statement
	{
		List::cast($$)->add( new ListObject($3) );
	}
  ;

assign.statement:
  l.value ASSIGN expression { $$=new StAssign($1,$3); }
  ;

l.value:
  IDENTIFIER subtype.reference.opt
	{
		Object *o;
		o=identifier->getValueAt($1);
		
		if (NULL==o)
		{ semerror($1,"identifier undeclared"); }

		if (!o->isKindOf(AVariable::ClassName))
		{ semerror($1,"identifier isn't a variable"); }

		$$=new Pair(o,$2);
	}
  | '*' IDENTIFIER subtype.reference.opt
	{
		Object *o;

		o=identifier->getValueAt($2);
		
		if (NULL==o)
		{ semerror($2,"identifier undeclared"); }
		
		$$=new Pair(new ExCell(o),$3);
	}
  | '*' '[' expression.list ']' subtype.reference.opt
	{ $$=new Pair(new ExCell(new ExRecord(List::cast($3))),$5); }
  ;


subtype.reference.opt:
  /* nothing */            { $$=new List(); }
  | subtype.reference.list { $$=$1; }
  ;

subtype.reference.list:
  '.' comp.selector
	{
		$$=new List();
		List::cast($$)->add( new ListObject($2) );
	}
  | subtype.reference.list '.' comp.selector
	{
		List::cast($1)->add( new ListObject($3) );
		$$=$1;
	}
  ;


if.statement:
  IF expression THEN statement ELSE statement  { $$=new StIfElse($2,$4,$6); }
  | IF expression THEN statement %prec NOELSE  { $$=new StIf($2,$4); }
  ;

case.statement:
  CASE expression OF subcase.statement.list otherwise.statement.opt END
	{
		$$=new StCase($2,$4,$5);
	}
  ;

subcase.statement.list:
  subcase.statement
	{
		$$=new List();
		List::cast($$)->add(new ListObject($1));
	}
  | subcase.statement.list subcase.statement
	{
		List::cast($1)->add(new ListObject($2));
		$$=$1;
	}
  ;

subcase.statement:
  group ':' statement ';'   { $$=new Pair($1,$3); }
  ;

otherwise.statement.opt:
  /* nothing */                     { $$=NULL; }
  | OTHERWISE ':' statement ';'     { $$=$3; }
  ;

for.statement:
  FOR loop.var.list DO statement
	{
		$$=new StFor($2,$4);
	}
   ;

loop.var.list:
  loop.var
	{
		$$=new ALoopVarList();
		ALoopVarList::cast($$)->add(new ListObject($1));
	}
  | loop.var.list '&' loop.var
	{
		ALoopVarList::cast($1)->add(new ListObject($3));
		$$=$1;
	}
  ;

loop.var:
  identifier.list IN group   { $$=new Pair($1,$3); }
  ;

/********** EXPRESSIONS **********/

expression:

		  '\'' IDENTIFIER '\'' expression 
			{ $$= new ExCast(Token::cast($2),$4); }

		| expression OR  expression { $$= new ExOr($1,$3); }

		| expression XOR expression { $$= new ExXor($1,$3); }

		| expression AND expression { $$= new ExAnd($1,$3); }

		| expression '=' expression { $$= new ExEq($1,$3); }
		| expression NE  expression { $$= new ExNot(new ExEq($1,$3)); }
		| expression '<' expression { $$= new ExLt($1,$3); }
		| expression '>' expression { $$= new ExGt($1,$3); }
		| expression LE  expression { $$= new ExNot( new ExGt($1,$3)); }
		| expression GE  expression { $$= new ExNot( new ExLt($1,$3)); }
		| expression IN  group      { $$= new ExIn($1,$3); }

		| expression '+' expression { $$= new ExAdd($1,$3); }
		| expression '-' expression { $$= new ExAdd($1,new ExMinus($3)); }

		| expression '*' expression { $$= new ExMult($1,$3); }
		| expression '/' expression { semerror($2,NOFLOAT); }
		| expression DIV expression { $$= new ExDiv($1,$3); }
		| expression MOD expression { $$= new ExMod($1,$3); }

		| '+' expression %prec SIGN { $$=$2; }
		| '-' expression %prec SIGN { $$= new ExMinus($2); }
		| NOT expression            { $$= new ExNot($2); }

		| expression '.' comp.selector { $$=new ExComp($1,$3); }
		
		| '*' expression %prec CELLREF { $$= new ExCell($2); }

		| '(' expression ')'         { $$=$2; }
		| '[' expression.list ']'    { $$=new ExRecord( List::cast($2) ); }

		| IDENTIFIER '(' expression.list ')'
			{
				$$=new ExFunction($1,$3);
			}    

		| BORDER { $$=new ExBorder(); }
/*        | NEXT  '(' expression ')' { $$=new ExFunction($1,$3); } */
/*        | PREV  '(' expression ')' { $$=new ExFunction($1,$3); } */
		| VALUE '(' expression.list ')' { $$=new ExFunction($1,$3); } /* list size=1 */
		| ELEMENT '(' IDENTIFIER ',' expression.list ')' 
			{
				if (NULL==identifier->getValueAt($3))
				{ semerror($3,"identifier undeclared"); }
				if (!identifier->getValueAt($3)->isKindOf(AType::ClassName))
				{ semerror($3,"identifier not declared as Type"); }
				
				$$=new FnElement(AType::cast(identifier->getValueAt($3)),$5);
			}
		| ONE    '(' loop.var.list ':' expression ')'
			{
				$$=new ExOne($3,$5);
			}
		| ALL    '(' loop.var.list ':' expression ')' 
			{ 
				$$=new ExAll($3,$5); 
			}
		| NUMBER '(' loop.var.list ':' expression ')'
			{
				$$=new ExNum($3,$5);
			}
			
/*
		NUMBER '(' IDENTIFIER IN group ':' expression ')'  
			{ 
				if (NULL==identifier->getValueAt($3))
				{ semerror($3,"identifier undeclared"); }
				if (!identifier->getValueAt($3)->isKindOf(AVariable::ClassName))
				{ semerror($3,"identifier not declared as variable"); }
				$$=new ExNum(identifier->getValueAt($3),$5,$7); 
			}
*/
		| IDENTIFIER
		  { /* may be a group */
			if (!identifier->includes($1))
			{ semerror($1,"identifier undeclared"); }
			else
			{
			  Object * o;
			  o=identifier->getValueAt($1);
			  DEBUG_str(BString::cast($1)->content())
			  DEBUG_str(BString::cast(o->className())->content())
			  if (o->isKindOf(AConstant::ClassName) )
			  { $$=o; }
			  else
			  if (o->isKindOf(AType::ClassName))
			  { semerror($1,"no type allowed here"); }
			  else
			  { $$=o; } /* AVariable or AGroup */
			}
		  } 
		| NUM                             /* int.constant without sign */ 
		  {
			$$=new AConInt(new Integer( atoi(Token::cast($1)->content()) ));
		  }
				   
		| FNUM                      { semerror($1,NOFLOAT); $$=NULL; }            
				 
		| TRUE                      { $$ = new AConBool(1); }
		| FALSE                     { $$ = new AConBool(0); }
		;


expression.list:
	expression  
		{
			$$ = new List(); 
			List::cast($$)->add( new ListObject($1) ); 
		}
	| expression.list  ',' expression 
		{  
			DEBUG_enter("expression.list:2")
			List::cast($1)->add( new ListObject($3) );
			$$=$1;
			DEBUG_exit("expression.list:2")
		}
	;



%%

