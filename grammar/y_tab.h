#ifndef YYSTYPE
#define YYSTYPE int
#endif
#define	PRECLO	258
#define	PRECHI	259
#define	NOPOINTPOINT	260
#define	NOELSE	261
#define	ELSE	262
#define	OR	263
#define	XOR	264
#define	AND	265
#define	POINTPOINT	266
#define	CONSTANT	267
#define	LE	268
#define	GE	269
#define	NE	270
#define	IN	271
#define	DIV	272
#define	MOD	273
#define	NOT	274
#define	SIGN	275
#define	CELLREF	276
#define	FNUM	277
#define	NUM	278
#define	IDENTIFIER	279
#define	SCANERROR	280
#define	CELLULAR	281
#define	AUTOMATON	282
#define	TYPE	283
#define	RULE	284
#define	INTEGER	285
#define	FLOAT	286
#define	BOOLEAN	287
#define	RECORD	288
#define	UNION	289
#define	COLOR	290
#define	TRUE	291
#define	FALSE	292
#define	DO	293
#define	OTHERWISE	294
#define	OF	295
#define	CONST	296
#define	GROUP	297
#define	VAR	298
#define	IF	299
#define	THEN	300
#define	CASE	301
#define	FOR	302
#define	BBEGIN	303
#define	END	304
#define	BORDER	305
#define	NEXT	306
#define	PREV	307
#define	ELEMENT	308
#define	VALUE	309
#define	ONE	310
#define	ALL	311
#define	NUMBER	312
#define	ASSIGN	313


extern YYSTYPE yylval;
