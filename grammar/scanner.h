/* File: scanner.h */

/*
 * $Log: scanner.h%v $
# Revision 1.3  1994/05/28  18:40:48  waldsch
# no changes
#
# Revision 1.2  1994/05/24  21:44:49  waldsch
# neu: Variable inputfilename
#
 * Revision 1.1  1994/05/21  18:20:04  waldsch
 * Initial revision
 *
 */

/*
 * $Id: scanner.h%v 1.3 1994/05/28 18:40:48 waldsch Exp waldsch $
 */
 
/*
 * what the scanner exports to world
 */


#ifndef scanner_h
#define scanner_h

extern char   scannerRCSID [];
extern FILE * yyin;
extern char * yytext;            /* the text of the actual token    */
extern int    yylex(void);       /* the prototype of yylex function */
extern int    inputlinenumber;
extern char   inputfilename[255];

#endif /* scanner_h */
