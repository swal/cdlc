/* File: parser.h */

/*
 * $Log: parser.h%v $
# Revision 1.3  1994/05/28  18:42:09  waldsch
# no changes
#
# Revision 1.2  1994/05/24  21:46:21  waldsch
# neu: ???
#
 * Revision 1.1  1994/05/21  18:19:42  waldsch
 * Initial revision
 *
 */

/*
 * $Id: parser.h%v 1.3 1994/05/28 18:42:09 waldsch Exp waldsch $
 */
 
/*
 * what parser exports to world:
 */

#ifndef parser_h
#define parser_h

#include "../library/dictnary.h"
#include "../statmnt/statmnt.h"
#include "../main/error.h"
#include "../main/debug.h"

extern char parserRCSID[] ;
extern int yydebug;

extern class Dictionary * identifier;
extern class List * color;
extern class Statement * gRule;
extern void semerror(class Object *token, char *s );

extern int yyparse () ;
extern void parserinit () ;

#endif /* parser_h */
