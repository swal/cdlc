/*
 * $Log$
 *
 */

/* $Id$ */ 


#include "syntaxel.h"
#include "../main/debug.h"


#define SUPERCLASS Object

INITIALIZE_ABSTRACT_CLASS(SyntaxElement,SUPERCLASS);


void SyntaxElement::warning(char *s)
{
    char info[255];

    sprintf(info,"%s:%u:%s",
        token->getFileName(),
        token->getLineNumber(),
        s
    );

// call warning from error.c:
    ::warning(info);
}


extern Token *yylval;

SyntaxElement::SyntaxElement()
{
    token=yylval;
}
