/*
 * $Log$
 *
 */

/*$Id$*/

#ifndef ATYPE_ENUM_H
#define ATYPE_ENUM_H

#include "../klassen/atype.h"
#include "../klassen/agroup.h"

class ATypeEnum : public AType
{
	protected:
		List * myList;
		virtual AGroup  * asGroupC();

	public:
		ATypeEnum();
		ATypeEnum(List * l);
		
		virtual void      setList(List * l);
		virtual BString * writeComponentNames(BString* head);
		virtual BString * codingOf(AConstant * c);
		virtual int       positionOf(AConstant * c);  /* 0,1,2... */
		virtual int       getNumberOfBits();
		virtual int       calcIndex(int i);
		virtual int       writeXCS(FILE *xcs,char *ind,int i);


	DEFAULT_CONCRETE_METHODS(ATypeEnum,AType);
};

#endif
