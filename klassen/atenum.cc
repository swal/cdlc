/*
 * $Log$
 *
 */

/* $Id$ */ 
#include "atenum.h"

#include "../main/debug.h"
#include "../main/error.h"
#include "../main/logic.h"
#include "../grammar/parser.h"
#include "../library/bstring.h"
#include "../library/listobj.h"
#include "../klassen/acenum.h"
#include "../klassen/aconstnt.h"


#define SUPERCLASS AType
INITIALIZE_CONCRETE_CLASS(ATypeEnum, SUPERCLASS);

ATypeEnum::ATypeEnum()
{
	myList=NULL;
}

ATypeEnum::ATypeEnum(List * l)
{
	myList=l;     /* ist eine Liste aus ListOject, die Token enthalten */
}

void ATypeEnum::setList(List * l)
{
	myList=l;
}

BString * ATypeEnum::writeComponentNames(BString* head)
{
	char s[10];
	DEBUG_enter("BString * ATypeEnum::writeComponentNames(BString* head)")
	head->append(new BString("["));
	sprintf(s,"%u",logd(myList->getSize()-1));
	head->append(new BString(s));
	head->append(new BString("..0]"));
	DEBUG_exit("BString * ATypeEnum::writeComponentNames(BString* head)")
	return head;
}

AGroup* ATypeEnum::asGroupC()
{
	AGroup * cache;

	DEBUG_enter("AGroup* ATypeEnum::asGroupC()")
	
	cache = new AGroup();
	for ( Iterator i(myList) ; i ; i++ )
	{
		cache->add( new ListObject(
					new AConEnum(
						BString::cast(ListObject::cast(i)->getObject() ),
						this
								)
								  )
				  );
	}
	DEBUG_exit("AGroup* ATypeEnum::asGroupC()")
	return cache;
}


int ATypeEnum::positionOf(AConstant * c)  /* 0,1,2... */
{
	BString * con;
	int num;
	DEBUG_enter("int ATypeEnum::positionOf(AConstant * c)")

	if (! (c->isKindOf(AConEnum::ClassName)) )
	{
		semerror(c,"constant isn't of type enum");
		return -1;
	}
	else
	{
		con=AConEnum::cast(c)->asString();
		num=0;
		for ( Iterator i(myList) ; i ; i++ )
		{
			if ( con==BString::cast (ListObject::cast(i)->getObject() ) )
			{
				DEBUG_exit("int ATypeEnum::positionOf(AConstant * c)")
				return num;                
			}
			else
			{
				num++;
			}
		}
		error("constant isn't of this enum type");
		DEBUG_exit("int ATypeEnum::positionOf(AConstant * c)--error")
		return -1;
	}
	
	DEBUG_exit("int ATypeEnum::positionOf(AConstant * c)")
}


BString * ATypeEnum::codingOf(AConstant * c)
{
	BString * s;
	DEBUG_enter("BString * ATypeEnum::codingOf(AConstant * c)")
	s=binary(positionOf(c),logd(myList->getSize()-1)+1);
	DEBUG_exit("BString * ATypeEnum::codingOf(AConstant * c)--error")
	return s;
}


int ATypeEnum::getNumberOfBits()
{
	int cache;
	int anz;

	anz=myList->getSize();
	cache=logd(anz-1)+1;

	return cache;
}

int ATypeEnum::writeXCS(FILE *xcs,char *ind,int i)
{
	char indent [255];

	strcpy(indent,ind);
	strcat(indent,"  ");
	
	fprintf(xcs," enumeration : %u\n",i);
	
	for ( Iterator j(myList) ; j ; j++ )
	{
		fprintf(xcs,"%s%s\n",
			indent,
			BString::cast(
				ListObject::cast(j)->getObject()
			)->content()
		);
	}

	fprintf(xcs,"%send\n",indent);

	return calcIndex(i);
}


int ATypeEnum::calcIndex(int i)
{
	if (smallCoding)
	{ return i* (myList->getSize()); }
	else
	{ return i* ( 1<<getNumberOfBits() ); }
}


