/*
 * $Log$
 *
 */

/*$Id$*/

#ifndef SYNTAXELEMENT_H
#define SYNTAXELEMENT_H

#include "../library/object.h"
#include "../klassen/token.h"


class SyntaxElement : public Object
{
	protected:
		Token *token;
		virtual void warning(char *s);

	public:
		SyntaxElement();


	DEFAULT_ABSTRACT_METHODS(SyntaxElement,Object);
};

#endif
