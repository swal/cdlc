/*
 * $Log$
 *
 */

/* $Id$ */ 

#include "../main/debug.h"
#include "../klassen/acint.h"
#include "../klassen/acbool.h"
#include "../library/bstring.h"



#define SUPERCLASS AConstant
INITIALIZE_CONCRETE_CLASS(AConInt, SUPERCLASS);


AConInt::AConInt()
{ 
  myInt=NULL;
}

AConInt::AConInt( int i )
{
  DEBUG_enter("AConInt::AConInt( int i )")
  myInt=new Integer(i);
  DEBUG_exit("AConInt::AConInt( int i )")
}


AConInt::AConInt(Integer * i)
{
    DEBUG_enter("AConInt::AConInt(Integer * i)")
    myInt=i;
    DEBUG_exit("AConInt::AConInt(Integer * i)")
}

int AConInt::value()
{
  int i;
  DEBUG_enter("AConInt::value()")
  i=myInt->value();
  DEBUG_exit("AConInt::value()")
  return i;
}


BString * AConInt::asString()
{
    BString * b;
/*    char s[255],t[200]; */
    
    DEBUG_enter("BString * AConInt::asString()")

/*    
    sprintf(s,"%2.2Xh",(myInt->value()));
    strncpy(s,&s[strlen(s)-3],4);
    strcpy(t,"0");
    strcat(t,s);
    b=new BString(t);
*/
    b=binary(myInt->value(),8);

    DEBUG_exit("BString * AConInt::asString()")
    return b;
}

AType * AConInt::getType()
{
    AType * t;
    DEBUG_enter("AType * AConInt::getType()")
    t=new ATypeRange(myInt,myInt);
    DEBUG_exit("AType * AConInt::getType()")
    return  t;
}

AConBool * AConInt::operator == ( Object* o )
{
    return new AConBool(myInt->value()==AConInt::cast(o)->value());
}

