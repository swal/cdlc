/*
 * $Log$
 *
 */

/* $Id$ */ 

#include "atint.h"
#include "../library/bstring.h"
#include "../library/list.h"
#include "../library/listobj.h"
#include "../main/logic.h"


#define SUPERCLASS ATypeRange
INITIALIZE_CONCRETE_CLASS(ATypeInt, SUPERCLASS);


ATypeInt::ATypeInt()
{
	DEBUG_enter("ATypeInt::ATypeInt()")
	lower=-32768;
	upper=+32767;
	DEBUG_exit("ATypeInt::ATypeInt()")
}


int ATypeInt::writeXCS(FILE *xcs,char *,int i)
{
	fprintf(xcs," integer %i %i : %u\n",lower,upper,i);
	return calcIndex(i);
}
