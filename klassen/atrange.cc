/*
 * $Log$
 *
 */

/* $Id$ */ 

#include "atrange.h"
#include "../library/bstring.h"
#include "../library/list.h"
#include "../library/listobj.h"
#include "../main/logic.h"


#define SUPERCLASS AType
INITIALIZE_CONCRETE_CLASS(ATypeRange, SUPERCLASS);

ATypeRange::ATypeRange()
{
	DEBUG_enter("ATypeRange::ATypeRange()")
	lower=1;
	upper=0;
	DEBUG_exit("ATypeRange::ATypeRange()")
}

ATypeRange::ATypeRange(Integer * lo, Integer * hi)
{
	DEBUG_enter("ATypeRange::ATypeRange(Integer * lo, Integer * hi)")
	lower=lo->value();
	upper=hi->value();
	if (lower>upper) { semerror(NULL,"in type range: left side must be smaller than right"); }
	DEBUG_exit("ATypeRange::ATypeRange(Integer * lo, Integer * hi)")
}

BString * ATypeRange::writeComponentNames(BString* head)
{
	BString * cache;
	char s[20];

	DEBUG_enter("BString * ATypeRange::writeComponentNames(BString* head)")
	
	sprintf(s,"[%u..0]",getNumberOfBits()-1);

	head->append(new BString(s));
	cache=head;

	DEBUG_exit("BString * ATypeRange::writeComponentNames(BString* head)")
	
	return cache;
}

AGroup* ATypeRange::asGroupC()
{

	AGroup * cache;
	int i;

//  CACHEING;

	DEBUG_enter("AGroup* ATypeRange::asGroupC()")

	cache=new AGroup();
	for (i=lower;i<=upper;i++)
	{
		cache->add( new ListObject(new AConInt(new Integer(i))) );
	}
	
	DEBUG_exit("AGroup* ATypeRange::asGroupC()")
	
	return cache;
}

BString * ATypeRange::codingOf(AConstant*c)
{
	BString * cache;
	
	DEBUG_enter("BString * ATypeRange::codingOf(AConstant*c)")

	cache=binary(AConInt::cast(c)->value(),getNumberOfBits()); 

	DEBUG_exit("BString * ATypeRange::codingOf(AConstant*c)")
	return cache;
}

Integer * ATypeRange::getLower()
{
	return new Integer(lower);
}

Integer * ATypeRange::getUpper()
{
	return new Integer(upper);
}

int ATypeRange::getNumberOfBits()
{
	int cache;
	int needUpperBits,needLowerBits;

	DEBUG_enter("int ATypeRange::getNumberOfBits()")
	
	if ( (upper>0) && (lower>=0) ) 
	{
		cache=logd(upper)+1;
	}
	else
	{
		if (upper>0) 
		{ needUpperBits=logd(upper)+2; }
		else      
		{ needUpperBits=1; }
	
		if (lower>0) 
		{ needLowerBits=1; }
		else
		{
			if((-1==lower)||(0==lower)) needLowerBits=1;
			else needLowerBits=logd(-lower-1)+2;
		}

		cache=needUpperBits>needLowerBits?needUpperBits:needLowerBits;
	}
	/* assert(lower<upper); */

	DEBUG_exit("int ATypeRange::getNumberOfBits()")

	return cache;
}


int ATypeRange::isSigned()
{
	int i;
	DEBUG_enter("int ATypeRange::isSigned()")
	i=lower<0;
	DEBUG_exit("int ATypeRange::isSigned()")
	return i;
}


int ATypeRange::writeXCS(FILE *xcs,char *,int i)
{
	fprintf(xcs," integer_subrange %i %i : %u\n",lower,upper,i);
	return calcIndex(i);
}


int ATypeRange::calcIndex(int i)
{
	if (smallCoding)
	{ return i* (asGroup()->getSize()); }
	else
	{ return i* (1 << getNumberOfBits()); }
}

int ATypeRange::positionOfConstant(AConstant*c,int *i)
{
	int val;
	int ret;
	
	if (smallCoding)
	{ return AType::positionOfConstant(c,i); }
	else
	{
		val=AConInt::cast(c)->value();
		if (val>=0)
		{ ret=(*i) * val; }
		else
		{ ret=(*i) * ( (1<<getNumberOfBits())+val );}
		
		(*i)=calcIndex(*i);
		return ret;
	}
}
