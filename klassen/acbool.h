/*
 * $Log$
 *
 */

/*$Id$*/

#ifndef ACONBOOL_H
#define ACONBOOL_H

#include "../klassen/aconstnt.h"
#include "../klassen/atbool.h"

class AConBool : public AConstant
{
	private:
		int myBool;
	
	public:
		AConBool();
		AConBool(int i);
		int value();
		virtual AType * getType();
		virtual BString * asString();
        virtual AConBool * operator == (Object*);

	DEFAULT_CONCRETE_METHODS(AConBool,AConstant);
};

#endif
