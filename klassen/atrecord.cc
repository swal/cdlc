/*
 * $Log$
 *
 */

/* $Id$ */ 

#include "atrecord.h"
#include "../main/error.h"
#include "../main/debug.h"
#include "../library/bstring.h"
#include "../library/listobj.h"


#define SUPERCLASS AType
INITIALIZE_CONCRETE_CLASS(ATypeRecord, SUPERCLASS);

ATypeRecord::ATypeRecord()
{
	DEBUG_enter("ATypeRecord::ATypeRecord()")
	myList=NULL;
	DEBUG_exit("ATypeRecord::ATypeRecord()")
}

ATypeRecord::ATypeRecord(Object* l)
{
	DEBUG_enter("ATypeRecord::ATypeRecord(Object* l)")

	/* 'l' must be: List of Pair ( BString , AType ) */
	
	if (!l->isKindOf(List::ClassName))
	{ error("argument of ATypeRecord isn't a List"); }
	
	for(Iterator i(List::cast(l));i;i++)
	{
		if (!ListObject::cast(i)->getObject()->isKindOf(Pair::ClassName))
		{ error("argument of ATypeRecord isnt a Pair"); }
		if (!Pair::cast(ListObject::cast(i)->getObject())->getRight()->isKindOf(AType::ClassName))
		{ error("ATypeRecord: right of pair isnt a type"); }
	}
	myList=List::cast(l);
	DEBUG_exit("ATypeRecord::ATypeRecord(Object* l)")
}

int ATypeRecord::getPositionOf(BString * tag)
{
	int pos,j;

	DEBUG_enter("AType * ATypeRecord::getPositionOf(BString * tag)")
	
	/* tag may be a name or a number */

	pos=atoi(tag->content());
	
	if ( (0<pos) && (myList->getSize() >= pos) )
	{
		/* index is a valid number */
		*tag = (BString::cast(Pair::cast(ListObject::cast(myList->at(pos))->getObject())->getLeft())->content());
	}
	else
	{
		pos=0;
		j=1;
		for (Iterator i(myList);i;i++)
		{
			if (0==strcmp(
					BString::cast(
						Pair::cast(
							ListObject::cast(i)->getObject()
						)->getLeft()
					)->content(),
					tag->content()
					))
			{ pos=j; }
			j++;
		}
		if (0==pos)
		{ semerror(tag,"record doesn't have such a member"); }
	}
	
	DEBUG_exit("AType * ATypeRecord::getPositionOf(BString * tag)")
	return pos;
}

AType * ATypeRecord::getTypeAt(int pos)
{
	Object *o;
	AType * at;
	DEBUG_enter("AType * ATypeRecord::getTypeAt(int pos)")
	DEBUG_int(pos)
	o=myList->at(pos);
	if (NULL==o)
	{
		at=NULL;
	}
	else
	{
		at=AType::cast(Pair::cast(ListObject::cast(o)->getObject())->getRight());
	}
	DEBUG_exit("AType * ATypeRecord::getTypeAt(int pos)")
	return at;
}

BString * ATypeRecord::getNameAt(int pos)
{
	Object *o;
	BString *s;
	DEBUG_enter("BString * ATypeRecord::getNameAt(int pos)")
	o=myList->at(pos);
	if (NULL==o)
	{
		s=NULL;
	}
	else
	{
		s=BString::cast(Pair::cast(ListObject::cast(o)->getObject())->getLeft());
	}
	DEBUG_exit("BString * ATypeRecord::getNameAt(int pos)")
	return s;
}

BString * ATypeRecord::writeComponentNames(BString* head)
{
	BString *s, *sc;
	char c[200];
	int first;
	int pos;

	DEBUG_enter("BString * ATypeRecord::writeComponentNames(BString* head)")

	s=new BString();
	
	first=1;
	pos=1;

	for ( Iterator i(myList); i; i++)
	{
		Pair * p;
		p=Pair::cast(ListObject::cast(i)->getObject());
		if (!first) s->append(new BString(", "));
		first=0;
		s->append(head);
		sc=BString::cast(p->getLeft());
		if (NULL!=sc) { s->append(sc); }
		else
		{ 
			sprintf(c,"%u",pos);
			s->append(new BString(c)); 
		}
		
		s->append(new BString("_"));

		pos++;
		s=AType::cast(p->getRight())->writeComponentNames(s);
	}
	DEBUG_exit("BString * ATypeRecord::writeComponentNames(BString* head)")
	return s;
}


AGroup* ATypeRecord::asGroupC()
{
	List *ll; /* the List of all ConstantLists */
	AGroup *cache;

	DEBUG_enter("AGroup* ATypeRecord::asGroupC()")

	ll=new List();

	for ( Iterator i(myList) ; i ; i++ )
	{
		AGroup *l;

		DEBUG_str(BString::cast(Pair::cast(ListObject::cast(i)->getObject())->getRight()->className())->content())
		
		/* die Liste von Konstanten */
		l=
			AType::cast(
				Pair::cast(
					ListObject::cast(i)->getObject()
				)->getRight()
			)->asGroup(); 

		ll->add(new ListObject(l));
	}

	/* 
	 *  jetzt muss aus den Listen in der Liste ll das Kreuzprodukt 
	 *  berechnet werden 
	 */


	List * index; 
	/* fuer jeden Typ (jede Liste in der Liste ll) ein Zeiger auf den  
	   Anfang und einen auf das aktuelle element
	*/

	index=new List();

	for ( Iterator j(ll) ; j ; j++ )
	{
		index->add(
			new ListObject(
			new Pair(
				List::cast(ListObject::cast(j)->getObject())->at(1),
				List::cast(ListObject::cast(j)->getObject())->at(1)
				)
			)
		);
	}

	DEBUG_msg("zeigerstruktur initialisiert")

	int carry;
	List * c;

	Iterator k(index);

	cache=new AGroup();

	do
	{
		c=new List();

		for ( k.reset() ; k ; k++ )
		{
			ListObject *act;

			act=ListObject::cast(
				Pair::cast(ListObject::cast(k)->getObject())->getRight()
			);
			c->add(new ListObject(act->getObject()));
		}
		
		DEBUG_msg("a ConRecord collected")
		
		cache->add(new ListObject(new AConRecord(c)));

		/* "addiere" ein carry bit */

		carry=1;

		for ( k.reset() ; k ; k++ )
		{
			if (1==carry)
			{
				ListObject *act;
			
				act=ListObject::cast(
					Pair::cast(ListObject::cast(k)->getObject())->getRight()
				);


				act=ListObject::cast(act->next());
				if (NULL==act)
				{ 
				/* setze zurueck an Listenanfang */
					act=ListObject::cast(
						Pair::cast(ListObject::cast(k)->getObject())->getLeft()
					);
				 
				}
				else
				{ carry=0; }

				Pair::cast(ListObject::cast(k)->getObject())->setRight(act);
				
		   
			}
		}

	}
	while ( 1!=carry );
  
	DEBUG_exit("AGroup* ATypeRecord::asGroupC()")
	return cache;
}


BString * ATypeRecord::codingOf(AConstant*c)
{
	BString *s;
	int pos;

	DEBUG_enter("BString * ATypeRecord::codingOf(AConstant*c)")
	
	s=new BString("");

	pos=1;
	for ( Iterator i(myList); i;i++)
	{
		AType *t;
		t=AType::cast(Pair::cast(ListObject::cast(i)->getObject())->getRight());
		
		s->append( t->codingOf(AConstant::cast(ListObject::cast(AConRecord::cast(c)->at(pos))->getObject())) );
		pos++;
	}

	DEBUG_exit("BString * ATypeRecord::codingOf(AConstant*c)")

	return s;
}


int ATypeRecord::getNumberOfBits(void)
{
	int cache;
	
	cache=0;
	for ( Iterator i(myList) ; i ; i++ )
	{
		cache +=
			AType::cast(
				Pair::cast( ListObject::cast(i)->getObject() )->getRight()
			)->getNumberOfBits();
	}
	return cache;
}

int ATypeRecord::writeXCS(FILE *xcs,char *ind,int i)
{
	char indent[255];

	fprintf(xcs," record : %u\n",i);
	strcpy(indent,ind);
	strcat(indent,"  ");

	for ( Iterator j(myList) ; j ; j++ )
	{
		fprintf(xcs,"%s%s",
			indent,
			BString::cast(
				Pair::cast( ListObject::cast(j)->getObject() )->getLeft()
			)->content()
		);

		i=AType::cast(
			Pair::cast( ListObject::cast(j)->getObject() )->getRight()
		)->writeXCS(xcs,indent,i);
	}

	fprintf(xcs,"%send\n",indent);
	return i;
}


int ATypeRecord::calcIndex(int i)
{
	if (smallCoding)
	{
		return i*(asGroup()->getSize());
	}
	else
	{
		for ( Iterator j(myList) ; j ; j++ )
		{
			i=
				AType::cast(
					Pair::cast( ListObject::cast(j)->getObject() )->getRight()
				)->calcIndex(i);
		}

		return i;
	}
}

int ATypeRecord::positionOfConstant(AConstant*c,int *i)
{
	int ret;
	int pos;

	DEBUG_enter("int ATypeRecord::positionOfConstant(AConstant*c,int *i)")
	
	if (smallCoding)
	{ ret=AType::positionOfConstant(c,i); }
	else
	{
		ListObject *j;
		ret=0;
		
		pos=myList->getSize();

		j=ListObject::cast(myList->last());

//        for ( Iterator j(myList); j ; j++)

		while (NULL!=j)
		{
			AType *t;
			AConstant *ac;
			
			t=
				AType::cast(Pair::cast(
					ListObject::cast(j)->getObject()
				)->getRight());

			ac=
				AConstant::cast(ListObject::cast(
					AConRecord::cast(c)->at(pos)
				)->getObject());

			ret += t->positionOfConstant(ac,i);
		
			pos--;

			j=(ListObject*)(j->previous());
		}
	}

	DEBUG_exit("int ATypeRecord::positionOfConstant(AConstant*c,int *i)")
	return ret;
}
