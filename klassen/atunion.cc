/*
 * $Log$
 *
 */

/* $Id$ */ 

#include "atunion.h"
#include "../main/error.h"
#include "../main/debug.h"
#include "../library/bstring.h"
#include "../klassen/acunion.h"


#define SUPERCLASS AType
INITIALIZE_CONCRETE_CLASS(ATypeUnion, SUPERCLASS);

ATypeUnion::ATypeUnion()
{
	DEBUG_enter("ATypeUnion::ATypeUnion()")
	myList=NULL;
	DEBUG_exit("ATypeUnion::ATypeUnion()")
}


ATypeUnion::ATypeUnion(Object* l)
{
	DEBUG_enter("ATypeUnion::ATypeUnion(Object* l)")
	/* 'l' must be: List of Pair ( BString , AType ) */
	
	if (!l->isKindOf(List::ClassName))
	{ error("argument of ATypeUnion isn't a List"); }
	
	for(Iterator i(List::cast(l));i;i++)
	{
		if (!ListObject::cast(i)->getObject()->isKindOf(Pair::ClassName))
		{ error("argument of ATypeUnion isnt a Pair"); }
		if (!Pair::cast(ListObject::cast(i)->getObject())->getRight()->isKindOf(AType::ClassName))
		{ error("ATypeUnion: right of pair isnt a type"); }
	}
	myList=List::cast(l);
	DEBUG_exit("ATypeUnion::ATypeUnion(Object* l)")
}


BString * ATypeUnion::codingOf(AConstant* c)
{
	int i;
	AType * ctype;
	BString * cod;

	DEBUG_enter("BString * ATypeUnion::codingOf(AConstant*c)")

	ctype=c->getType();

	for ( 
		i=1 ; 
		(NULL!=getTypeAt(i))
		&&(ctype->className()!=getTypeAt(i)->className()) ; 
		i++ 
	);
       
	if (NULL==getTypeAt(i))
	{ semerror(NULL,"no such type in this union"); }
	
	cod=c->getType()->codingOf(c->asConstant());
	
	DEBUG_exit("BString * ATypeUnion::codingOf(AConstant*c)")
	return cod;
}


int ATypeUnion::getPositionOf(BString * tag)
{
	int pos,j;

	DEBUG_enter("AType * ATypeUnion::getPositionOf(BString * tag)")
	
	/* tag may be a name or a number */

	pos=atoi(tag->content());
	
	if ( (0<pos) && (myList->getSize() >= pos) )
	{
		/* index is a valid number */
		*tag = (BString::cast(Pair::cast(ListObject::cast(myList->at(pos))->getObject())->getLeft())->content());
	}
	else
	{
		pos=0;
		j=1;
		for (Iterator i(myList);i;i++)
		{
			if (0==strcmp(
					BString::cast(
						Pair::cast(
							ListObject::cast(i)->getObject()
						)->getLeft()
					)->content(),
					tag->content()
					))
			{ pos=j; }
			j++;
		}
		if (0==pos)
		{ semerror(tag,"record doesn't have such a member"); }
	}
	
	DEBUG_exit("AType * ATypeUnion::getPositionOf(BString * tag)")
	return pos;
}

AType * ATypeUnion::getTypeAt(int pos)
{
	Object *o;
	AType * at;
	DEBUG_enter("AType * ATypeUnion::getTypeAt(int pos)")
	DEBUG_int(pos)
	o=myList->at(pos);
	if (NULL==o)
	{
		at=NULL;
	}
	else
	{
		at=AType::cast(Pair::cast(ListObject::cast(o)->getObject())->getRight());
	}
	DEBUG_exit("AType * ATypeUnion::getTypeAt(int pos)")
	return at;
}

BString * ATypeUnion::getNameAt(int pos)
{
	Object *o;
	BString *s;
	DEBUG_enter("BString * ATypeUnion::getNameAt(int pos)")
	o=myList->at(pos);
	if (NULL==o)
	{
		s=NULL;
	}
	else
	{
		s=BString::cast(Pair::cast(ListObject::cast(o)->getObject())->getLeft());
	}
	DEBUG_exit("BString * ATypeUnion::getNameAt(int pos)")
	return s;
}

BString * ATypeUnion::writeComponentNames(BString* head)
{
	BString *s, *sc;
	char c[200];
	int first;
	int pos;

	DEBUG_enter("BString * ATypeUnion::writeComponentNames(BString* head)")

	s=new BString();
	
	first=1;
	pos=1;

	for ( Iterator i(myList); i; i++)
	{
		Pair * p;
		p=Pair::cast(ListObject::cast(i)->getObject());
		if (!first) s->append(new BString(", "));
		first=0;
		s->append(head);
		sc=BString::cast(p->getLeft());
		if (NULL!=sc) { s->append(sc); }
		else
		{ 
			sprintf(c,"%u",pos);
			s->append(new BString(c)); 
		}
		
		s->append(new BString("_"));

		pos++;
		s=AType::cast(p->getRight())->writeComponentNames(s);
	}
	DEBUG_exit("BString * ATypeUnion::writeComponentNames(BString* head)")
	return s;
}


AGroup* ATypeUnion::asGroupC()
{
	AGroup * cache;
	int anz,max=0;
	AType *at, *atmax;
	
	for ( Iterator i(myList) ; i ; i++ )
	{
		
		at=AType::cast(
			Pair::cast( 
				ListObject::cast(i)->getObject() 
			)->getRight()
		   );

		anz=at->getNumberOfBits();
		if (anz>max) 
		{ 
			max=anz; 
			atmax=at;
		}
	}

	cache=atmax->asGroup();

	DEBUG_exit("AGroup* ATypeUnion::asGroupC()")
	return cache;
}


int ATypeUnion::getNumberOfBits()
{
	int anz,max=0;
	
	for ( Iterator i(myList) ; i ; i++ )
	{
		anz=AType::cast(
			Pair::cast( ListObject::cast(i)->getObject() )->getRight()
		)->getNumberOfBits();
		if (anz>max) { max=anz; }
	}

	return max;
}

int ATypeUnion::calcIndex(int i)
{
	int anz,max=0;
	
	for ( Iterator j(myList) ; j ; j++ )
	{
		anz=AType::cast(
			Pair::cast( ListObject::cast(j)->getObject() )->getRight()
		)->calcIndex(i);
		if (anz>max) { max=anz; }
	}

	return i*max;
}

int ATypeUnion::writeXCS(FILE *xcs,char *ind,int i)
{
	int i2,max;
	char indent[255];

	fprintf(xcs," union : %u\n",i);
	strcpy(indent,ind);
	strcat(indent,"  ");

	max=0;

	for ( Iterator j(myList) ; j ; j++ )
	{
		fprintf(xcs,"%s%s",
			indent,
			BString::cast(
				Pair::cast( ListObject::cast(j)->getObject() )->getLeft()
			)->content()
		);

		i2=AType::cast(
			Pair::cast( ListObject::cast(j)->getObject() )->getRight()
		)->writeXCS(xcs,indent,i);

		if (i2>max) {max=i2;}
	}

	fprintf(xcs,"%send\n",indent);
	return max;
}


int ATypeUnion ::positionOfConstant(AConstant*c,int *i)
{
	return c->getType()->positionOfConstant(c,i);
}
