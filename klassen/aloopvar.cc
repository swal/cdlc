/*
 * $Log$
 *
 */

/* $Id$ */ 

#include "aloopvar.h"
#include "../main/debug.h"
#include "../library/pair.h"
#include "../library/listobj.h"
#include "../library/list.h"
#include "../klassen/agroup.h"
#include "../klassen/avariabl.h"
#include "../klassen/aconstnt.h"
#include "../grammar/parser.h"   /* identifier dictionary */
#include "../statmnt/stassign.h"

/*
 *  Ist eine Liste von Paaren ( Liste mit identifiern, gruppe )
 *  Die identifier repraesentieren Variablen
 */

#define SUPERCLASS List
INITIALIZE_CONCRETE_CLASS(ALoopVarList, SUPERCLASS);


ALoopVarList::ALoopVarList()
{
    DEBUG_enter("ALoopVarList::ALoopVarList()")
    DEBUG_exit("ALoopVarList::ALoopVarList()")
}


/*
 * Um die maximale Anzahl von Iterationen zu bestimmen muss die
 * Laenge der groessten Gruppe bestimmt werden. Dies ist der
 * Rueckgabewert
 */

int ALoopVarList::maxIteration() 
{
    int max;
    DEBUG_enter("static int maxIteration()")

    max=0;
    for ( Iterator i(this); i ; i++ )
    {
        int size;

        size=AGroup::cast(
                Pair::cast(
                    ListObject::cast(i)->getObject()
                )->getRight()
            )->getSize();
        if (size>max) { max=size; }
    }
    DEBUG_exit("static int maxIteration()")
    return max;
}



/* Rueckgabewert: 1 bei Erfolg, 0 bei Misserfolg */

int ALoopVarList::assignToIteration(int iter,AVariable* cond)
{
    int ok,size;
    
    DEBUG_enter("int ALoopVarList::assignToIteration(int iter)")
    
    size=maxIteration();

    if (iter>size)  
    { ok=0; }
    else
    {
        ok=1;
        for (Iterator i(this);i;i++) /* gehe durch alle "a,b,c in {..}" Konstrukte */
        {
            Pair   *p;
            List   *idList;
            AGroup *group;
            int groupSize;
            ListObject * actualC; /* holds the actual constant in the group */
        
            p     =Pair::cast(ListObject::cast(i)->getObject());
            idList=List::cast(p->getLeft());           /* ist "a,b,c" */
            group =AGroup::cast(p->getRight());        /* ist "{...}" */
            groupSize=group->getSize();

            actualC=ListObject::cast( group->at( ((iter-1)%groupSize)+1 ) );

            for(Iterator j(idList);j;j++)
            {
                AVariable *av;
                
//                printf("%s\n",BString::cast(ListObject::cast(j)->getObject())->content());
                av=AVariable::cast(identifier->getValueAt(ListObject::cast(j)->getObject()));
// 1:                av->newInstance();
                
                (new StAssign(
                                new Pair(av,new List()) ,
                                AConstant::cast( actualC->getObject() )
                             )
                )->logic(cond);
                
                actualC=ListObject::cast(actualC->next());
                if (NULL==actualC) { actualC=ListObject::cast(group->first()); }
            }
        }
    }

    DEBUG_exit("int ALoopVarList::assignToIteration(int iter)")
    return ok;
}


int ALoopVarList::setToIteration(int iter)
{
    int ok,size;
    DEBUG_enter("int ALoopVarList::setToIteration(int iter)")
/*
    size=AGroup::cast(
                Pair::cast(
                    ListObject::cast(
                        this->first()
                    )->getObject()
                )->getRight()
            )->getSize();
*/
    size=maxIteration();

    if (iter>size) 
    { ok=0; }
    else
    {
        ok=1;
        for (Iterator i(this);i;i++) /* gehe durch alle "a,b,c in {..}" Konstrukte */
        {
            Pair   *p;
            List   *idList;
            AGroup *group;
            int groupSize;
            ListObject * actualC; /* holds the actual constant in the group */
        
            p     =Pair::cast(ListObject::cast(i)->getObject());
            idList=List::cast(p->getLeft());           /* ist "a,b,c" */
            group =AGroup::cast(p->getRight());        /* ist "{...}" */
            groupSize=group->getSize();

            actualC=ListObject::cast( group->at( ((iter-1)%groupSize)+1 ) );

            for(Iterator j(idList);j;j++)
            {
                AVariable *av;
//                printf("%s\n",BString::cast(ListObject::cast(j)->getObject())->content());
                av=AVariable::cast(identifier->getValueAt(ListObject::cast(j)->getObject()));
// 1:                av->newInstance();
                av->setConstant(AConstant::cast(actualC->getObject()));
                actualC=ListObject::cast(actualC->next());
                if (NULL==actualC) { actualC=ListObject::cast(group->first()); }
            }
        }
    }

    DEBUG_exit("int ALoopVarList::setToIteration(int iter)")
    return ok;
}

void ALoopVarList::assignResults(List * cond)
{
    int cNum;
    
    DEBUG_enter("void ALoopVarList::assignResults(List * cond)")

    for( Iterator cPair(this) ; cPair ; cPair++ )
    {
        List * cVar=
            List::cast(
                Pair::cast(
                    ListObject::cast(cPair)->getObject()
                )->getLeft()
            );
        for( Iterator singleVar(cVar) ; singleVar ; singleVar++ )
        { 
            AVariable::cast( 
                identifier->getValueAt(BString::cast(ListObject::cast(singleVar)->getObject()))
            )->newInstance(); }         
    }
    
    fprintf(fout,"; side effect:\n*FUNCTION-TABLE\n");
    fprintf(fout,"$HEADER :\n X [");
    for( Iterator i(cond) ; i ; i++ )
    {
        if ( i!=cond->first() ) { fprintf(fout,","); }
        fprintf(fout," %s ",
            AVariable::cast(ListObject::cast(i)->getObject())->getName()->content()
        );
    }
    fprintf(fout,"] : Y ;\n");

    cNum=1;
    for( Iterator c(cond) ; c ; c++ )
    {
        int i;
        int isFirst;

        fprintf(fout," X");
        i=1;
        while(i<cNum) 
        {
            fprintf(fout," 0");
            i++;
        }
        fprintf(fout," 1");
        i++;
        while(i<=cond->getSize())
        {
            fprintf(fout," -");
            i++;
        }
        

        fprintf(fout," : Y ");

        isFirst=1;

        for( Iterator j(this) ; j ; j++ ) /* ueber alle "a,b,c in {..}" */
        {
            List * idList;
            AGroup * constList;
            int constPos;
            
            idList=
                List::cast(
                    Pair::cast(
                        ListObject::cast(j)->getObject()
                    )->getLeft()                
                );
        
            constList=
                AGroup::cast(
                    Pair::cast(
                        ListObject::cast(j)->getObject()
                    )->getRight()                
                );

            constPos=cNum;
        
            for( Iterator k(idList) ; k ; k++ )  /* ueber alle "a,b,c" */
            {
                AVariable * tVar;
            
                tVar=AVariable::cast(
                        identifier->getValueAt(
                            BString::cast(ListObject::cast(k)->getObject())
                        )
                    );

                if ( !isFirst ) 
                { fprintf(fout,","); }

                isFirst=0;

                tVar->setConstant(NULL);
            
                fprintf(fout," %s = %s ", 
                    tVar->getName()->content(),
                    tVar->getType()->codingOf(
                        AConstant::cast(
                            ListObject::cast(
                                constList->at(((constPos-1)%(constList->getSize()))+1)
                            )->getObject()
                        )
                    )->content()
                );

                constPos++;
            }        
        }
        
        fprintf(fout," ;\n");
        cNum++;
    }


//    fprintf(fout," X $REST : Y - ;\n\n");
    fprintf(fout,"\n");
    
    DEBUG_exit("void ALoopVarList::assignResults(List * cond)")
}

