/*
 * $Log$
 *
 */

/* $Id$ */ 

#include "../main/debug.h"
#include "../klassen/token.h"
#include "../library/bstring.h"


#define SUPERCLASS BString
INITIALIZE_CONCRETE_CLASS(Token, SUPERCLASS);

Token::Token(void)
{
    DEBUG_enter("Token::Token(void)")
    theString=NULL;
    carbonCopy("");
    linenumber=0;
    filename=NULL;
    DEBUG_exit("Token::Token(void)")
}

Token::Token(char *s,int l, char * f)
{
    DEBUG_enter("Token::Token(char *s,int l, char * f)")
    theString=NULL;
    carbonCopy(s);
    linenumber=l;
    filename=new char[strlen(f)+1];
    strcpy(filename,f);
    DEBUG_exit("Token::Token(char *s,int l, char * f)")
}

Token::~Token()
{
    DEBUG_enter("Token::~Token()")
    delete theString;
    delete filename;
    DEBUG_exit("Token::~Token()")
}

int Token::getLineNumber()
{
    return linenumber;
}

char * Token::getFileName()
{
    DEBUG_enter("char * Token::getFileName()")
    DEBUG_exit("char * Token::getFileName()")
    return filename;
}

