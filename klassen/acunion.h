/*
 * $Log$
 *
 */

/*$Id$*/

#ifndef ACONUNION_H
#define ACONUNION_H

#include "../klassen/aconstnt.h"
#include "../klassen/atunion.h"
#include "../library/bstring.h"
#include "../klassen/acbool.h"


class AConUnion : public AConstant
{
	protected:
		BString   * myTag;
		AConstant * myConstant;
		
	public:
		AConUnion();
		AConUnion(BString* tag, Object * constant);

		virtual AType     * getType();
		virtual BString   * asString();
		virtual AConBool  * operator == (Object*);
		virtual BString   * getTag();
		virtual AConstant * asConstant();

	DEFAULT_CONCRETE_METHODS(AConUnion,AConstant);
};

#endif
