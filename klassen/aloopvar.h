/*
 * $Log$
 *
 */

/*
 * ist eine Liste von Paaren ( Liste mit identifiern, gruppe )
 */

/*$Id$*/

#ifndef ALOOPVARLIST_H
#define ALOOPVARLIST_H

#include "../library/list.h"
#include "../klassen/avariabl.h"

class ALoopVarList : public List
{
	public:
		ALoopVarList();

		virtual int maxIteration();

		/* Rueckgabewert: 1 bei Erfolg, 0 bei Misserfolg */
	
		virtual int  setToIteration(int iter);
		virtual int  assignToIteration(int iter,AVariable* cond);
		virtual void assignResults(List * cond);
		
	DEFAULT_CONCRETE_METHODS(ALoopVarList,List);
};

#endif
