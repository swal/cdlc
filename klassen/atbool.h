/*
 * $Log$
 *
 */

/*$Id$*/

#ifndef ATYPE_BOOL_H
#define ATYPE_BOOL_H

#include "../klassen/atype.h"
#include "../library/list.h"
#include "../library/listobj.h"
#include "../klassen/acbool.h"


class ATypeBool : public AType
{
	protected:
		virtual AGroup  * asGroupC();

	public:
		ATypeBool();
		virtual BString * writeComponentNames(BString* head);

		virtual BString * codingOf(AConstant*);
		virtual int       getNumberOfBits();
		virtual int       calcIndex(int i);
		virtual int       writeXCS(FILE *xcs,char *ind,int i);

	DEFAULT_CONCRETE_METHODS(ATypeBool,AType);
};

#endif
