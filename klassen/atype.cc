/*
 * $Log$
 *
 */

/* $Id$ */ 

#include "atype.h"
#include "../library/bstring.h"
#include "../klassen/atbool.h"

#define SUPERCLASS Object

INITIALIZE_ABSTRACT_CLASS(AType,SUPERCLASS);

AType::AType()
{
	asGroupCache=NULL;
	asGroupTag=0;
}

AGroup  * AType::asGroup()
{
	if (0==asGroupTag)
	{
		asGroupCache=asGroupC();
		asGroupTag=1;
	}
	return asGroupCache;
}

int AType::positionOfConstant(AConstant*c,int *i)
{
	int code;
	int ret;
	int pos;

	DEBUG_enter("int AType::positionOfConstant(AConstant*c,int *i)")

	code=0;
	pos=1;

	for ( Iterator j( this->asGroup() ) ; j && (0==code) ; j++ )
	{
		AConstant * c2;

		c2=AConstant::cast(ListObject::cast(j)->getObject());

		if (c==c2)
		{ code=pos; }
		else
		{
			if               /* nicht schoen, aber selten */
				( 0==strcmp(
					c->asString()->content(),
					c2->asString()->content()
				) )  
			{ code=pos; }
		}

		pos++;
	}

	if (0==code)
	{ error("positionOfConstant not found"); }
	
	code--;

	ret=(*i)*code;

	(*i)=calcIndex(*i);

	DEBUG_exit("int AType::positionOfConstant(AConstant*c,int *i)")

	return ret;
}

