/*
 * $Log$
 *
 */

/*$Id$*/

#ifndef ACONSTANT_H
#define ACONSTANT_H

#include "../library/object.h"
#include "../express/express.h"
#include "../klassen/atype.h"

class AConBool;

class AConstant : public Expression
{
	public:
		virtual AConstant * asConstant();  /* return this */
		virtual BString   * asString()            = 0 ;
		virtual AConBool  * operator == (Object*) = 0 ;
		virtual AVariable * logic();
		
	
	DEFAULT_ABSTRACT_METHODS(AConstant,Expression);
};

#endif
