/*
 * $Log$
 *
 */

/* $Id$ */ 

#include "agroup.h"
#include "../library/bstring.h"
#include "../library/listobj.h"
#include "../klassen/aconstnt.h"
#include "../grammar/parser.h"


#define SUPERCLASS List
INITIALIZE_CONCRETE_CLASS(AGroup, SUPERCLASS);


AGroup::AGroup()  { }


Collection * AGroup::add (Object *o)
{
    if (! ListObject::cast(o)->getObject()->isKindOf(AConstant::ClassName) )
    { semerror(ListObject::cast(o)->getObject(),"only constants allowed in groups"); }
    
    if (0!=getSize())   /* check for type equality */
    {
        if ( !
              Expression::cast(ListObject::cast(first())->getObject())->getType()->
            isKindOf(
BString::cast(Expression::cast(ListObject::cast( o     )->getObject())->getType()->className())
            )
           )
        { semerror(NULL /*ListObject::cast(o)->getObject()*/,"all constants in a group must have the same type"); }
    }
    
    return addTail(ListNode::cast(o));
}
