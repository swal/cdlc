/*
 * $Log$
 *
 */

/* $Id$ */ 

#include "acbool.h"
#include "../library/bstring.h"
#include "../main/debug.h"



#define SUPERCLASS AConstant
INITIALIZE_CONCRETE_CLASS(AConBool, SUPERCLASS);


AConBool::AConBool()
{
    DEBUG_enter("AConBool::AConBool()")
    myBool=0;
    DEBUG_exit("AConBool::AConBool()")
}

AConBool::AConBool(int i)
{
    DEBUG_enter("AConBool::AConBool(int i)")
    if (0==i)
    { myBool=0; }
    else
    { myBool=1; }
    DEBUG_exit("AConBool::AConBool(int i)")
}

int AConBool::value()
{
    DEBUG_enter("int AConBool::value()")
    DEBUG_exit("int AConBool::value()")
    return myBool;
}

AType * AConBool::getType()
{
    DEBUG_enter("AType * AConBool::getType()")
    DEBUG_exit("AType * AConBool::getType()")
    return new ATypeBool();
}

BString * AConBool::asString()
{
    BString * s;
    DEBUG_enter("BString * AConBool::asString()")

    if (0==myBool)
    { s=new BString("false"); }
    else
    { s=new BString("true "); }
    DEBUG_exit("BString * AConBool::asString()")
    return s;
}

AConBool * AConBool::operator == ( Object* o )
{
    AConstant * con;
    AConBool * cache;

    DEBUG_enter("AConBool * AConBool::operator == ( Object* o )")

    con=Expression::cast(o)->asConstant();
    
    if (!con->isKindOf(AConBool::ClassName))
    {
        cache=new AConBool(0);
    }
    else if ( (AConBool::cast(con)->value()) == (this->value()) )
    {
        cache=new AConBool(1);
    }
    else
    {
        cache=new AConBool(0);
    }

    DEBUG_exit("AConBool * AConBool::operator == ( Object* o )")

    return cache;
}

