/*
 * $Log$
 *
 */

/*$Id$*/

#ifndef ATYPE_INT_H
#define ATYPE_INT_H

#include "../library/integer.h"
#include "../library/bstring.h"
#include "../klassen/atrange.h"
#include "../klassen/acint.h"
#include "../klassen/agroup.h"
#include "../main/error.h"
#include "../main/debug.h"
#include "../grammar/parser.h"


class ATypeInt : public ATypeRange
{
	public:
		ATypeInt();
		
		virtual int      writeXCS(FILE *xcs,char *ind,int i);

	DEFAULT_CONCRETE_METHODS(ATypeInt,ATypeRange);
};

#endif
