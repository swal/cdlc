/*
 * $Log$
 *
 */

/* $Id$ */ 

#include "avariabl.h"
#include "../library/bstring.h"
#include "../library/listobj.h"
#include "../klassen/atrecord.h"
#include "../klassen/atunion.h"
#include "../klassen/atrange.h"
#include "../klassen/atenum.h"
#include "../main/main.h"


#define SUPERCLASS Expression
INITIALIZE_CONCRETE_CLASS(AVariable, SUPERCLASS);


AVariable::AVariable()
{
	DEBUG_enter("AVariable::AVariable()")
	myType=NULL;
	myName=NULL;
	myConstant=NULL;

	recVar=NULL;
	
	generation=1;
	
	DEBUG_enter("AVariable::AVariable()")
}


AVariable::AVariable(BString* name,AType * type)
{
	int i;
	AType *at;
	BString *b;
	
	DEBUG_enter("AVariable::AVariable(BString* name,AType * type)")
	myName=name;
	myType=type;

	if (myType->isKindOf(ATypeRecord::ClassName))
	{
		recVar=new List();
		i=1;
		do
		{
			at=ATypeRecord::cast(myType)->getTypeAt(i);
			if (NULL!=at)
			{
				b=new BString();
				b->append(myName);
				b->append(new BString("_"));
				b->append(ATypeRecord::cast(myType)->getNameAt(i));
				recVar->add(new ListObject( new AVariable(b,at) ));
			}
			i++;
		}
		while (NULL!=at);
	}
	else
	{
	if (myType->isKindOf(ATypeUnion::ClassName))
	{
		recVar=new List();
		i=1;
		do
		{
			at=ATypeUnion::cast(myType)->getTypeAt(i);
			if (NULL!=at)
			{
				b=new BString();
				b->append(myName);
				b->append(new BString("_"));
				  /*  b->append(ATypeRecord::cast(myType)->getNameAt(i)); */
				recVar->add(new ListObject( new AVariable(b,at) ));
			}
			i++;
		}
		while (NULL!=at);
	}

	else
	{ recVar=NULL; }
	}
	
	generation=1;
	myConstant=NULL;
	DEBUG_exit("AVariable::AVariable(BString* name,AType * type)")
}


void AVariable::newInstance()
{
	DEBUG_enter("void AVariable::newInstance()")
	if (myType->isKindOf(ATypeRecord::ClassName))
	{
		for ( Iterator i(recVar) ; i ; i++ )
		{
			AVariable::cast(ListObject::cast(i)->getObject())->newInstance();
		}
	}
	else
	{
		generation++;
		myConstant=NULL;
		generateLocal(this);
	}

	DEBUG_exit("void AVariable::newInstance()")
}


void AVariable::setConstant(AConstant * c)
{
	int j;
	
	DEBUG_enter("void AVariable::setConstant(AConstant * c)")
	
	if (myType->isKindOf(ATypeRecord::ClassName))
	{
		j=1;
		for ( Iterator i(recVar) ; i ; i++ )
		{ 
			Object * actO;
			AConstant * actC;

			if (NULL==c) { actC=NULL; }
			else
			{
			  actO=AConRecord::cast(c)->at(j);
			  if (NULL==actO) { error("record constant has wrong dimension"); }
			  actC=AConstant::cast( ListObject::cast(actO)->getObject() );

			}

			AVariable::cast(
				ListObject::cast(i)->getObject()
			)->setConstant( actC ) ;
			j++;
		}
	}
	else
	{
		myConstant=c;
	}
	DEBUG_exit("void AVariable::setConstant(AConstant * c)")
}


AConstant * AVariable::asConstant()
{
	AConstant * c;
	DEBUG_enter("AConstant * AVariable::asConstant()")
	if (optimise)
	{
		if (myType->isKindOf(ATypeRecord::ClassName))
		{
			List * l;
			AConstant * tc;
			int allConst;

			allConst=1;
			l=new List();
			for ( Iterator i(recVar) ; i ; i++ )
			{
				tc=AVariable::cast( ListObject::cast(i)->getObject() )->asConstant();
				if (NULL==tc) { allConst=0; }
				l->add( new ListObject(tc) );
			}
			if (1==allConst)
			{ c=new AConRecord(l); }
			else
			{ c=NULL; }
		}
		else
		{
			c=myConstant;
		}
	}
	else
	{ c=NULL; }
	DEBUG_exit("AConstant * AVariable::asConstant()")
	return c;
}


AType * AVariable::getType()
{
	DEBUG_enter("AType * AVariable::getType()")
	DEBUG_exit("AType * AVariable::getType()")
	return myType;
}


AVariable * AVariable::logic()
{
	DEBUG_enter("AVariable * AVariable::logic()")
	DEBUG_exit("AVariable * AVariable::logic()")
	return this;
}


AVariable * AVariable::getSubName(BString* sub)
{
	AVariable * subVar;
	int pos;

	DEBUG_enter("AVariable * AVariable::getSubName(BString* sub)")

	if (myType->isKindOf(ATypeRecord::ClassName))
	{

		pos=ATypeRecord::cast(myType)->getPositionOf(sub);

		if (0==pos)
		{ semerror(sub,"record member not found"); }
		subVar=AVariable::cast(ListObject::cast(recVar->at(pos))->getObject());
	}
	else
	{
		if (myType->isKindOf(ATypeUnion::ClassName))
		{
			pos=ATypeUnion::cast(myType)->getPositionOf(sub);

			if (0==pos)
			{ semerror(sub,"union member not found"); }
			subVar=AVariable::cast(ListObject::cast(recVar->at(pos))->getObject());
		}
		else
		{ error("AVariable::getSubName: this Type doesnt have SubNames"); }
	}
	
	DEBUG_exit("AVariable * AVariable::getSubName(BString* sub)")
	return subVar;
}


BString * AVariable::getName()
{
	char s[100];
	BString * b;
	int first;

	DEBUG_enter("BString * AVariable::getName()")
	
	if (NULL==asConstant())
	{   /* schade, ich bin keine Konstante */
		
		sprintf(s,"_%u",generation);
		b=new BString(s);
		b->append(myName);
		b->append(new BString("_"));

		if (myType->isKindOf(ATypeRecord::ClassName))
		{
			BString *n;
			n=new BString("[ ");
/*
			n->append(myType->writeComponentNames(b));
*/
			first=1;
			for ( Iterator i(recVar) ; i ; i++ )
			{
				if (1!=first)
				{ n->append(new  BString(",")); }
				first=0;
				n->append(
					AVariable::cast( ListObject::cast(i)->getObject() )
					->getName()
				);
			}
			n->append(new BString(" ]"));
			b=n;
		}
		else if (
				 (myType->isKindOf(ATypeRange::ClassName)) ||
				 (myType->isKindOf(ATypeEnum::ClassName))
				)
		{
			BString *n;
			n=new BString("");
			n->append(myType->writeComponentNames(b));
			b=n;
		} 
	}
	else   /* wenn ich eine Konstante bin, soll deren Wert erscheinen */
	{
		b=myType->codingOf(asConstant());
	}

	DEBUG_exit("BString * AVariable::getName()")

	return b;
}


BString * AVariable::getBitName(int bit)
{
	BString * bs;
	char sbit[10];
	
	DEBUG_enter("BString * AVariable::getBitName(int bit)")

	if (!myType->isKindOf(ATypeRange::ClassName))
	{ error("getBitName is for numbers only"); }
	
	if (NULL==asConstant())
	{
		sprintf(sbit,"%u",bit);
		bs=getBaseName();
		bs->append(new BString(sbit));
	}
	else
	{
		int max=ATypeRange::cast(myType)->getNumberOfBits();
		bs=getName()->copy(max-bit-1,max-bit);
	}
	DEBUG_exit("BString * AVariable::getBitName(int bit)")
	return bs;
}


BString * AVariable::getName(AType * type)
{
	char s[100];
	BString * b;

	DEBUG_enter("BString * AVariable::getName(AType * type)")
	
	if(NULL==asConstant())
	{ /* schade, ich bin keine Konstante */
		
		sprintf(s,"_%u",generation);
		b=new BString(s);
		b->append(myName);
		b->append(new BString("_"));

		if (myType->isKindOf(ATypeRecord::ClassName))
		{
			b=getName();
		}
		else
		{
			if (myType->isKindOf(ATypeRange::ClassName))
			{
				BString *n;
				
				n=new BString("");
				
				if ( myType->getNumberOfBits()<type->getNumberOfBits() )
				{
					n->append(new BString("["));
					for(
						int i=1 ;
						i<=(type->getNumberOfBits()-myType->getNumberOfBits()) ;
						i++
					   )
					{
						if (ATypeRange::cast(myType)->isSigned())
						{
							char sign[100];
							n->append(getBaseName());
							sprintf(sign,"%u",myType->getNumberOfBits());
							n->append(new BString(sign))->append(new BString(","));
						}
						else
						{ n->append(new BString("_0,")); }
					}
					n->append(myType->writeComponentNames(b))->append(new BString("]"));
				}
				else
				{
//                  n->append(myType->writeComponentNames(b));
					n->append(type->writeComponentNames(b));
				}
				b=n;
			}
			else  // TypeRange
			{
				if (myType->isKindOf(ATypeEnum::ClassName))    
				{
					BString *n;
					n=new BString("");
					n->append(myType->writeComponentNames(b));
					b=n;
				}
			}
		} 
	}
	else /* wenn ich eine Konstante bin, soll deren Wert erscheinen */
	{
		b=type->codingOf(asConstant());
	}

	DEBUG_exit("BString * AVariable::getName(AType * type)")

	return b;
}


BString * AVariable::getBaseName()
{
	BString * cache;
	char s[10];
	
	DEBUG_enter("BString * AVariable::getBaseName()")
	sprintf(s,"_%u",generation);
	cache=new BString(s);
	cache->append(myName);
	cache->append(new BString("_"));
	DEBUG_exit("BString * AVariable::getBaseName()")
	
	return cache;
}

