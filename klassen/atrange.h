/*
 * $Log$
 *
 */

/*$Id$*/

#ifndef ATYPE_RANGE_H
#define ATYPE_RANGE_H

#include "../library/integer.h"
#include "../library/bstring.h"
#include "../klassen/atype.h"
#include "../klassen/acint.h"
#include "../klassen/agroup.h"
#include "../main/error.h"
#include "../main/debug.h"
#include "../grammar/parser.h"


class ATypeRange : public AType
{
	protected:
		int lower, upper;
		virtual AGroup  * asGroupC();
		
	public:
		ATypeRange();
		ATypeRange(Integer * lo, Integer * hi);
		
		virtual BString * writeComponentNames(BString* head);
		virtual BString * codingOf(AConstant*);
		virtual Integer * getLower();
		virtual Integer * getUpper();
		virtual int       getNumberOfBits();
		virtual int       isSigned();   /* has sign bit */
		virtual int       calcIndex(int i);
		virtual int       writeXCS(FILE *xcs,char *ind,int i);
		virtual int       positionOfConstant(AConstant*c,int *i);


	DEFAULT_CONCRETE_METHODS(ATypeRange,AType);
};

#endif
