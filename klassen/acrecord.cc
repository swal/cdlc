/*
 * $Log$
 *
 */

/* $Id$ */ 

#include "acrecord.h"
#include "../klassen/acbool.h"
#include "../library/bstring.h"
#include "../library/listobj.h"
#include "../express/express.h"
#include "../main/debug.h"



#define SUPERCLASS AConstant
INITIALIZE_CONCRETE_CLASS(AConRecord, SUPERCLASS);


AConRecord::AConRecord()
{
    DEBUG_enter("AConRecord::AConRecord()")
    myList=NULL;
    DEBUG_exit("AConRecord::AConRecord()")
}


AConRecord::AConRecord(Object * list)
{
    DEBUG_enter("AConRecord::AConRecord(Object * list)")
    if (!list->isKindOf(List::ClassName))
    { error("argument of AConRecord ist't a list"); }
    for (Iterator i(List::cast(list));i;i++)
    {
        if (!ListObject::cast(i)->getObject()->isKindOf(AConstant::ClassName))
        { error ("a member of argument list of AConRecord isn't a constant"); }
    }
    myList=List::cast(list);
    DEBUG_exit("AConRecord::AConRecord(Object * list)")
}

Object * AConRecord::at(int i)
{
    Object * o;
    DEBUG_enter("Object * AConRecord::at(int i)")
    if (NULL==myList) fatalError("bad initialised AConRecord");
    o=myList->List::at(i);
    DEBUG_exit("Object * AConRecord::at(int i)")
    return o;
}

AType * AConRecord::getType()
{
    List * l;
    DEBUG_enter("AType * AConRecord::getType()")
    l=new List();
    for(Iterator i(myList);i;i++)
    {
        l->add(new ListObject(new Pair(NULL,
        AConstant::cast(ListObject::cast(i)->getObject())->getType()
        )));
    }
    DEBUG_exit("AType * AConRecord::getType()")
    return new ATypeRecord(l);
}

BString * AConRecord::asString()
{
    BString * s;

    DEBUG_enter("BString * AConRecord::asString()")

    s=new BString("");

    for( int i=1 ; NULL!=myList->at(i) ; i++ )
    {
        DEBUG_msg("in AConRecord::asString()")
        s->append(
            Expression::cast(
                ListObject::cast(
                    myList->at(i)
                )->getObject()
            )->asConstant()->asString() 
        );
    }
            
    DEBUG_exit("BString * AConRecord::asString()")

    return s;
}

AConBool * AConRecord::operator == ( Object* o )
{
    int equal;
    int pos;

    DEBUG_enter("AConBool * AConRecord::operator == ( Object* o )")

    equal=1;
    pos=1;

    for ( Iterator i(myList) ; i && (!equal) ; i++ )
    {
        equal=
        AConBool::cast(
        (*AConstant::cast(ListObject::cast(i)->getObject()))==
        AConstant::cast(ListObject::cast(AConRecord::cast(o)->at(pos))->getObject())
        )->value();

        pos++;
    }


    DEBUG_exit("AConBool * AConRecord::operator == ( Object* o )")

    return new AConBool(equal);
}

