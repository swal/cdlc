/*
 * $Log$
 *
 */

/* $Id$ */ 

#include "atbool.h"
#include "../library/bstring.h"


#define SUPERCLASS AType
INITIALIZE_CONCRETE_CLASS(ATypeBool, SUPERCLASS);


ATypeBool::ATypeBool()
{ 
	DEBUG_enter("ATypeBool::ATypeBool()")
	DEBUG_exit("ATypeBool::ATypeBool()")
}

BString * ATypeBool::writeComponentNames(BString* head)
{
	DEBUG_enter("BString * ATypeBool::writeComponentNames(BString* head)")
	DEBUG_exit("BString * ATypeBool::writeComponentNames(BString* head)")
	return head;
}

AGroup* ATypeBool::asGroupC()
{
	AGroup * g;

	DEBUG_enter("AGroup* ATypeBool::asGroupC()")
	g=new AGroup();
	g->add(new ListObject(new AConBool(0)));
	g->add(new ListObject(new AConBool(1)));
	DEBUG_exit("AGroup* ATypeBool::asGroupC()")
	return g;
}

BString * ATypeBool::codingOf(AConstant*c)
{
	BString * b;

	DEBUG_enter("BString * ATypeBool::codingOf(AConstant*c)")
	if (0==AConBool::cast(c)->value())
	b=new BString("0");
	else
	b=new BString("1");
	DEBUG_exit("BString * ATypeBool::codingOf(AConstant*c)")

	return b;
}

int ATypeBool::getNumberOfBits()
{
	return 1;
}

int ATypeBool::writeXCS(FILE *xcs,char *,int i)
{
	fprintf(xcs," boolean : %u\n",i);
	return calcIndex(i);
}


int ATypeBool::calcIndex(int i)
{
	return 2*i;
}

