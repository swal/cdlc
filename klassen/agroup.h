/*
 * $Log$
 *
 */

/*$Id$*/

#ifndef AGROUP_H
#define AGROUP_H

#include "../library/list.h"

/* a CDL-Group is a list of constants, which nust have the same type */

class AGroup : public List
{
	public:
		AGroup();

		virtual Collection * add (Object *o);

	DEFAULT_CONCRETE_METHODS(AGroup,List);
};

#endif
