/*
 * $Log$
 *
 */

/*$Id$*/

#ifndef ACONINT_H
#define ACONINT_H

#include "../klassen/aconstnt.h"
#include "../klassen/atrange.h"
#include "../library/integer.h"

class AConBool;

class AConInt : public AConstant
{
	protected:
		Integer * myInt;
	
	public:
		AConInt();
		AConInt( Integer * i );
		AConInt( int i );
		virtual int value();
		virtual AType* getType();
		virtual BString * asString();
		virtual AConBool * operator == (Object*);

	DEFAULT_CONCRETE_METHODS(AConInt,AConstant);
};

#endif
