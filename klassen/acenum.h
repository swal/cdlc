/*
 * $Log$
 *
 */

/*$Id$*/

#ifndef ACONENUM_H
#define ACONENUM_H

#include "../library/bstring.h"
#include "../klassen/aconstnt.h"
#include "../klassen/atenum.h"

class AConBool;


class AConEnum : public AConstant
{
	protected:
		BString* myName;
		ATypeEnum * myType;
	
	public:
		AConEnum();
		AConEnum(BString* name,AType * type);

		virtual BString  * asString();
		virtual AType    * getType();
		virtual AConBool * operator == (Object * o);

	DEFAULT_CONCRETE_METHODS(AConEnum,AConstant);
};

#endif
