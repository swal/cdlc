/*
 * $Log$
 *
 */

/* $Id$ */ 

#include "acunion.h"
#include "../klassen/acbool.h"
#include "../library/bstring.h"
#include "../main/debug.h"



#define SUPERCLASS AConstant
INITIALIZE_CONCRETE_CLASS(AConUnion, SUPERCLASS);


AConUnion::AConUnion()
{
    DEBUG_enter("AConUnion::AConUnion()")
    myTag=NULL;
    myConstant=NULL;
    DEBUG_exit("AConUnion::AConUnion()")
}

AConUnion::AConUnion(BString * tag, Object * constant )
{
    DEBUG_enter("AConUnion::AConUnion(BString * tag, Object * constant )")
    myTag=tag;
    myConstant=AConstant::cast(constant);
    DEBUG_exit("AConUnion::AConUnion(BString * tag, Object * constant )")
}

AType * AConUnion::getType()
{
    DEBUG_enter("AType * AConUnion::getType()")
    DEBUG_exit("AType * AConUnion::getType()")
    return myConstant->getType();
}

BString * AConUnion::asString()
{
    BString * bs=NULL;
    
    DEBUG_enter("BString * AConUnion::asString()")
    if (NULL==bs)
    {
	char s[255];
	strcpy(s,"'");
	strcat(s,myTag->content());
	strcat(s,"'");
	strcat(s,myConstant->asString()->content());
	bs=new BString(s);
    }
    DEBUG_exit("BString * AConUnion::asString()")
    return bs;
}

AConBool * AConUnion::operator == ( Object* o )
{
    AConBool *acb;

    DEBUG_enter("AConBool * AConUnion::operator == ( Object* o )")

    if (!o->isKindOf(AConUnion::ClassName))
    { error("argument for AConBool== is not AConUnion"); }
    if ((*asConstant())==(AConstant::cast(o)->asConstant()))
    { acb=new AConBool(1); }
    else
    { acb=new AConBool(0); }

    DEBUG_exit("AConBool * AConUnion::operator == ( Object* o )")
    return acb;
}

BString * AConUnion::getTag()
{
	return myTag;
}

AConstant * AConUnion::asConstant()
{
	return myConstant;
}
