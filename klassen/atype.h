/*
 * $Log$
 *
 */

/*$Id$*/

#ifndef ATYPE_H
#define ATYPE_H

#include "../library/object.h"
#include "../klassen/agroup.h"
#include "../klassen/aconstnt.h"
#include "../library/list.h"
#include "../library/listobj.h"


class AType : public Object
{
	protected:
				AGroup  * asGroupCache;
				int       asGroupTag;
		virtual AGroup  * asGroupC()=NULL;

	public:
				AType();

		virtual BString * writeComponentNames(BString* head)=NULL;
		virtual AGroup  * asGroup();
		virtual BString * codingOf(AConstant*)=NULL; /* returns the coding as a binary number */
		virtual int       getNumberOfBits()=NULL;
		virtual int       calcIndex(int i)=NULL;
		virtual int       writeXCS(FILE *xcs,char *ind,int i)=NULL;
		virtual int       positionOfConstant(AConstant*c,int *i);

	DEFAULT_ABSTRACT_METHODS(AType,Object);
};

#endif
