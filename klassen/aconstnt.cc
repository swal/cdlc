/*
 * $Log$
 *
 */

/* $Id$ */ 

#include "../main/debug.h"
#include "aconstnt.h"
#include "../klassen/acbool.h"
#include "../library/bstring.h"



#define SUPERCLASS Expression
INITIALIZE_ABSTRACT_CLASS(AConstant, SUPERCLASS);

AConstant * AConstant::asConstant()
{
  AConstant * ac;

  DEBUG_enter("AConstant::asConstant()")
  ac=this;
  DEBUG_exit("AConstant::asConstant()")
  return ac;
}

AVariable * AConstant::logic()
{
    AVariable * v;
    AType * codingType;
    
    DEBUG_enter("AVariable * AConstant::logic()")
    DEBUG_str(BString::cast(this->className())->content())

    codingType=this->getType(); /* *** falsch! */

//2    v=generateTempVariable(codingType); 

//
//    fprintf(fout,"; a constant:\n*BOOLEAN-EQUATIONS\n %s = %s ;\n\n",
//        v->getName()->content(), 
//        codingType
//            ->codingOf(this)  /* wie ist ihr Wert? als BString* */
//            ->content()
//    );

    v=new AVariable( new BString("") , codingType ); //2

    v->setConstant(this);

    DEBUG_exit("AVariable * AConstant::logic()")
    return v;
}

