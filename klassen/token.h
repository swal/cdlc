/*
 * $Log$
 *
 */

/*$Id$*/

#ifndef TOKEN_H
#define TOKEN_H

#include "../library/bstring.h"

class Token : public BString
{
	private:
		int linenumber;
		char * filename;

	public:
		Token(void);
		Token(char * s, int l, char * f);
		virtual ~Token();        
		virtual int getLineNumber();
		virtual char * getFileName();

	DEFAULT_CONCRETE_METHODS(Token,BString);
};

#endif
