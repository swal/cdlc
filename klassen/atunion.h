/*
 * $Log$
 *
 */

/*$Id$*/

#ifndef ATYPE_UNION_H
#define ATYPE_UNION_H


#include "../klassen/atype.h"
#include "../library/list.h"
#include "../library/pair.h"
#include "../library/listobj.h"
#include "../library/integer.h"
#include "../klassen/acrecord.h"
#include "../grammar/parser.h"


class ATypeUnion : public AType
{
	protected:
			List    * myList;
		virtual AGroup  * asGroupC();
	public:
		ATypeUnion();
		ATypeUnion(Object *);

		virtual int       getPositionOf(BString * tag);
		virtual AType   * getTypeAt(int pos);
		virtual BString * getNameAt(int pos);
		virtual BString * writeComponentNames(BString* head);
		virtual BString * codingOf(AConstant*);
		virtual int       getNumberOfBits(void);
		virtual int       calcIndex(int i);
		virtual int       writeXCS(FILE *xcs,char *ind,int i);
		virtual int       positionOfConstant(AConstant*c,int *i);
		
	DEFAULT_CONCRETE_METHODS(ATypeUnion,AType);
};

#endif
