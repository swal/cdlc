/*
 * $Log$
 *
 */

/*$Id$*/

#ifndef ACONRECORD_H
#define ACONRECORD_H

#include "../klassen/aconstnt.h"
#include "../klassen/atrecord.h"
#include "../library/list.h"
#include "../klassen/acbool.h"

/* extern class AConBool; geht nicht */

class AConRecord : public AConstant
{
	private:
		List* myList;
		
	public:
		AConRecord();
		AConRecord(Object * list);  /* list must be a 'List' of 'AConstant' */

		virtual Object   * at(int);
		virtual AType    * getType();
		virtual BString  * asString();
		virtual AConBool * operator == (Object*);

	DEFAULT_CONCRETE_METHODS(AConRecord,AConstant);
};

#endif
