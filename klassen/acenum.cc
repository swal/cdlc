/*
 * $Log$
 *
 */

/* $Id$ */ 

#include "acenum.h"
#include "../klassen/acbool.h"
#include "../klassen/atenum.h"
#include "../main/debug.h"



#define SUPERCLASS AConstant
INITIALIZE_CONCRETE_CLASS(AConEnum, SUPERCLASS);


AConEnum::AConEnum()
{
    myName=NULL;
    myType=NULL;
}

AConEnum::AConEnum(BString* name,AType *type)
{    
    myName=name;
    myType=ATypeEnum::cast(type);
}

BString* AConEnum::asString()
{
    return myName;
}

AType * AConEnum::getType()
{
    return myType;
}

AConBool * AConEnum::operator == ( Object * o )
{
    AConstant * con;

    DEBUG_enter("AConBool * AConEnum::operator == ( Object * o )")
    con=Expression::cast(o)->asConstant();
    DEBUG_msg("x")

    if ( !con->isKindOf(AConEnum::ClassName))
    {
        DEBUG_exit("AConBool * AConEnum::operator == ( Object * o ) -- false")
        return (new AConBool(0)); 
    }

    DEBUG_msg("other object isKindOf AConEnum")

    if ( 
        0!=strcmp(
            AConEnum::cast(con)->asString()->content(),
                                 asString()->content()
            )
       )
    { 
        DEBUG_exit("AConBool * AConEnum::operator == ( Object * o ) -- false")
        return (new AConBool(0)); 
    }

    DEBUG_exit("AConBool * AConEnum::operator == ( Object * o ) -- true")
    return (new AConBool(1)); 
}

