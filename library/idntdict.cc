/*
 * $Log$
 *
 */

/* $Id$ */

#include "idntdict.h"

#define SUPERCLASS Dictionary
INITIALIZE_CONCRETE_CLASS(IdentityDictionary, Dictionary);

IdentityDictionary::IdentityDictionary()
	:Dictionary()
{
}

IdentityDictionary::IdentityDictionary(int n)
	:Dictionary(n)
{
}

unsigned int IdentityDictionary::bestPosition(Object *o)
{
DEBUG_enter("IdentityDictionary::bestPosition");
	unsigned int i=o->hash() % slots;
	
	while(marker[i]==1 && !(key[i]->identical(o)))
	{
		i++;
		if(i>=slots)i=0;
	}
//cout << "bestPos=" << i << " , slots=" << slots << endl;  
DEBUG_exit("IdentityDictionary::bestPosition");
	return i;
}

Object * IdentityDictionary::getValueByEquality(Object *key)
{
  unsigned int i = Dictionary::bestPosition(key);
  return marker[i]==1 ? content[i] : NULL;
}

