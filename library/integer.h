
/*
 * $Log$
 *
 */

/*$Id$*/

#include "object.h"

#ifndef INTEGER_H
#define INTEGER_H

class Integer : public Object
{
	private:
		int val;
	public:
		Integer();
		Integer(int i);

		virtual unsigned int hash();
		virtual int          value();
		virtual void         setValue(int v);
		virtual Boolean      equal(Object *another);
		virtual operator int & ();
		virtual operator int ();

		DEFAULT_CONCRETE_METHODS(Integer,Object);
};

#endif
