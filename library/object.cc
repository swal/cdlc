/*
 * $Log: object.cc,v $
 *
 */

/* $Id$ */

/* 
// Ich habe einen Super-Coredump-Absturz bei fatalError eingebaut
// (damit man mit dem Debugger die Aufrufkette ansehen kann)
//
// wieder abgeklemmt, weil er in einer DOS-Box unter Windows den
// gesamten Rechner aufhaengt, einschliesslich 
// zerschiessen der FAT !
*/

#define OBJECT_C
#include "object.h"

/* extern "C" { void volatile exit(int); } */

Object::~Object() {}


BString * Object::ClassName; // wird in updateCreation() initialisiert

void fatalError(char *errorString)
{
	cerr << errorString << endl << flush;
//    volatile float x=0.0;
//    volatile float y=2.1;
//    y=y/x;
	exit(1);
}


Object * Object::newObject()
{
	error( "internal error: Object::newObject(): abstract class!");
	return NULL;
}

Boolean Object::isKindOf(Object /*BString*/ *s)
{
	int i;
	DEBUG_enter("Object::isKindOf");
	i=s->identical(className()) ? True : False;
	DEBUG_exit("Object::isKindOf");
	return i;
}

unsigned int Object::hash()
{
	unsigned int ui;
	DEBUG_enter("unsigned int Object::hash()")
	ui=(unsigned int) multiplicativehash((unsigned int)this);
	DEBUG_exit("unsigned int Object::hash()")
	return ui;
}

int Object::identical(Object *another)
{
	int eq;
	DEBUG_enter("int Object::identical(Object *another)")
	eq=another==this;
	DEBUG_exit("int Object::identical(Object *another)")
	return eq;
}

int Object::equal(Object *another)
{
	int eq;
	DEBUG_enter("int Object::equal(Object *another)")
	eq=identical(another);
	DEBUG_exit("int Object::equal(Object *another)")
	return eq;
}

class Object /*Bstring*/ * Object::className()
{
	Object * o;
	DEBUG_enter("")
	o=(Object *)ClassName;
	DEBUG_exit("")
	return o;
}

/*
extern IdentityDictionary * CreationFunctionDictionary;
*/
