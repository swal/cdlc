/*
 * $Log$
 *
 */

/* $Id$ */
 
#include "listnode.h"

#define SUPERCLASS Object
INITIALIZE_CONCRETE_CLASS(ListNode, SUPERCLASS);

ListNode::ListNode()
{
	DEBUG_enter("ListNode::ListNode()")
	successor=NULL;
	predecessor=NULL;
	DEBUG_exit("ListNode::ListNode()")
}

ListNode * ListNode::next()
{
	ListNode * s;

	if(NULL==successor)
	{ fatalError("next() of dummy-tail of list"); }

	if(NULL==successor->successor)
	{ s=NULL; }
	else
	{ s=successor; }

	return s;
}

ListNode * ListNode::previous()
{
	if(NULL==predecessor)
	{
		fatalError("previous() of dummy-head of list");
	}

	if(NULL==predecessor->predecessor)
	{
		return NULL;
	}
	else
	{
		return predecessor;
	}
}

void ListNode::remove()
{
	if(NULL==successor)
	{
		fatalError("ListNode not initialized (successor) ?");
	}
	if(NULL==predecessor)
	{
		fatalError("ListNode not initialized (predecessor) ?");
	}
		
	successor->predecessor=predecessor;
	predecessor->successor=successor;

	successor=NULL;
	predecessor=NULL;
}

void ListNode::insertAfter(ListNode *prev)
{
	if(NULL==prev)
	{
		fatalError("ListNode::insertAfter() : argument is NULL");
	}

	if(NULL==prev->successor)
	{
		fatalError("ListNode::insertAfter() : successor of argument is NULL");
	}

	successor=prev->successor;
	predecessor=prev;

	successor->predecessor=this;
	predecessor->successor=this;
}

void ListNode::insertBefore(ListNode *next)
{
	if(NULL==next)
	{
		fatalError("ListNode::insertBefore() : argument is NULL");
	}

	if(NULL==next->predecessor)
	{
		fatalError("ListNode::insertBefore() : predecessor of argument is NULL");
	}

	successor=next;
	predecessor=next->predecessor;

	successor->predecessor=this;
	predecessor->successor=this;
}

