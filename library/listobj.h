/*
 * $Log$
 *
 */

/*$Id$*/

#include "object.h"
#include "listnode.h"

#ifndef LISTOBJECT_H
#define LISTOBJECT_H

class ListObject: public ListNode
{
	protected:
		Object * theObject;

	public:
		ListObject();
		ListObject(Object * o);
		Object * getObject();
		void setObject(Object * o);

	DEFAULT_CONCRETE_METHODS(ListObject,ListNode);
};

#endif
