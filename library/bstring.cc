/*
 * $Log$
 *
 */

/* $Id$ */


#include "bstring.h"
#include "../main/debug.h"
/* #include <strings.h>     die SUN mag das nicht */
#include <string.h>      /* fuer bcopy */

#define SUPERCLASS Object
INITIALIZE_CONCRETE_CLASS(BString,SUPERCLASS)

BString::BString(char *s) 
{
	DEBUG_enter("BString::BString(char *s)")
	DEBUG_str(s)
	theString=NULL; 
	carbonCopy(s);
	DEBUG_exit("BString::BString(char *s)");
}


BString::BString()
{
	DEBUG_enter("BString::BString()")
	theString=NULL; 
	carbonCopy("");
	DEBUG_exit("BString::BString()")
}


BString::~BString()
{
	DEBUG_enter("BString::~BString()")
	if (NULL!=theString)
	{
		delete theString;
	}
	DEBUG_exit("BString::~BString()")
}

unsigned int BString::hash()
{
	unsigned int i;
	DEBUG_enter("BString::hash()")
	i=theString ? (unsigned int) hashpjw(theString) : 0;
	DEBUG_exit("BString::hash()")
	return i;
}

int BString::equal(Object *another)
{ 
	DEBUG_enter("BString::equal")
	if(this==another)
	{
		DEBUG_exit("BString::equal")
		return 1;
	}
	if(another->isKindOf(BString::ClassName))
	{
		if(theString)
		{
			DEBUG_exit("BString::equal")
//cout<<"this string contains: "<<theString<<endl;
//cout<<"the compare string contains: "<<BString::cast(another)->content()<<endl;
			return BString::cast(another)->content() ? 
					(strcmp(theString,
						BString::cast(another)->content())==0) 
					: 0;
		}
		else
		{
			DEBUG_exit("BString::equal")
			return BString::cast(another)->content() ? 0 : 1;
		}
	}
	else
	{
		DEBUG_exit("BString::equal")
		return 0;
	}
}


BString & BString::operator = (const BString & s)
{ 
	DEBUG_enter("BString & BString::operator = (const BString & s)")
	carbonCopy(s.theString); 
	DEBUG_exit("BString & BString::operator = (const BString & s)")
	return *this; 
}


BString & BString::operator = (char * s)
{ 
	DEBUG_enter("BString & BString::operator = (char * s)")
	if(s)
	{
		carbonCopy(s);
	} 
	else 
	{ 
		carbonCopy("");
	}
	DEBUG_exit("BString & BString::operator = (char * s)")
	return *this;
}

BString::operator char * () 
{ 
	return theString; 
}

char * BString::content() 
{ 
	return theString; 
}


void BString::carbonCopy(const char *s)
{
	DEBUG_enter("BString::carbonCopy")
	
	if (NULL!=theString)
	{
		delete theString;
	}
   
	if ( NULL==s )
	{
		theString=NULL;
	}  
	else
	{
		theString = new char[strlen(s)+1];
		if (NULL==theString)
		{ fatalError("out of memory"); }
		strcpy(theString,s);
	}
	DEBUG_exit("BString::carbonCopy")
}



// *************** schuckis funcs ******************

int BString::containsChar(char c)
{
	int contained;
	DEBUG_enter("BString::containsChar(char)")
	contained=(strchr(theString,(int)c)!=0);
	DEBUG_exit("BString::containsChar(char)")
	return contained;
}

BString * BString::copy(int from, int to)
{
	BString * subString;
	int size=to-from;
	char * subs;

	DEBUG_enter("BString::copy(int,int)")
	subs=new char[size+1]; 
	bcopy(&theString[from],subs,size);
	subs[size]=(char)0;
	subString= new BString(subs);
	delete subs;
	DEBUG_exit("BString::copy(int,int)")
	return subString;
}


int BString::getStringSize()
{
	int size;
	DEBUG_enter("int BString::getStringSize()")
	size=strlen(theString);
	DEBUG_exit("int BString::getStringSize()")

	return size;
}

int BString::subStringPosition(BString * subString)
{
	int pos=-1;
	char *ptr;
	
	ptr=strstr(theString, subString->content());
	if (ptr!=NULL)
	{
		pos=(int)(ptr-theString);
	}
	
	return pos;
}

BString * BString::splitAt(BString * splitString)
{
	BString * front=NULL;
	char * oldString;
	int sPos, cutIndex;
	
	sPos=subStringPosition(splitString);    
	if (sPos>=0)
	{
		front=copy(0,sPos);
		oldString = theString;
		cutIndex = sPos + strlen(splitString->content());
		theString = new char[strlen(&(oldString[cutIndex]))+1];
		strcpy(theString,&(oldString[cutIndex]));
		delete oldString;
	}
	
	return front;
}


int BString::occurencesOfChar(char c)
{
	int occ=0;
	int i;

	for (i=0;i<strlen(theString);i++)
	{
		if (theString[i]==c) occ++;
	}
	return occ;
}


BString * BString::append(BString *anotherString)
{
	if (!anotherString) return this;
	char *newStr = new char [anotherString->getStringSize()+getStringSize()+1];
	strcpy(newStr,theString);
	strcpy(&newStr[getStringSize()], anotherString->content());
	delete theString;
	theString = newStr;
	return this;
}

