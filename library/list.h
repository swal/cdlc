/*
 * $Log$
 *
 */

/*$Id$*/



#ifndef LIST_H
#define LIST_H

#include "../library/object.h"
#include "../library/listnode.h"
#include "../library/collectn.h"

class List : public Collection
{
	
	protected:   
		List(ListNode *h, ListNode *t);
		ListNode * head;
		ListNode * tail;

	public:
		List();
		
		virtual Object *     first();   /* returns first element of the list */
		virtual Object *     last();    /* returns last element of the list */
		virtual Object *     next(Object *);       
		virtual Object *     previous(Object *);
		virtual Object *     at(int);   /* returns element on position 1..n */
		virtual int          includes(Object *);   /* 0, if not.   1..n otherwise */
		virtual List *       addHead(ListNode *);
		virtual List *       addTail(ListNode *);
		virtual Collection * add (Object *o);
		virtual Collection * remove (Object *o);
		virtual int      getSize();

	DEFAULT_CONCRETE_METHODS(List,Collection);
};

#endif
