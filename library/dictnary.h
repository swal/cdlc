/*
 * $Log$
 *
 */

/*$Id$*/

#include "collectn.h"

#ifndef DICTIONARY_H
#define DICTIONARY_H

class Dictionary : public Collection
{
	private:
		int size;
		int currentIndex;

	protected:
		int slots;
		Object ** key;
		Object **content;
		int *marker;

		virtual unsigned int bestPosition(Object *o);

	public:
		Dictionary();
		Dictionary(int n);
		virtual Object * first();
		virtual Object * next(Object *o);
		virtual Collection * add (Object *); // cancelled
		virtual Collection * remove (Object *); // cancelled
		virtual Dictionary * addAssoc( Object *k, Object *v);
		virtual Dictionary * removeKey(Object *k);
		virtual Object * getValueAt(Object *k);
		virtual int includes(Object *k);
		virtual int getSize();
	DEFAULT_CONCRETE_METHODS(Dictionary,Collection);
};

#endif
