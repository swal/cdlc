/*
 * $Log$
 *
 */

/*$Id$*/

#include "dictnary.h"

#ifndef IDENTITYDICTIONARY_H
#define IDENTITYDICTIONARY_H

class IdentityDictionary : public Dictionary
{
	protected:
		virtual unsigned int bestPosition(Object *o);

	public:
		IdentityDictionary();
		IdentityDictionary(int n);

		virtual Object *getValueByEquality(Object *key);

	DEFAULT_CONCRETE_METHODS(IdentityDictionary, Dictionary);
};

#endif
