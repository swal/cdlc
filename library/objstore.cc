/*
 * $Log$
 *
 */

/* Persistente Objekte werden nicht benoetigt, deshalb ohne Funktion */

/* $Id$ */

#include "objstore.h"
#include "idntdict.h"

void CreationFunctionWrapper::trapForAbstractClasses() {}

IdentityDictionary * CreationFunctionDictionary=0;

BString *updateCreation(BString *classname,
			CreationFunction *cf
			)
{
//  cout << "Klasseninitialisierung von " << classname->content() << endl<<flush;
	static int initialized=0;

	if (initialized==0)
	{
		CreationFunctionDictionary = new IdentityDictionary(300);
		initialized=1;

		Object::ClassName=new BString("Object");
		BString::ClassName=new BString("BString");

		CreationFunctionWrapper *ocfw= new CreationFunctionWrapper;
		ocfw->creationFunction=Object::newObject;
		CreationFunctionDictionary
			->addAssoc(Object::ClassName,ocfw);

		CreationFunctionWrapper *bcfw= new CreationFunctionWrapper;
		bcfw->creationFunction=BString::newObject;
		CreationFunctionDictionary
			->addAssoc(BString::ClassName,bcfw);
	}

	CreationFunctionWrapper *cfw= new CreationFunctionWrapper;
	cfw->creationFunction=cf;
	CreationFunctionDictionary
		->addAssoc(classname,cfw);

	return classname;
}
