/*
 * $Log$
 *
 */

/*$Id$*/

#include "object.h"

#ifndef PAIR_H
#define PAIR_H

class Pair : public Object
{
	protected:
		Object *left, *right;
	public:
		Pair();
		Pair( Object* l, Object* r );

		void setRight(Object *);
		void setLeft(Object *);

		Object * getRight();
		Object * getLeft();

	DEFAULT_CONCRETE_METHODS(Pair,Object);
};

#endif

