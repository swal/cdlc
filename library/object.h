/*
 * $Log$
 *
 */

/* 
 * True und False eingefuehrt
 * Klasse Boolean durch ein #define simuliert
 */

/*$Id$*/

#ifndef OBJECT_H
#define OBJECT_H


#define Boolean int
#define True 1
#define False 0

#include <string.h>
#include <iostream.h>
#include <fstream.h>
#ifdef MSDOS
#include <builtin.h>
#else
#include "hash.h"
#endif

#include "../main/debug.h"
#include "../main/error.h"

typedef class Object *CreationFunction();

class Object
{
	private:
		static class BString * init();
		static Object *newObject();
		friend class BString *updateCreation(class BString *classname,CreationFunction *rf);
		virtual void trapForAbstractClasses()=0;

	public:
		virtual ~Object(void);
		static class BString * ClassName;

		virtual class Object /*Bstring*/ * className();
		virtual Boolean isKindOf(Object /*BString*/ *s);

		virtual unsigned int hash();
		virtual Boolean identical(Object *another);
		virtual Boolean equal(Object *another);
};


/* some additional standart methods: */

#define DEFAULT_METHODS1(CLASS,BASE)\
private:\
	static BString * init();\
	static Object *newObject();\
public:\
	static BString * ClassName;\
	virtual Object /*BString*/ * className();\
	virtual Boolean isKindOf (Object /*BString*/ *s);\
	static CLASS * cast(Object *o)\
//END_OF_MACRO

#define DEFAULT_CONCRETE_METHODS(CLASS,BASE)\
private:\
	virtual void trapForAbstractClasses();\
	DEFAULT_METHODS1(CLASS,BASE)\
//END_OF_MACRO

#define DEFAULT_ABSTRACT_METHODS(CLASS,BASE)\
private:\
	virtual void trapForAbstractClasses()=0;\
	DEFAULT_METHODS1(CLASS,BASE)\
//END_OF_MACRO


#define INITIALIZE_CLASS(CLASS,BASE)\
	BString * CLASS::ClassName=CLASS::init(); \
	BString * CLASS::init()\
	{ return updateCreation(new BString(#CLASS),CLASS::newObject); }\
\
	Object /*BString*/ * CLASS::className() \
	{ return (Object *)CLASS::ClassName; } \
\
	Boolean CLASS::isKindOf (Object /*BString*/ *s) \
	{ \
		Boolean b; \
		DEBUG_enter( #CLASS::isKindOf() ) \
		b=s->identical((Object*)CLASS::ClassName) ? True : BASE::isKindOf(s);\
		DEBUG_exit( #CLASS::isKindOf() ) \
		return b; \
	}\
\
	CLASS * CLASS::cast(Object *o) \
	{ \
		DEBUG_enter( #CLASS::cast() ) \
		if (o) \
		{ \
			DEBUG_msg("::cast()") \
			if ( o->isKindOf((Object *)ClassName) ) \
			{ DEBUG_exit( #CLASS::cast() ) return (CLASS *)o; } \
			else \
			{ fatalError(strcat("illegal cast to " #CLASS " from ", BString::cast(o->className())->content()) );}\
		} \
		DEBUG_exit( #CLASS::cast() ) \
		return NULL; \
	} \

// END_OF_MACRO


#define INITIALIZE_CONCRETE_CLASS(CLASS,BASE)\
	INITIALIZE_CLASS(CLASS,BASE)\
	Object * CLASS::newObject() { return new CLASS; }\
	void CLASS::trapForAbstractClasses() {}\
//END_OF_MACRO

#define INITIALIZE_ABSTRACT_CLASS(CLASS,BASE)\
	INITIALIZE_CLASS(CLASS,BASE)\
	Object * CLASS::newObject()\
		{ fatalError( #CLASS "::newObject(): abstract class!"); return 0; }\
//END_OF_MACRO


#define NOT_IMPLEMENTED(s) cerr << "*** not implemented : " << s << endl << flush;
#define NO_RETURN_VALUE(s) ((cerr << "*** no return value : " << s << endl << flush), 0)

void fatalError(char *errorString);

#include "bstring.h"
#include "integer.h"
#include "collectn.h"

BString *updateCreation(BString *classname,CreationFunction *rf);

#endif
