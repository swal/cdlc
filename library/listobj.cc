/*
 * $Log$
 *
 */

/* $Id$ */
 
#include "listobj.h"

#define SUPERCLASS ListNode
INITIALIZE_CONCRETE_CLASS(ListObject, SUPERCLASS);


ListObject::ListObject()
{
	DEBUG_enter("ListObject::ListObject()")
	theObject=NULL;
	ListNode();
	DEBUG_exit("ListObject::ListObject()")
}

ListObject::ListObject(Object * o)
{
	DEBUG_enter("ListObject::ListObject(Object * o)")
	theObject=o;
	ListNode();
	DEBUG_exit("ListObject::ListObject(Object * o)")
}

Object * ListObject::getObject()
{
	DEBUG_enter("Object * ListObject::getObject()")
	DEBUG_exit("Object * ListObject::getObject()")
	return theObject;
}

void ListObject::setObject(Object * o)
{
    DEBUG_enter("void ListObject::setObject(Object * o)")
    theObject=o;
    DEBUG_exit("void ListObject::setObject(Object * o)")
}

