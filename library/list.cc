/*
 * $Log$
 *
 */

/* $Id$ */

#include "list.h"

#define SUPERCLASS Object
INITIALIZE_CONCRETE_CLASS(List, SUPERCLASS)

List::List()
{
	DEBUG_enter("List::List()")
	head = new ListNode;
	tail = new ListNode;

	head->successor=tail;
	tail->predecessor=head;
	DEBUG_exit("List::List()")
}

List::List(ListNode *h, ListNode *t)
{
	DEBUG_enter("List::List(ListNode *h, ListNode *t)")        
	head = h;
	tail = t;

	head->successor=tail;
	tail->predecessor=head;
	DEBUG_exit("List::List(ListNode *h, ListNode *t)")
}

Object * List::first()
{
		return head->next();
}

Object * List::last()
{
		return tail->previous();
}

Object * List::next(Object *element)
{
		ListNode * listNode = ListNode::cast(element);

		return listNode->next();
}

Object * List::at(int pos)
{
	Object * act;

	DEBUG_enter("Object * List::at(int pos)")
	if (1>pos) 
	{ 
		DEBUG_exit("Object * List::at(int pos) 'pos<1'")
		return NULL; 
	}
	else
	{
		act=first();
		for (int i=2;i<=pos;i++)
		{
			act=this->next(act);
			if (act==tail) 
			{ 
				DEBUG_exit("Object * List::at(int pos) 'pos>getSize()'")
				return NULL; 
			}
		}
	}
	DEBUG_exit("Object * List::at(int pos)")
	return act;   
}

Object * List::previous(Object *element)
{
		ListNode * listNode = ListNode::cast(element);

		return listNode->previous();
}

int List::includes(Object *element)
{
	int pos;
	
	pos=1;
	for(Iterator i(this);i;i++)
	{
		if(element==i)
		{
			return pos;
		}
		pos++;
	}

	return 0;
}

List * List::addHead(ListNode *listNode)
{
		listNode->insertAfter(head);
		return this;
}

List * List::addTail(ListNode *listNode)
{
		listNode->insertBefore(tail);
		return this;
}

int List::getSize()
{
		int n=0;

		for(Iterator i(this);i;i++) n++;

		return n;
}

Collection * List::add (Object *o)
{
	return addTail(ListNode::cast(o));
}

Collection * List::remove (Object *o)
{
	ListNode::cast(o)->remove(); 
	return this;
}

