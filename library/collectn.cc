/*
 * $Log$
 *
 */

/* $Id$ */

#include "collectn.h"

#define SUPERCLASS Object

INITIALIZE_ABSTRACT_CLASS(Collection, SUPERCLASS);

void Collection::addAll (Collection *c)
{
	for(Iterator i(c);i;i++)
		add(i);
}

void Collection::removeAll (Collection *c)
{
	for(Iterator i(c);i;i++)
		remove(i);
}

