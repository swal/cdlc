/*
 * $Log$
 *
 */

/*$Id$*/

#include "object.h"

#ifndef ITERATOR_H
#define ITERATOR_H

#include "collectn.h"

class Iterator : public Object
{   
	private:
		Object *current;
		class Collection *collection;
		Iterator ();
	public:
		Iterator (class Collection *c);
		operator Object * () ;
/*		operator int () ; kollidiert auf manchen C++ mit Object * in if() */
		Object & operator * ();
		void operator ++ ();                     /* is pre  incerement  */
		void operator ++ (int);                  /* is post incerement */
		void reset();
	DEFAULT_CONCRETE_METHODS(Iterator,Object);
};

#endif
