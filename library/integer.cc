/*
 * $Log$
 *
 */

/* $Id$ */

#include "integer.h"

#define SUPERCLASS Object
INITIALIZE_CONCRETE_CLASS(Integer, SUPERCLASS);

void Integer::setValue(int v)
{
  val=v;
}

Boolean Integer::equal(Object *another)
{
	if(another->isKindOf(Integer::ClassName))
	{
		return val==Integer::cast(another)->value();
	}
	else
	{
		return 0;
	}
}

Integer::Integer()
{
	val=0;
}

Integer::Integer(int i)
{
	DEBUG_enter("Integer::Integer(int i)")
	DEBUG_int(i)
	val=i;
	DEBUG_exit("Integer::Integer(int i)")
}

Integer::operator int ()
{
    return val;
}

unsigned int Integer::hash()
{
	return (unsigned int) val;
}

int Integer::value ()
{
	return val;
}

Integer::operator int & ()
{
	return val;
}


