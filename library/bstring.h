/*
 * $Log$
 *
 */

/*$Id$*/

#ifndef BSTRING_H
#define BSTRING_H

#include "object.h"

class BString : public Object
{
	protected:
		void carbonCopy(const char *s);
		char *theString; 

	public:
		BString(char *s);            /* macht sich eine lokale Kopie */
		BString();
		virtual ~BString();
		
		virtual unsigned int       hash();
		virtual Boolean            equal(Object *another);
		virtual operator char*     ();
		virtual char *             content();
		virtual int                containsChar(char c);
		virtual BString & operator = (const BString & s);
		virtual BString & operator = (char * s);
		virtual BString *          copy(int from, int to);
		virtual int                getStringSize();
		virtual BString *          splitAt(BString * splitString);
		virtual int                subStringPosition(BString * subString);
		virtual int                occurencesOfChar(char c);
		virtual BString *          append(BString* anotherString);

	DEFAULT_CONCRETE_METHODS(BString,Object);
	
	friend BString *updateCreation(BString *classname,CreationFunction *rf); 
};

#endif
