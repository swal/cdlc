/*
 * $Log$
 * 
 */

/*$Id$*/

#ifndef OBJECTSTORE_H
#define OBJECTSTORE_H

typedef class Object *CreationFunction();

#include <fstream.h>
#include "object.h"

class CreationFunctionWrapper : public Object
{
	private:
		void trapForAbstractClasses();

	public:
		CreationFunction *creationFunction;
};

#endif
