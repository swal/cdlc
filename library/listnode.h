/*
 * $Log$
 *
 */

/*$Id$*/

#include "object.h"

#ifndef LISTNODE_H
#define LISTNODE_H

class ListNode : public Object
{
	protected:
		ListNode * successor;
		ListNode * predecessor;
				
	public:
		ListNode();

		virtual ListNode * next();
		virtual ListNode * previous();
		virtual void       remove();
		virtual void       insertAfter(ListNode *);
		virtual void       insertBefore(ListNode *);

	DEFAULT_CONCRETE_METHODS(ListNode,Object);
		
	friend class List;
};

#endif
