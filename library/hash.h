#ifndef HASH_H
#define HASH_H

unsigned int hashpjw(const char*);
unsigned int multiplicativehash(int);
unsigned int foldhash(double);

#endif
