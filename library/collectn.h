/*
 * $Log$
 *
 */

/*$Id$*/

#include "object.h"

#ifndef COLLECTION_H
#define COLLECTION_H

#include "iterator.h"

class Collection : public Object
{
	public:
		virtual Object * first() = 0;
		virtual Object * next(Object *) = 0;
		virtual int includes(Object *) = 0;
		virtual Collection * add (Object *) = 0; /* oder ohne const? */
		virtual Collection * remove (Object *) = 0;
		virtual int getSize() = 0 ;
		void addAll (Collection *c);
		void removeAll (Collection *c);
	DEFAULT_ABSTRACT_METHODS(Collection,Object);
};

#endif
