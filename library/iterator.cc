/*
 * $Log$
 *
 */

/* $Id$ */

/* 
// Stabiler gemacht: Kein Absturz bei fehlender Collection (bzw. NULL),
// und ++-Operation auch nach dem Ende erlaubt.
*/

#include "iterator.h"

#define SUPERCLASS Object
INITIALIZE_CONCRETE_CLASS(Iterator, SUPERCLASS);


Iterator::Iterator()
{
	DEBUG_enter("Iterator::Iterator()")
	current=NULL;
	collection=NULL;
	DEBUG_exit("Iterator::Iterator()")
}

Iterator::Iterator(Collection *c)
{
	DEBUG_enter("Iterator::Iterator(Collection *)")

	if(c)
	{
		collection=c;
		reset();
	}
	else
	{
		current=NULL;
	}

	DEBUG_exit("Iterator::Iterator(Collection *)")
}


Iterator::operator Object * () 
{
	DEBUG_enter("Iterator::operator const Object * ()")
	DEBUG_exit("Iterator::operator const Object * ()")
	return current;
}

/*
Iterator::operator int ()
{
	return current==NULL ? False : True ;
}
*/

Object & Iterator::operator * ()
{
	DEBUG_enter("const Object & Iterator::operator * () const")
	if (NULL==current)
	{ error("internal error: Iterator"); }
	DEBUG_exit("const Object & Iterator::operator * () const")
	return *current;
}

void Iterator::operator ++ ()
{
	DEBUG_enter("Iterator::operator++")

	if(current)
	{
		current=collection->next(current);
	}
	DEBUG_exit("Iterator::operator++")
}

void Iterator::operator ++ (int)
{
	DEBUG_enter("Iterator::operator++post")
	if(current)
	{
		current=collection->next(current);
	}
	DEBUG_exit("Iterator::operator++post")
}

void Iterator::reset ()
{
	DEBUG_enter("Iterator::reset")
	if(collection)
	{
		current=collection->first();
	}
	else
	{
		current=NULL;
	}
	DEBUG_exit("Iterator::reset")
}

