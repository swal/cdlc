/*
 * $Log$
 *
 */

/* $Id$ */

/* 
// Revision 1.5  1993/09/30  00:19:52  bokowski
// Fehler behoben: ab jetzt gibt es kein Dictionary mit null Slots mehr
*/

#include "dictnary.h"

#define SUPERCLASS Collection
INITIALIZE_CONCRETE_CLASS(Dictionary, SUPERCLASS);


Dictionary::Dictionary()  // create a new, empty dictionary
{
	DEBUG_enter("Dictionary::Dictionary()")
	size=0;
	slots=1;
	content=new Object * [1];
	key=new Object * [1];
	marker=new int [1];
		marker[0]=0;
	currentIndex=0;
	DEBUG_exit("Dictionary::Dictionary()")
}


// create a new dictionary, which already has n slots    

Dictionary::Dictionary(int n)  
{
	DEBUG_enter("Dictionary::Dictionary(int)")
	size=0;
	slots=n;

	content = new Object * [n];
	key = new Object * [n];
	marker = new int[n];
	
	for(int i=0;i<n;i++) marker[i]=0;
	
	currentIndex=0;
	DEBUG_exit("Dictionary::Dictionary(int)")
}


unsigned int Dictionary::bestPosition(Object *o)
{
	DEBUG_enter("Dictionary::bestPosition")
	unsigned int pos=o->hash() % slots;
	
	while( marker[pos]==1 && !(key[pos]->equal(o)) )
	{
		pos++;
		if (pos>=slots) pos=0;
	}
	DEBUG_msg("found POS in number of SLOTS")
	DEBUG_int(pos)
	DEBUG_int(slots)
	DEBUG_exit("Dictionary::bestPosition")
	return pos;
}


Object * Dictionary::first()
{
	DEBUG_enter("Dictionary::first")
	currentIndex=0;
	
	while(currentIndex<slots && marker[currentIndex]!=1)
		currentIndex++;
	
	DEBUG_exit("Dictionary::currentIndex")
	return currentIndex<slots ? key[currentIndex] : NULL ;
}


Object * Dictionary::next(Object *)
{
	DEBUG_enter("Dictionary::first")
	do
	{
		currentIndex++;
	}
	while(currentIndex<slots && marker[currentIndex]!=1);
	
	DEBUG_exit("Dictionary::next")
	return (currentIndex < slots) ? key[currentIndex] : NULL ;
}


Collection * Dictionary::add (Object *)
{
	fatalError("no add for dictionary");
	return NULL;
}


Collection * Dictionary::remove (Object *)
{
	fatalError("no remove for dictionary");
	return NULL;
}


int Dictionary::getSize()
{ 
	return size; 
}

Object * Dictionary::getValueAt(Object *k)
{
DEBUG_enter("Dictionary::getValueAt")
//cout << "key=" << k << endl;
	unsigned int i=bestPosition(k);
//cout << "position=" << i << " , marker= " << marker[i] << endl;
DEBUG_exit("Dictionary::getValueAt")
	return marker[i]==1 ? content[i] : 0 ;
}

int Dictionary::includes(Object *k)
{
	unsigned int i;
	int j;
	
	DEBUG_enter("Dictionary::includes")
	i=bestPosition(k);
	j=(marker[i]==1);
	DEBUG_exit("Dictionary::includes")
	return j;
}


Dictionary * Dictionary::addAssoc( Object *k, Object * v)
{
	DEBUG_enter("Dictionary::addAssoc")
	DEBUG_ptr( k )
	DEBUG_ptr( v )
	if(size<(int)((float)slots*0.9))
	{
		unsigned int i=bestPosition(k);
		marker[i]=1;
		key[i]=k;
		content[i]=v;
		size++;
	}
	else
	{
		Dictionary *bigger = new Dictionary(slots ? slots*2 : 4);
		for(int i=0; i<slots; i++)
		{
			if(marker[i]==1)
			{
				bigger->addAssoc(key[i],content[i]);
			}
		}
		bigger->addAssoc(k,v);
//cout << "1: size=" << slots << " , key=" << key << " , content = " << content << endl;
		delete [] key;

		delete [] content;

		delete [] marker;

		key=bigger->key;
		content=bigger->content;
		marker=bigger->marker;
		size=bigger->size;
		slots=bigger->slots;
		bigger->key=0;
		bigger->content=0;
		bigger->marker=0;

		delete bigger;
	}
	
	DEBUG_exit("Dictionary::addAssoc")
	return this;
}


Dictionary * Dictionary::removeKey(Object *k)
{
	unsigned int i;
	DEBUG_enter("Dictionary::removeKey")
	i=bestPosition(k);
	if(marker[i]==1)
	{
		marker[i]=2;
		key[i]=0;
		content[i]=0;
	}
	DEBUG_exit("Dictionary::removeKey")
	return this;
}


