/*
 * $Log$
 *
 */

/* $Id$ */

#include "pair.h"

#define SUPERCLASS Object
INITIALIZE_CONCRETE_CLASS(Pair, SUPERCLASS);

Pair::Pair()
{
	left=NULL;
	right=NULL;
}

Pair::Pair(Object * l, Object * r )
{
	DEBUG_enter("Pair::Pair(Object * l, Object * r )")
	left=l;
	right=r;
	DEBUG_exit("Pair::Pair(Object * l, Object * r )")
}

void Pair::setRight(Object * o)
{
	right=o;
}

void Pair::setLeft(Object * o)
{
	left=o;
}

Object * Pair::getRight()
{
	DEBUG_enter("Object * Pair::getRight()")
	DEBUG_exit("Object * Pair::getRight()")
	return right;
}

Object * Pair::getLeft()
{
	DEBUG_enter("Object * Pair::getLeft()")
	DEBUG_exit("Object * Pair::getLeft()")
	return left;
}


