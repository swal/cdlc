/*
 * $Log$
 *
 */

/* $Id$ */ 

#include "exand.h"
#include "../main/debug.h"
#include "../klassen/atbool.h"
#include "../klassen/acbool.h"
#include "../library/bstring.h"
#include "../main/error.h"
#include "../grammar/parser.h"

#define SUPERCLASS Expression
INITIALIZE_CONCRETE_CLASS(ExAnd, SUPERCLASS);


ExAnd::ExAnd()
{
    DEBUG_enter("ExAnd::ExAnd()")
    ex1=NULL;
    ex2=NULL;
    DEBUG_exit("ExAnd::ExAnd()")
}

ExAnd::ExAnd( Object * exleft, Object * exright )
{
    DEBUG_enter("ExAnd::ExAnd( Object * exleft, Object * exright )")
    
    if (!exleft ->isKindOf(Expression::ClassName))    
    {
        printf("\n%s\n",BString::cast(exleft->className())->content());
        error("internal error: ExAnd's left subexpr. isn't an expr.");
    }
    if (!exright->isKindOf(Expression::ClassName))
    { error("internal error: ExAnd's right subexpr. isn't an expr."); }
    if (!Expression::cast(exleft)->getType()->isKindOf(ATypeBool::ClassName))
    { semerror(NULL,"type conflict: left side of 'and' isn't bool"); }
    if (!Expression::cast(exright)->getType()->isKindOf(ATypeBool::ClassName))
    { semerror(NULL,"type conflict: right side of 'and' isn't bool"); }
  
    ex1=Expression::cast(exleft );
    ex2=Expression::cast(exright);

    DEBUG_exit("ExAnd::ExAnd( Object * exleft, Object * exright )")
}

AType * ExAnd::getType()
{
    DEBUG_enter("AType * ExAnd::getType()")
    DEBUG_exit("AType * ExAnd::getType()")
    return new ATypeBool();
}

AConstant * ExAnd::asConstant()
{
    AConstant * ac1, * ac2;
    AConstant * cache;


    DEBUG_enter("AConstant * ExAnd::asConstant()")

    ac1=ex1->asConstant();
    ac2=ex2->asConstant();
    
    if ((NULL!=ac1) && (NULL!=ac2))
    {
        DEBUG_msg("ExAnd.1")
        cache=
            new AConBool( 
                AConBool::cast(ac1)->value() & 
                AConBool::cast(ac2)->value() 
            );
    }
    else 
    {
        if (NULL!=ac1)
        { 
            DEBUG_enter("ExAnd.2")
            if ( !(AConBool::cast(ac1)->value()) ) { cache=ac1; }
            DEBUG_exit("ExAnd.2")
        }
        else if (NULL!=ac2)
        {
            DEBUG_msg("ExAnd.3")
            if (!AConBool::cast(ac2)->value()) { cache=ac2; }
        }
        else
        {
            DEBUG_msg("ExAnd.4")
            cache= NULL;
        }
        cache=NULL;
    }

    DEBUG_exit("AConstant * ExAnd::asConstant()")
    return cache;
}

AVariable * ExAnd::logic()
{
    AVariable *v;
    AConstant *ac1,*ac2;

    DEBUG_enter("AVariable * ExAnd::logic()")

    if (NULL!=asConstant())
    { 
        v=new AVariable(new BString(""),getType()); 
        v->setConstant(asConstant());
    }
    else
    {
        ac1=ex1->asConstant();
        ac2=ex2->asConstant();

        if (NULL!=ac1)
        { v=ex2->logic(); }
        else if (NULL!=ac2)
        { v=ex1->logic(); }
        else
        {
            /* neue Variable generieren und als *LOCAL bekannt machen */
            v=generateTempVariable(getType());

            fprintf(fout,"; and-expression:\n*BOOLEAN-EQUATIONS\n %s = %s & %s ;\n\n",
                v           ->getName()->content(),
                ex1->logic()->getName()->content(),
                ex2->logic()->getName()->content()
            );
        }
    }    

    DEBUG_exit("AVariable * ExAnd::logic()")

    return v;
}

