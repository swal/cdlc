#include "fnvalue.h"
#include "../library/integer.h"
#include "../klassen/avariabl.h"
#include "../klassen/atrange.h"
#include "../klassen/atenum.h"
#include "../klassen/atbool.h"
#include "../statmnt/stassign.h"
#include "../main/main.h"
#include "../main/logic.h"


#define SUPERCLASS Expression

INITIALIZE_CONCRETE_CLASS(FnValue, SUPERCLASS);


FnValue::FnValue()
{
    DEBUG_enter("FnValue::FnValue()")
    DEBUG_exit("FnValue::FnValue()")
}

FnValue::FnValue(Object * exList)
{
    DEBUG_enter("FnValue::FnValue(Object * exList)")
    if ( 1!=List::cast(exList)->getSize() )
    { error("wrong number of parameters for value-function"); }
    myEx=Expression::cast(ListObject::cast(List::cast(exList)->at(1))->getObject());
    if ( !myEx->getType()->isKindOf(ATypeEnum::ClassName) )
    { error("argument of value() isn't of type enum"); }
    DEBUG_exit("FnValue::FnValue(Object * exList)")
}

AType * FnValue::getType()
{
    AType * at;
    DEBUG_enter("AType * FnValue::getType()")
    at=
        new ATypeRange(
            new Integer(0),
            new Integer(
                ATypeEnum::cast(myEx->getType())->asGroup()->getSize()-1
            )
        );
    DEBUG_exit("AType * FnValue::getType()")
    return at;
}
    
AConstant * FnValue::asConstant()
{
    AConstant * c;
    DEBUG_enter("AConstant * FnValue::asConstant()")
    if (NULL!=myEx->asConstant())
    {
        c=new AConInt( decimal(myEx->getType()->codingOf(myEx->asConstant())) );
    }
    else
    {
        c=NULL;
    }
    DEBUG_exit("AConstant * FnValue::asConstant()")
    return c;
}

AVariable * FnValue::logic()
{
    AVariable * v;

    DEBUG_enter("AVariable * FnValue::logic()")
    
    if (NULL!=asConstant())
    { v=new AVariable(new BString(""),getType()); }
    else
    {
        v=generateTempVariable(getType());
        fprintf(fout,"; FnValue:\n*BOOLEAN-EQUATIONS\n %s = %s ;\n\n",
            v            ->getName()->content(),
            myEx->logic()->getName()->content()
        );
    }
    v->setConstant(asConstant()); 
    DEBUG_exit("AVariable * FnValue::logic()")
    return v;
}
