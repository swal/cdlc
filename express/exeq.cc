/*
 * $Log$
 *
 */

/* $Id$ */ 

#include "exeq.h"
#include "../main/debug.h"
#include "../main/error.h"
#include "../klassen/acbool.h"
#include "../klassen/atrange.h"
#include "../library/bstring.h"
#include "../grammar/parser.h"

#include "../main/main.h"


#define SUPERCLASS Expression
INITIALIZE_CONCRETE_CLASS(ExEq, SUPERCLASS);


ExEq::ExEq()
{
    DEBUG_enter("ExEq::ExEq()")
    ex1=NULL;
    ex2=NULL;
    DEBUG_exit("ExEq::ExEq()")
}

ExEq::ExEq( Object * exleft, Object * exright )
{
    DEBUG_enter("ExEq::ExEq( Object * exleft, Object * exright )")
    if (!exleft ->isKindOf(Expression::ClassName)) 
    { error("internal error: left Subexs of ExEq isn't an Expr"); }
    if (!exright->isKindOf(Expression::ClassName))
    { 
        printf("\n%s\n",BString::cast(exright->className())->content());
        error("internal error: right Subexs of ExEq isn't an Expr"); 
    }
    ex1=Expression::cast(exleft );
    ex2=Expression::cast(exright);
    DEBUG_exit("ExEq::ExEq( Object * exleft, Object * exright )")
}

AType * ExEq::getType()
{
    DEBUG_enter("AType * ExEq::getType()")
    DEBUG_exit("AType * ExEq::getType()")
    return new ATypeBool();
}

AConstant * ExEq::asConstant()
{
    AConstant *cache;
    AConstant * ac1, *ac2;


    DEBUG_enter("AConstant * ExEq::asConstant()")
    ac1=ex1->asConstant();
    ac2=ex2->asConstant();
    if (ex1->getType()->isKindOf(ATypeRange::ClassName))
    {
        int l1,u1,l2,u2;
        u1=ATypeRange::cast(ex1->getType())->getUpper()->value();
        l1=ATypeRange::cast(ex1->getType())->getLower()->value();
        u2=ATypeRange::cast(ex2->getType())->getUpper()->value();
        l2=ATypeRange::cast(ex2->getType())->getLower()->value();
        if ((u1<l2)||(u2<l1))
        {
            warning("comparison of ranges with no common number");
            DEBUG_exit("AConstant * ExEq::asConstant()");
            cache=new AConBool(0);
            return cache;
        }
    }

    if ((NULL!=ac1) && (NULL!=ac2))
    {
        cache=(*ac1==ac2);
    }
    else
    {
        cache=NULL;
    }
    DEBUG_exit("AConstant * ExEq::asConstant()")
    return cache;
}

AVariable * ExEq::logic()
{
    AVariable *x;

    DEBUG_enter("AVariable * ExEq::logic()")

    if (NULL!=asConstant())
    { 
        x=new AVariable(new BString(""),getType()); 
        x->setConstant(asConstant());
    }
    else
    {
        AVariable *r, *l;

        r=ex1->logic();
        l=ex2->logic();

        /*
            Attention:
            There is a difference between  a # 4  ,and   4 # a  in LOG/iC.
            Only the first is what we want.
        */

        if (NULL!=l->asConstant())
        { AVariable * help; help=l; l=r; r=help; }

        if (0==strcmp(r->getName( l->getType() )->content(),"1"))
        {
            x=l;
            /*
            fprintf(fout,"; equal-expression\n*BOOLEAN-EQUATIONS\n %s = %s;\n\n",
                x->getName()->content(),
                l->getName()->content()
            );
            */
        }
        else
        {
            x=generateTempVariable(getType());
            
            if (0==strcmp(r->getName( l->getType() )->content(),"0"))
            {
                fprintf(fout,"; equal-expression\n*BOOLEAN-EQUATIONS\n %s = / %s;\n\n",
                    x->getName()->content(),
                    l->getName()->content()
                );
            }
            else
            {
                if ( l->getType()->isKindOf(ATypeRange::ClassName) )
                {
                    /* spezial difficult type conversions for numbers */
                    int up,lo,upr,lor;
                    ATypeRange * atr;
                    
                    up=Integer::cast(ATypeRange::cast(l->getType())->getUpper())->value();
                    lo=Integer::cast(ATypeRange::cast(l->getType())->getLower())->value();
                    upr=Integer::cast(ATypeRange::cast(r->getType())->getUpper())->value();
                    lor=Integer::cast(ATypeRange::cast(r->getType())->getLower())->value();
                    if (up<upr) up=upr;
                    if (lo>lor) lo=lor;
                    atr=new ATypeRange(new Integer(lo),new Integer(up));
                        
                    fprintf(fout,"; equal-expression numbers\n*BOOLEAN-EQUATIONS\n %s = /(+(%s#%s));\n\n",
                        x->getName()->content(),
                        l->getName( atr )->content(),
                        r->getName( atr )->content()
                    );                
                }
                else
                {
                    fprintf(fout,"; equal-expression\n*BOOLEAN-EQUATIONS\n %s = /(+(%s#%s));\n\n",
                        x->getName()->content(),
                        l->getName()->content(),
                        r->getName( l->getType() )->content()
                    );
                }
            }
        }
    }
    DEBUG_exit("AVariable * ExEq::logic()")
    return x;
}

