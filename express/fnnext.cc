
#include "fnnext.h"
#include "../library/integer.h"
#include "../klassen/avariabl.h"
#include "../klassen/atrange.h"
#include "../klassen/atenum.h"
#include "../klassen/atbool.h"
#include "../klassen/acenum.h"
#include "../statmnt/stassign.h"
#include "../main/main.h"
#include "../main/error.h"
#include "../main/logic.h"


#define SUPERCLASS Expression

INITIALIZE_CONCRETE_CLASS(FnNext, SUPERCLASS);


FnNext::FnNext()
{
    DEBUG_enter("FnNext::FnNext()")
    DEBUG_exit("FnNext::FnNext()")
}

FnNext::FnNext(Object * exList)
{
    DEBUG_enter("FnNext::FnNext(Object * exList)")
    if ( 1!=List::cast(exList)->getSize() )
    { error("wrong number of parameters for next-function"); }
    myEx=Expression::cast(ListObject::cast(List::cast(exList)->at(1))->getObject());
    if ( !myEx->getType()->isKindOf(ATypeEnum::ClassName) )
    { error("argument of next() isn't of type enum"); }
    DEBUG_exit("FnNext::FnNext(Object * exList)")
}

AType * FnNext::getType()
{
    AType * at;
    DEBUG_enter("AType * FnNext::getType()")
    at=myEx->getType();
    DEBUG_exit("AType * FnNext::getType()")
    return at;
}
    
AConstant * FnNext::asConstant()
{
    AConstant * c;
    DEBUG_enter("AConstant * FnNext::asConstant()")
    if (NULL!=myEx->asConstant())
    {
        ATypeEnum *ate;
        int pos;

        ate=ATypeEnum::cast(myEx->getType());
        pos=ate->positionOf(myEx->asConstant())+2;

        if (pos>ate->asGroup()->getSize()) { pos=1; }

        c=AConEnum::cast(
          ListObject::cast(ate->asGroup()->at(pos))->getObject()
        );
    }
    else
    {
        c=NULL;
    }
    DEBUG_exit("AConstant * FnNext::asConstant()")
    return c;
}

AVariable * FnNext::logic()
{
    AVariable * v;

    DEBUG_enter("AVariable * FnNext::logic()")
    
    if (NULL!=asConstant())
    { 
        v=new AVariable(new BString(""),getType()); 
        v->setConstant(asConstant());    
    }
    else
    {
        AGroup * g;
        AVariable * e;

        e=myEx->logic();

        v=generateTempVariable(getType());
        
        fprintf(fout,"; FnNext:\n*FUNCTION-TABLE\n$HEADER:\n X %s : Y %s ;\n",
            e->getName()->content(),
            v->getName()->content()
        );

        g=ATypeEnum::cast(getType())->asGroup();

        for ( int i=1 ; i<g->getSize() ; i++ )
        {
            fprintf(fout," X %s : Y %s ;\n",
                getType()->codingOf(
                    AConEnum::cast(ListObject::cast(g->at(i))->getObject())
                )->content(),
                getType()->codingOf(
                    AConEnum::cast(ListObject::cast(g->at(i+1))->getObject())
                )->content()
            );
        }

        i=g->getSize();

            fprintf(fout," X %s : Y %s ;\n\n",
                getType()->codingOf(
                    AConEnum::cast(ListObject::cast(g->at(i))->getObject())
                )->content(),
                getType()->codingOf(
                    AConEnum::cast(ListObject::cast(g->at(1))->getObject())
                )->content()
            );
                                            
    }
    DEBUG_exit("AVariable * FnNext::logic()")
    return v;
}
