/*
 * $Log$
 *
 */

/*$Id$*/

#ifndef EXPRESSION_OR_H
#define EXPRESSION_OR_H

#include "../express/express.h"

class ExOr : public Expression
{
	private:
		Expression * ex1, * ex2;

	public:
		ExOr();
		ExOr( Object * exleft, Object * exright );
		virtual AType * getType();
		virtual AConstant * asConstant();
		virtual AVariable * logic();

	DEFAULT_CONCRETE_METHODS(ExOr,Expression);
};

#endif
