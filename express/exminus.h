/*
 * $Log$
 *
 */

/*$Id$*/

#ifndef EXPRESSION_MINUS_H
#define EXPRESSION_MINUS_H

#include "../express/express.h"
#include "../klassen/atype.h"
#include "../klassen/aconstnt.h"

class ExMinus : public Expression
{
	private:
		Expression * ex;

	public:
		ExMinus();
		ExMinus( Object * expression );

		virtual AType     * getType();
		virtual AConstant * asConstant();       
		virtual AVariable * logic();

	DEFAULT_CONCRETE_METHODS(ExMinus,Expression);
};

#endif
