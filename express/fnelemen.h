/*
 * $Log$
 *
 */

/*$Id$*/

#ifndef FNELEMENT_H
#define FNELEMENT_H

#include "../express/express.h"
#include "../klassen/atype.h"
#include "../klassen/token.h"

class FnElement : public Expression
{
	protected:
		Expression * myEx;
		AType * myType;
		Token * token;

	public:
		FnElement();
		FnElement( AType* type,Object * exList );
		
		virtual AType     * getType();
		virtual AConstant * asConstant();
		virtual AVariable * logic();
		
	DEFAULT_CONCRETE_METHODS(FnElement,Expression);
};

#endif
