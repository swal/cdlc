/*
 * $Log$
 *
 */

/*$Id$*/

#ifndef EXPRESSION_BORDER_H
#define EXPRESSION_BORDER_H

#include "../express/express.h"

class ExBorder : public Expression
{
	public:
		ExBorder();
		virtual AType     * getType();
		virtual AConstant * asConstant();
		virtual AVariable * logic();
		
	DEFAULT_CONCRETE_METHODS(ExBorder,Expression);
};

#endif
