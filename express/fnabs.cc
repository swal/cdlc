
#include "fnabs.h"
#include "../library/integer.h"
#include "../library/pair.h"
#include "../library/list.h"
#include "../klassen/avariabl.h"
#include "../klassen/atrange.h"
#include "../klassen/atenum.h"
#include "../klassen/atbool.h"
#include "../klassen/acenum.h"
#include "../statmnt/stassign.h"
#include "../main/main.h"
#include "../main/logic.h"
#include "../express/exlt.h"
#include "../express/exminus.h"

#define SUPERCLASS Expression

INITIALIZE_CONCRETE_CLASS(FnAbs, SUPERCLASS);


FnAbs::FnAbs()
{
    DEBUG_enter("FnAbs::FnAbs()")
    DEBUG_exit("FnAbs::FnAbs()")
}

FnAbs::FnAbs(Object * exList)
{
    DEBUG_enter("FnAbs::FnAbs(Object * exList)")
    if ( 1!=List::cast(exList)->getSize() )
    { error("wrong number of parameters for abs-function"); }
    myEx=Expression::cast(ListObject::cast(List::cast(exList)->at(1))->getObject());
    if ( !myEx->getType()->isKindOf(ATypeRange::ClassName) )
    { error("argument of abs() is not of type int"); }
    DEBUG_exit("FnAbs::FnAbs(Object * exList)")
}

AType * FnAbs::getType()
{
    AType * at;
    int lo,hi,max,min;

    DEBUG_enter("AType * FnAbs::getType()")

    lo=ATypeRange::cast(myEx->getType())->getLower()->value();
    hi=ATypeRange::cast(myEx->getType())->getLower()->value();

    if (abs(lo)>abs(hi)) 
    { max=abs(lo); }
    else
    { max=abs(hi); }
    
    min=0;
    if (lo>0) { min=lo; }
    if (hi<0) { min=abs(hi); }

    at=new ATypeRange( new Integer(min),new Integer(max) );
    DEBUG_exit("AType * FnAbs::getType()")
    return at;
}
    
AConstant * FnAbs::asConstant()
{
    AConstant * c;
    DEBUG_enter("AConstant * FnAbs::asConstant()")
    if (NULL!=myEx->asConstant())
    {
        c=new AConInt(
            abs( AConInt::cast(myEx->asConstant())->value() )
        );
    }
    else
    {
        c=NULL;
    }
    DEBUG_exit("AConstant * FnAbs::asConstant()")
    return c;
}

AVariable * FnAbs::logic()
{
    AVariable * v;

    DEBUG_enter("AVariable * FnAbs::logic()")

    
    if (NULL!=asConstant())
    { 
        v=generateTempVariable(getType());
        v->setConstant(asConstant());    
    }
    else
    {
        AVariable *e, *negativ;
        int upper,lower;
        ATypeRange* eType;

        e=myEx->logic();

        eType=ATypeRange::cast(e->getType());

        lower=eType->getLower()->value();
        upper=eType->getUpper()->value();

        if (lower>=0)
        { v=e; }
        else
        {
            if (upper<0)
            { v=(new ExMinus(e))->logic(); }
            else
            {
                negativ=generateTempVariable(new ATypeBool());
                fprintf(fout,"*BOOLEAN-EQUATIONS\n %s=%s;\n",
                    negativ->getName()->content(),
                    e->getBitName(eType->getNumberOfBits()-1)->content()
                );
                (new StAssign(
                    new Pair(e,new List()), 
                    (new ExMinus(e))->logic()
                ))->logic(negativ);
                v=e;
            }
        }
    }
    DEBUG_exit("AVariable * FnAbs::logic()")
    return v;
}
