/*
 * $Log$
 *
 */

/* $Id$ */ 

#include "exxor.h"
#include "../main/debug.h"
#include "../main/error.h"
#include "../klassen/atbool.h"
#include "../klassen/acbool.h"
#include "../library/bstring.h"
#include "../express/exnot.h"
#include "../grammar/parser.h"


#define SUPERCLASS Expression
INITIALIZE_CONCRETE_CLASS(ExXor, SUPERCLASS);


ExXor::ExXor()
{
    DEBUG_enter("ExXor::ExXor()")
    ex1=NULL;
    ex2=NULL;
    DEBUG_exit("ExXor::ExXor()")
}

ExXor::ExXor( Object * exleft, Object * exright )
{
    DEBUG_enter("ExXor::ExXor( Object * exleft, Object * exright )")
    
    if (!exleft ->isKindOf(Expression::ClassName)) 
    { error("internal error: left subexpression of xor isn't an expression"); }
    if (!exright->isKindOf(Expression::ClassName))
    { error("internal error: right subexpression of xor isn't an expression"); }
    if (!Expression::cast(exleft)->getType()->isKindOf(ATypeBool::ClassName))
    { semerror(NULL,"type conflict: left side of xor isn't bool"); }
    if (!Expression::cast(exright)->getType()->isKindOf(ATypeBool::ClassName))
    { semerror(NULL,"type conflict: right side of xor isn't bool"); }
  
    ex1=Expression::cast(exleft );
    ex2=Expression::cast(exright);
    DEBUG_exit("ExXor::ExXor( Object * exleft, Object * exright )")
}

AType * ExXor::getType()
{
    DEBUG_enter("AType * ExXor::getType()")
    DEBUG_exit("AType * ExXor::getType()")
    return new ATypeBool();
}

AConstant * ExXor::asConstant()
{
    AConstant * ac1, *ac2;
    AConstant * cache;

    DEBUG_enter("AConstant * ExXor::asConstant()")

    ac1=ex1->asConstant();
    ac2=ex2->asConstant();
    if ((NULL!=ac1) && (NULL!=ac2))
    {
        cache=new AConBool( AConBool::cast(ac1)->value() ^ AConBool::cast(ac2)->value() );
    }
        else
    {
        cache=NULL;
    }

    DEBUG_exit("AConstant * ExXor::asConstant()")        
        
    return cache; 
}

AVariable * ExXor::logic()
{
    AVariable *v;
    AConBool *ac1, *ac2;

    DEBUG_enter("AVariable * ExXor::logic()")

    if (NULL!=asConstant())
    {
        v=new AVariable(new BString(""),getType());
    }
    else
    {
        ac1=AConBool::cast(ex1->asConstant());
        ac2=AConBool::cast(ex2->asConstant());
        
        if (NULL!=ac1)
        {
            if (ac1->value())
            { v=(new ExNot(ex2))->logic(); }
            else
            { v=ex2->logic(); }
        }
        else if (NULL!=ac2)
        {
            if (ac2->value())
            { v=(new ExNot(ex1))->logic(); }
            else
            { v=ex1->logic(); }
        }
        else
        {
            /* neue Variable generieren und als *LOCAL bekannt machen */
            v=generateTempVariable(getType());

            fprintf(fout,"; Xor:\n*BOOLEAN-EQUATIONS\n %s = %s # %s ;\n\n",
                v           ->getName()->content(),
                ex1->logic()->getName()->content(),
                ex2->logic()->getName()->content()
            );
        }
    }
    
    v->setConstant(asConstant());

    DEBUG_exit("AVariable * ExXor::logic()")

    return v;
}

