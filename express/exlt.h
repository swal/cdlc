/*
 * $Log$
 *
 */

/*$Id$*/

#ifndef EXPRESSION_LOWERTHAN_H
#define EXPRESSION_LOWERTHAN_H

#include "../express/express.h"

class ExLt : public Expression
{
	private:
		Expression * ex1, * ex2;
	
	public:
		ExLt();
		ExLt( Object * exleft, Object * exright );

		virtual AType     * getType();
		virtual AConstant * asConstant();
		virtual AVariable * logic();
		
	DEFAULT_CONCRETE_METHODS(ExLt,Expression);
};

#endif
