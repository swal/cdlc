/*
 * $Log$
 *
 */

/*$Id$*/

#ifndef EXPRESSION_DIV_H
#define EXPRESSION_DIV_H

#include "../express/express.h"
#include "../klassen/atype.h"
#include "../klassen/aconstnt.h"


class ExDiv : public Expression
{
	private:
		Expression * ex1, * ex2;

	public:
		ExDiv();
		ExDiv( Object * exleft, Object * exright );
		virtual AType * getType();
		virtual AConstant * asConstant();
		virtual AVariable * logic();
		
	DEFAULT_CONCRETE_METHODS(ExDiv,Expression);
};

void subAdd(BString *a,BString *b,BString *cin,BString *sA,BString *c,BString *s);


#endif
