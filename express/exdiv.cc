/*
 * $Log$
 *
 */

/* $Id$ */ 

#include "exdiv.h"
#include "../main/debug.h"
#include "../klassen/atenum.h"
#include "../klassen/atbool.h"
#include "../klassen/acint.h"
#include "../library/bstring.h"
#include "../main/error.h"
#include "../grammar/parser.h"
#include "../express/exadd.h"


#define SUPERCLASS Expression
INITIALIZE_CONCRETE_CLASS(ExDiv, SUPERCLASS);


ExDiv::ExDiv()
{
	DEBUG_enter("ExDiv::ExDiv()")
	ex1=NULL;
	ex2=NULL;
	DEBUG_exit("ExDiv::ExDiv()")
}

ExDiv::ExDiv( Object * exleft, Object * exright )
{
	DEBUG_enter("ExDiv::ExDiv( Object * exleft, Object * exright )")
	if (!exleft ->isKindOf(Expression::ClassName))
	{ error("internal error: one of ExDiv's Subexs isn't an Expr"); }
	if (!exright->isKindOf(Expression::ClassName))
	{ error("internal error: one of ExDiv's Subexs isn't an Expr"); }
	if (!Expression::cast(exleft)->getType()->isKindOf(ATypeRange::ClassName))
	{ semerror(NULL,"type conflict: left side of 'div' isn't int"); }
	if (!Expression::cast(exright)->getType()->isKindOf(ATypeRange::ClassName))
	{ semerror(NULL,"type conflict: right side of 'div' isn't int"); }
 
	ex1=Expression::cast(exleft );
	ex2=Expression::cast(exright);
	DEBUG_exit("ExDiv::ExDiv( Object * exleft, Object * exright )")
}

AType * ExDiv::getType()
{
	AType * cache;
	int upper,lower,max,teilerUpper,teilerLower;
	
	DEBUG_enter("AType * ExDiv::getType()")

	upper      =ATypeRange::cast(ex1->getType())->getUpper()->value();
	lower      =ATypeRange::cast(ex1->getType())->getLower()->value();

	if ( abs(upper)>abs(lower) ) { max=abs(upper); } else { max=abs(lower); }
	
	teilerUpper=ATypeRange::cast(ex2->getType())->getUpper()->value();
	teilerLower=ATypeRange::cast(ex2->getType())->getLower()->value();

	if ( (teilerUpper>=0) && (teilerLower<=0) )
	{ warning("division by zero is undefined"); }

	if ( (teilerUpper>=0) && (teilerLower<=0) )
	{ cache=new ATypeRange(new Integer(-max),new Integer(max)); }
	else
	{
		if (teilerLower>0)
		{ cache=new ATypeRange(new Integer(lower/teilerLower),new Integer(upper/teilerLower)); }
		else
		{ cache=new ATypeRange(new Integer(upper/teilerUpper),new Integer(lower/teilerUpper)); }
	}
	
	DEBUG_exit("AType * ExDiv::getType()")

	return cache;
}

AConstant * ExDiv::asConstant()
{
	AConstant * ac1, *ac2, *cache;
	int maxAbsL, minAbsR;

	DEBUG_enter("AConstant * ExDiv::asConstant()")

	maxAbsL=abs(ATypeRange::cast(ex1->getType())->getLower()->value());
	if (maxAbsL<abs(ATypeRange::cast(ex1->getType())->getUpper()->value()))
	{   maxAbsL=abs(ATypeRange::cast(ex1->getType())->getUpper()->value()); }

	minAbsR=abs(ATypeRange::cast(ex2->getType())->getLower()->value());
	if (minAbsR>abs(ATypeRange::cast(ex2->getType())->getUpper()->value()))
	{   minAbsR=abs(ATypeRange::cast(ex2->getType())->getUpper()->value()); }

	if (maxAbsL<minAbsR)
	{ 
		cache=new AConInt(0); 
		DEBUG_exit("AConstant * ExDiv::asConstant()")
		return cache;
	}


	ac1=ex1->asConstant();
	ac2=ex2->asConstant();

	if (NULL!=ac2)
	{
		if (0==AConInt::cast(ac2)->value())
		{ semerror(NULL,"division by zero"); }
	}
	if ((NULL!=ac1) && (NULL!=ac2))
	{
		cache= new AConInt(
			AConInt::cast(ac1)->value() / 
			AConInt::cast(ac2)->value()   );
	}
	else
	{
		cache= NULL;
	}
	DEBUG_exit("AConstant * ExDiv::asConstant()")
	return cache;
}

/* one bit universal subtractor (subAdd=="1") or adder (subAdd=="0") */
void subAdd(BString *a,BString *b,BString *cin,BString *sA,BString *c,BString *s)
{
	DEBUG_enter("static void subAdd()")

	fprintf(fout,"*BOOLEAN-EQUATIONS\n %s = %s # %s # %s # %s ;\n",    
		s->content(),
		a->content(),
		b->content(),
		sA->content(),
		cin->content()
	);
	
	fprintf(fout," %s = ((%s#%s) & (%s+%s)) + (%s&%s);\n\n",
		c->content(),

		sA->content(),
		b->content(),

		a->content(),
		cin->content(),

		a->content(),
		cin->content()
	);

	DEBUG_exit("static void subAdd()")
}


AVariable * ExDiv::logic()
{
	AVariable * cache;
	AVariable * av1, *av2;

	int divByShift;

	
	DEBUG_enter("AVariable * ExDiv::logic()")

	if (NULL!=asConstant())
	{
		cache=new AVariable(new BString(""),getType());
		cache->setConstant(asConstant());
	}
	else
	{
		av1=ex1->logic();
		av2=ex2->logic();
		
		cache=generateTempVariable(getType());
		divByShift=0;
		
		if (NULL!=av2->asConstant())
		{
			int c;
			c=AConInt::cast(av2->asConstant())->value();
			if ( c==(1<<logd(c)) )
			{
				fprintf(fout,"; div:\n*BOOLEAN-EQUATIONS\n %s = %s[%u..%u] ;\n\n",
					cache->getName()->content(),
					av1->getBaseName()->content(),
					ATypeRange::cast(av1->getType())->getNumberOfBits()-1,
					logd(c)                 
				);
				divByShift=1;
			}
		}

		if (!divByShift)
		{
			DEBUG_msg("AVariable * ExDiv::logic()-var/var")
		/*
		  Algorithmus aus:
		  R. Hoffmann: "Rechnerentwurf" Seite 132
		*/

			Dictionary * p; /* enthaelt aktuellen Zwischenwert */
			AVariable *qt; /* Quotient von +1 Korrektur */
			int n;  /* q=av2 [n..0] */
			int m;  /* p [m..0] */

			int i;

			n=av2->getType()->getNumberOfBits()-1;
			m=av1->getType()->getNumberOfBits()-1+n;

			p=new Dictionary();

			for ( i=0 ; i<=m ; i++ )
			{
				if (i<av1->getType()->getNumberOfBits())
				{
					p->addAssoc(
						new Integer(i),
						BString::cast(av1->getBitName(i))
					);
				}
				else
				{
					if (ATypeRange::cast(av1->getType())->isSigned())
					{
						p->addAssoc(
							new Integer(i),
							BString::cast(av1->getBitName(av1->getType()->getNumberOfBits()-1))
						);
					}
					else
					{
						p->addAssoc(
							new Integer(i),
							new BString("0")
						);

					}
				}
			}

			qt=generateTempVariable( this->getType() );

			AVariable * sa;
			AVariable *cin;

			BString * signP;

			signP=BString::cast( p->getValueAt(new Integer(m)) );

			sa=generateTempVariable(new ATypeBool());
			cin=sa;

			fprintf(fout,"*BOOLEAN-EQUATION\n %s = /(%s # %s)",
				sa->getName()->content(),
				signP->content(),
				av2->getBitName(n)->content()
			);

			for (
				int step= (m-n) ;
				step>=0 ;
				step-- 
				)
			{
				/* baue ein Addier-Subtrahier-Werk */
				AVariable *s,*c;
				for ( int bit=0 ; bit<=n ; bit++ )
				{
					BString * x;
					s=generateTempVariable(new ATypeBool());
					c=generateTempVariable(new ATypeBool());

					x=BString::cast( 
						p->getValueAt( new Integer(bit+step) )
					);

					subAdd(
						x,
						av2->getBitName(bit),
						cin->getName(),
						sa->getName(),
						c->getName(),
						s->getName()
					);
					cin=c;
					p->addAssoc( new Integer(bit+step),BString::cast(s->getName()));
				}
				/* neues Sub-Add-Flag */
				sa=generateTempVariable(new ATypeBool());
				fprintf(fout,"*BOOLEAN-EQUATIONS\n %s=/(%s#%s);\n",
					sa->getName()->content(),
					av2->getBitName(n)->content(),
					s->getName()->content()
				);

				/* vorlaeufiger Quotient (vor increment) */
				fprintf(fout," %s=%s;\n",
					qt->getBitName(step)->content(),
					sa->getName()->content()
				);
			}

			sa=generateTempVariable(new ATypeBool());

			fprintf(fout,"*BOOLEAN-EQUATIONS\n %s=/(%s#%s);\n",
				sa->getName()->content(),
				BString::cast( p->getValueAt(new Integer(n)) )->content(),
				av2->getBitName(n)->content()
			);

			cin=sa;

			for ( int bit=0 ; bit<=n ; bit++ )
			{
				AVariable *s,*c;

				s=generateTempVariable(new ATypeBool());
				c=generateTempVariable(new ATypeBool());

				subAdd(
					BString::cast( p->getValueAt(new Integer(bit)) ),
					av2->getBitName(bit),
					cin->getName(),
					sa->getName(),
					c->getName(),
					s->getName()
				);
				cin=c;
				p->addAssoc( new Integer(bit+step),BString::cast(s->getName()));
			}

			AVariable * inc;
			inc=generateTempVariable(new ATypeRange(new Integer(0),new Integer(1)));

			fprintf(fout,"*BOOLEAN-EQUATION\n %s= %s#(%s&(",
				inc->getName()->content(),
				av2->getBitName(n)->content(),
				signP->content()
			);

			for ( i=0 ; i<n ; i++ )
			{
				if (0!=i) { fprintf(fout,"+"); }

				fprintf(fout,"%s",
					BString::cast(p->getValueAt(new Integer(i)))->content()
				);
			}

			fprintf(fout,"));\n\n");

			cache=(new ExAdd(qt,inc))->logic();

			
		} /* "AVariable * ExDiv::logic()-var/var" */

	} /* (NULL==asConstant()) */


	DEBUG_exit("AVariable * ExDiv::logic()")

	return cache;
}

