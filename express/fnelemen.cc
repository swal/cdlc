#include "fnelemen.h"
#include "../library/integer.h"
#include "../klassen/avariabl.h"
#include "../klassen/atrange.h"
#include "../klassen/atbool.h"
#include "../klassen/atenum.h"
#include "../main/main.h"
#include "../main/logic.h"


#define SUPERCLASS Expression
INITIALIZE_CONCRETE_CLASS(FnElement, SUPERCLASS);


FnElement::FnElement()
{
	DEBUG_enter("FnElement::FnElement()")
	myType=NULL;
	myEx  =NULL;
	DEBUG_exit("FnElement::FnElement()")
}

FnElement::FnElement(AType* type,Object * exList)
{
	DEBUG_enter("FnElement::FnElement(AType* type,Object * exList)")
	
	if ( 1!=List::cast(exList)->getSize() )
	{ error("element()-function should have one parameter"); }

	if ( !type->isKindOf(ATypeEnum::ClassName) )
	{ semerror(type,"argument of element() isn't of enum type"); }

	myType=type;
 
	myEx=
		Expression::cast(
			ListObject::cast(
				List::cast(exList)->at(1)
			)->getObject()
		);

	if (!myEx->getType()->isKindOf(ATypeRange::ClassName))
	{ semerror(NULL,"expression argument of element() isn't of type int"); }
	if ( ATypeRange::cast(myEx->getType())->getLower()->value()<0 )
	{ warning("argument of element() may be negativ"); }
	if (ATypeRange::cast(myEx->getType())->getUpper()->value()
		  > (type->asGroup()->getSize()-1) )
	{ warning("argument of element() may be to great"); }
		
	DEBUG_exit("FnElement::FnElement(AType* type,Object * exList)")
}

AType * FnElement::getType()
{
	DEBUG_enter("AType * FnElement::getType()")
	DEBUG_exit("AType * FnElement::getType()")
	return myType;
}
	
AConstant * FnElement::asConstant()
{
	AConstant * ac;
	DEBUG_enter("AConstant * FnElement::asConstant()")
	ac=myEx->asConstant();
	if ( NULL!=ac )
	{
		ac=AConstant::cast(ListObject::cast(myType->asGroup()->at(AConInt::cast(ac)->value()))->getObject());
	}
	DEBUG_exit("AConstant * FnElement::asConstant()")
	return ac;
}

AVariable * FnElement::logic()
{
	AVariable * cache;
	DEBUG_enter("AVariable * FnElement::logic()")
	if(NULL!=asConstant())
	{ cache=new AVariable(new BString(""),getType()); }
	else
	{
		AVariable * result;
		
		result=myEx->logic();
		cache=generateTempVariable(getType());
		fprintf(fout,"; element():\n*BOOLEAN-EQUATIONS\n %s = %s ;\n\n",
			cache ->getName()->content(),
			result->getName(new ATypeRange(
				new Integer(0),
				new Integer(ATypeEnum::cast(getType())->asGroup()->getSize()-1)
				) )->content()
		);
	}
	cache->setConstant(asConstant());

	DEBUG_exit("AVariable * FnElement::logic()")
	return cache;
}
