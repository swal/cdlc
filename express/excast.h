/*
 * $Log$
 *
 */

/*$Id$*/

#ifndef EXPRESSION_CAST_H
#define EXPRESSION_CAST_H

#include "../express/express.h"

class ExCast : public Expression
{
	private:
		BString * subtype;
		Expression * ex;

	public:
		ExCast();
		ExCast(Object*subt,Object*expression);
		virtual AType * getType();
		virtual AConstant * asConstant();
		virtual AVariable * logic();
		
	DEFAULT_CONCRETE_METHODS(ExCast,Expression);
};

#endif
