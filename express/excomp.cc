/*
 * $Log$
 *
 */

/* $Id$ */ 

#include "excomp.h"
#include "../main/debug.h"
#include "../klassen/atbool.h"
#include "../klassen/acbool.h"
#include "../library/bstring.h"
#include "../main/error.h"
#include "../grammar/parser.h"
#include "../express/exrecord.h"

#define SUPERCLASS Expression
INITIALIZE_CONCRETE_CLASS(ExComp, SUPERCLASS);

/* component reference expression: "myEx.myComp" */

ExComp::ExComp()
{
    DEBUG_enter("ExComp::ExComp()")
    myEx=NULL;
    myComp=NULL;
    myPos=0;
    DEBUG_exit("ExComp::ExComp()")
}

ExComp::ExComp( Object * ex, Object * comp )
{    
    DEBUG_enter("ExComp::ExComp( Object * ex, Object * comp )")
    
    if (!ex ->isKindOf(Expression::ClassName)) 
    { error("internal error: ExComp's Subexs isn't an Expr"); }

    if (!comp->isKindOf(BString::ClassName))
    { error("internal error: ExComp's comp isn't a BString"); }

    if (!Expression::cast(ex)->getType()->isKindOf(ATypeRecord::ClassName))
    { semerror(NULL,"type conflict: expression of 'subtype' isn't a record"); }

    myPos=ATypeRecord::cast(Expression::cast(ex)->getType())
        ->getPositionOf(BString::cast(comp));

    if ( 0==myPos )
    { semerror(ex,"record expression doesn't have such a member"); }
    
    myEx=Expression::cast(ex);   /* ExRecord::cast(ex)->getExpressionAt(pos); */
    myComp=BString::cast(comp);

    DEBUG_exit("ExComp::ExComp( Object * ex, Object * comp )")
}

AType * ExComp::getType()
{
    AType * at;
    DEBUG_enter("AType * ExComp::getType()")
    
    at=ATypeRecord::cast(myEx->getType())->getTypeAt(myPos);
   
    DEBUG_exit("AType * ExComp::getType()")
    return at;
}

AConstant * ExComp::asConstant()
{
    AConstant *c;
   
    DEBUG_enter("AConstant * ExComp::asConstant()")
    
    c=myEx->asConstant();

    if (NULL!=c)
    {
        Object * o;

        o=ListObject::cast(AConRecord::cast(c)->at(myPos))->getObject();
        DEBUG_str(BString::cast(o->className())->content())
        c=AConstant::cast( o );
    }
    
    DEBUG_exit("AConstant * ExComp::asConstant()")
    return c;
}

AVariable * ExComp::logic()
{
    AVariable * v;
   
    DEBUG_enter("AVariable * ExComp::logic()")

    v=myEx->logic()->getSubName(myComp);
 
    DEBUG_exit("AVariable * ExComp::logic()")
    return v;
}

