/*
 * $Log$
 *
 */

/*$Id$*/

#ifndef EXPRESSION_XOR_H
#define EXPRESSION_XOR_H

#include "../express/express.h"

class ExXor : public Expression
{
	private:
		Expression * ex1, * ex2;
	
	public:
		ExXor();
		ExXor( Object * exleft, Object * exright );
		virtual AType * getType();
		virtual AConstant * asConstant();
		virtual AVariable * logic();

	DEFAULT_CONCRETE_METHODS(ExXor,Expression);
};

#endif
