/*
 * $Log$
 *
 */

/* $Id$ */ 

#include "exlt.h"
#include "../main/debug.h"
#include "../klassen/atbool.h"
#include "../klassen/atenum.h"
#include "../klassen/atrange.h"
#include "../klassen/acbool.h"
#include "../library/bstring.h"
#include "../main/error.h"
#include "../grammar/parser.h"


#define SUPERCLASS Expression
INITIALIZE_CONCRETE_CLASS(ExLt, SUPERCLASS);


ExLt::ExLt()
{
    DEBUG_enter("ExLt::ExLt()")
    ex1=NULL;
    ex2=NULL;
    DEBUG_exit("ExLt::ExLt()")
}


ExLt::ExLt( Object * exleft, Object * exright )
{
    DEBUG_enter("ExLt::ExLt( Object * exleft, Object * exright )")
    
    if (!exleft ->isKindOf(Expression::ClassName))
    { error("internal error: one of ExLt's Subexs isn't an Expr"); }
    if (!exright->isKindOf(Expression::ClassName))
    { error("internal error: one of ExLt's Subexs isn't an Expr"); }
    if (!(  (Expression::cast(exleft)->getType()->isKindOf(ATypeEnum::ClassName))
          ||(Expression::cast(exleft)->getType()->isKindOf(ATypeRange::ClassName))
        ) )
    { semerror(exleft,"left argument of relational operator has wrong type"); }
    if (!(  (Expression::cast(exright)->getType()->isKindOf(ATypeEnum::ClassName))
          ||(Expression::cast(exright)->getType()->isKindOf(ATypeRange::ClassName))
        ) )
    { semerror(exright,"right argument of relational operator has wrong type"); }
    
    ex1=Expression::cast(exleft );
    ex2=Expression::cast(exright);
    DEBUG_exit("ExLt::ExLt( Object * exleft, Object * exright )")
}


AType * ExLt::getType()
{
    DEBUG_enter("AType * ExLt::getType()")
    DEBUG_exit("AType * ExLt::getType()")
    return new ATypeBool();
}


AConstant * ExLt::asConstant()
{
    AConstant * ac1, *ac2, *cache;
    DEBUG_enter("AConstant * ExLt::asConstant()")
    
    ac1=ex1->asConstant();
    ac2=ex2->asConstant();
    if ((NULL!=ac1) && (NULL!=ac2))
    {
        if (ac1->getType()->isKindOf(ATypeRange::ClassName))
        {
            cache= new AConBool(
                AConInt::cast(ac1)->value() < AConInt::cast(ac2)->value()
            );
        }
        else
        {
            /* AConEnum */
            cache= new AConBool(
                ATypeEnum::cast(ac1->getType())->positionOf(ac1) <
                ATypeEnum::cast(ac1->getType())->positionOf(ac2)
            );
        }
    }
    else
    {
        cache= NULL;
    }
    DEBUG_exit("AConstant * ExLt::asConstant()")
    return cache;
}

AVariable * ExLt::logic()
{
    AVariable * cache;

    DEBUG_enter("AVariable * ExLt::logic()")
    
    cache=generateTempVariable(getType());   

    if (NULL!=asConstant())
    { 
        cache->setConstant(asConstant());
    }
    else
    {
        AVariable *v1,*v2;
        v1=ex1->logic();
        v2=ex2->logic();
        fprintf(fout,"; ExLt:\n*BOOLEAN-EQUATIONS\n %s = %s < %s ;\n\n",
            cache->getName()->content(),
            v1->getName()->content(),
            v2->getName()->content()
        );
    }
    DEBUG_exit("AVariable * ExLt::logic()")
    return cache;
}

