/*
 * $Log$
 *
 */

/*$Id$*/

#ifndef FNRANDOM_H
#define FNRANDOM_H

#include "../express/express.h"
#include "../klassen/atype.h"

class FnRandom : public Expression
{
	protected:
		int max;
		int num;

	public:
		FnRandom();
		FnRandom( Object * exList );
		
		virtual AType     * getType();
		virtual AConstant * asConstant();
		virtual AVariable * logic();
		
	DEFAULT_CONCRETE_METHODS(FnRandom,Expression);
};

#endif
