/*
 * $Log$
 *
 */

/*$Id$*/

#ifndef EX_ONE_H
#define EX_ONE_H

#include "../express/express.h"
#include "../klassen/atype.h"
#include "../klassen/atbool.h"
#include "../klassen/acbool.h"
#include "../klassen/aloopvar.h"

class ExOne : public Expression
{
    protected:
        ALoopVarList* myList;
        Expression * myEx;
        
	public:
		ExOne();
		ExOne(Object *loopVarList,Object * ex);

		virtual AType     * getType();
		virtual AConstant * asConstant();
		virtual AVariable * logic();
		
	DEFAULT_CONCRETE_METHODS(ExOne,Expression);
};

#endif
