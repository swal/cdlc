/*
 * $Log$
 *
 */

/*$Id$*/

#ifndef EXPRESSION_RECORD_H
#define EXPRESSION_RECORD_H

#include "../express/express.h"
#include "../klassen/atype.h"
#include "../klassen/aconstnt.h"
#include "../library/list.h"

class ExRecord : public Expression
{
	private:
		List * li;

	public:
		ExRecord();
		ExRecord( Object * list);
		virtual Expression * getExpressionAt(int pos);
		virtual AType * getType();
		virtual AConstant * asConstant();
		virtual AVariable * logic();
		
	DEFAULT_CONCRETE_METHODS(ExRecord,Expression);
};

#endif
