/*
 * $Log$
 *
 */

/* $Id$ */ 

#include "exnum.h"
#include "../klassen/atenum.h"
#include "../library/bstring.h"
#include "../main/error.h"
#include "../main/debug.h"
#include "../main/logic.h"
#include "../klassen/acint.h"
#include "../grammar/parser.h"


#define SUPERCLASS Expression
INITIALIZE_CONCRETE_CLASS(ExNum, SUPERCLASS);


ExNum::ExNum()
{
    DEBUG_enter("ExNum::ExNum()")
    myList=NULL;
    myEx  =NULL;
/*
    myVariable=NULL;
    myGroup=NULL;
    myExpression=NULL;
*/    
    DEBUG_exit("ExNum::ExNum()")
}

/*
ExNum::ExNum(Object * var,Object * gr,Object * ex)
{
    DEBUG_enter("ExNum::ExNum(Object * var,Object * gr,Object * ex)")
    myVariable=AVariable::cast(var);
    myGroup=AGroup::cast(gr);
    myExpression=Expression::cast(ex);
    DEBUG_exit("ExNum::ExNum(Object * var,Object * gr,Object * ex)")
}
*/

ExNum::ExNum(Object *loopVarList,Object * ex)
{
    DEBUG_enter("ExNum::ExNum(List *loopVarList,Expression * ex)")
    if (!loopVarList->isKindOf(ALoopVarList::ClassName))
    { error("internal error: first argument of ExNum isn't a loop-var-list"); }
    if (!ex->isKindOf(Expression::ClassName))
    { error("internal error: second argument of ExNum isn't an expression"); }
    myList=ALoopVarList::cast(loopVarList);
    myEx=Expression::cast(ex);
    DEBUG_exit("ExNum::ExNum(List *loopVarList,Expression * ex)")
}

AType * ExNum::getType()
{
    AType * t;
    DEBUG_enter("AType * ExNum::getType()")
    t=new ATypeRange(new Integer(0), new Integer(myList->maxIteration()));
    DEBUG_exit("AType * ExNum::getType()")
    return t;
}

AConstant * ExNum::asConstant()
{
    DEBUG_enter("AConstant * ExNum::asConstant()")
    DEBUG_exit("AConstant * ExNum::asConstant()")
    /* no shortcut defined: to implement */
    return NULL;
}

AVariable * ExNum::logic()
{
    AVariable * v;
    List * sumList;
    int first;
    int i;
    
    DEBUG_enter("AVariable * ExNum::logic()")

    sumList=new List();

    i=1;

    /* calculate all conditions */
    
    while ( 0!=myList->setToIteration(i) )
    {
        AVariable *local ;
        
        local=myEx->logic();
        sumList->add(new ListObject(local));
        i++;
    }
    
/*    
    for(Iterator i(myGroup);i;i++)
    {
        AConstant *c;
        
        c=Expression::cast( ListObject::cast(i)->getObject() ) ->asConstant();
        if (NULL==c)
        { semerror(NULL,"can't evaluate group menber"); }

        myVariable->setConstant(c);
        
        sumList->add( new ListObject(myExpression->logic()));
    }
*/

    /* die Anzahlvariable : */
    v=generateTempVariable(getType());

    int lineLength;
    lineLength=6;

    fprintf(fout,"; num-expression calc-number\n*FUNCTION-TABLE\n$HEADER :\n X [");
    first=1;
    for(Iterator j(sumList);j;j++)
    {
        char * varName;

        if (!first)
        {
            fprintf(fout,",");
            lineLength+=1;
        }
        first=0;
        varName= AVariable::cast(ListObject::cast(j)->getObject())
                                                  ->getName()->content();
        lineLength+=strlen(varName);
        if (lineLength>=maxLineLength) 
        {
            fprintf(fout,"\n");
            lineLength=strlen(varName);
        }
      
        fprintf(fout,"%s",varName);
    }
    fprintf(fout,"]\n : Y %s ;\n",v->getName()->content());

    /* *** table follows *** */
    {
        int anz=1<<sumList->getSize();
        for (int j=0;j<anz;j++)
        {
            int ones,x;
            
            ones=0;
            x=1;
            while(x<=j)
            {
                if (0!=(x&j)) {ones++;}
                x=x<<1;
            }

            fprintf(fout," X %*uD : Y %uD ;\n",4,j,ones);
        }
    }

    fprintf(fout,"\n");
    
    DEBUG_exit("AVariable * ExNum::logic()")
    return v;
}

