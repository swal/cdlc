/*
 * $Log$
 *
 */

/* $Id$ */ 

#include "exall.h"
#include "../klassen/atenum.h"
#include "../klassen/atbool.h"
#include "../klassen/acint.h"
#include "../klassen/acbool.h"
#include "../library/bstring.h"
#include "../main/error.h"
#include "../main/debug.h"
#include "../grammar/parser.h"
#include "../express/exnot.h"


#define SUPERCLASS Expression
INITIALIZE_CONCRETE_CLASS(ExAll, SUPERCLASS);


ExAll::ExAll()
{
    DEBUG_enter("ExAll::ExAll()")
    myList=NULL;
    myEx  =NULL;
    DEBUG_exit("ExAll::ExAll()")
}

ExAll::ExAll(Object *loopVarList,Object * ex)
{
    DEBUG_enter("ExAll::ExAll(List *loopVarList,Expression * ex)")
    if (!loopVarList->isKindOf(ALoopVarList::ClassName))
    { error("internal error: first argument of ExAll isn't a loop-var-list"); }
    if (!ex->isKindOf(Expression::ClassName))
    { error("internal error: second argument of ExAll isn't an expression"); }
    myList=ALoopVarList::cast(loopVarList);
    myEx=Expression::cast(ex);
    DEBUG_exit("ExAll::ExAll(List *loopVarList,Expression * ex)")
}

AType * ExAll::getType()
{
    DEBUG_enter("AType * ExAll::getType()")
    DEBUG_exit("AType * ExAll::getType()")
    return new ATypeBool();
}

AConstant * ExAll::asConstant()
{
    /* no shortcut defined: to implement */
    return NULL;
}

AVariable * ExAll::logic()
{
    AVariable *result;
    List *resList, *nresList;
    int i;

    DEBUG_enter("AVariable * ExAll::logic()")
    
    resList=new List();

    i=1;


    /* calculate all conditions */
    
    while ( 0!=myList->setToIteration(i) )
    {
        AVariable *local ;
        
        local=myEx->logic();
        resList->add(new ListObject(local));
        i++;
    }


    /* 'or' all conditions */
    
    result=generateTempVariable(new ATypeBool());

    fprintf(fout,"; and all ExAll-conditions:\n*BOOLEAN-EQUATIONS\n %s = (",
        result->getName()->content()
    );

    for( Iterator j(resList) ; j ; j++ )
    {
        if ( j!=resList->first() ) { fprintf(fout,"&"); }
        fprintf(fout," %s ",
            AVariable::cast(ListObject::cast(j)->getObject())->getName()->content()
        );
    }
    fprintf(fout,") ;\n\n");


    /* set loop vars (side effect) */
    
    /* invert all conditions in resList to nresList */
    nresList=new List();
    for ( Iterator k(resList) ; k ; k++ )
    {
        nresList->add(
            new ListObject(
                (new ExNot(
                    AVariable::cast(ListObject::cast(k)->getObject())
                ))->logic()
            )
        );
    }


    myList->assignResults(nresList); 

    DEBUG_exit("AVariable * ExAll::logic()")

    return result;
}

