/*
 * $Log$
 *
 */

/*$Id$*/

#ifndef EXPRESSION_CELL_H
#define EXPRESSION_CELL_H

#include "../express/express.h"
#include "../klassen/atype.h"
#include "../klassen/aconstnt.h"

class ExCell : public Expression
{
	private:
		Expression * ex;
	    int calcXY();

	public:
		ExCell();
		ExCell( Object * expression );

		virtual AType * getType();
		virtual AConstant * asConstant();
		virtual AVariable * logic();
		
	DEFAULT_CONCRETE_METHODS(ExCell,Expression);
};

#endif
