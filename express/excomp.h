/*
 * $Log$
 *
 */

/*$Id$*/

#ifndef EXPRESSION_COMP_H
#define EXPRESSION_COMP_H

#include "../express/express.h"
#include "../klassen/atrecord.h"
#include "../library/bstring.h"

class ExComp : public Expression
{
	private:
		Expression * myEx;
		BString * myComp;
		int myPos;
	
	public:
		ExComp();
		ExComp( Object * ex, Object * comp );
		virtual AType * getType();
		virtual AConstant * asConstant();
		virtual AVariable * logic();
		
	DEFAULT_CONCRETE_METHODS(ExComp,Expression);
};

#endif
