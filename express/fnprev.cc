
#include "fnprev.h"
#include "../library/integer.h"
#include "../klassen/avariabl.h"
#include "../klassen/atrange.h"
#include "../klassen/atenum.h"
#include "../klassen/atbool.h"
#include "../klassen/acenum.h"
#include "../statmnt/stassign.h"
#include "../main/main.h"
#include "../main/logic.h"


#define SUPERCLASS Expression

INITIALIZE_CONCRETE_CLASS(FnPrev, SUPERCLASS);


FnPrev::FnPrev()
{
    DEBUG_enter("FnPrev::FnPrev()")
    DEBUG_exit("FnPrev::FnPrev()")
}

FnPrev::FnPrev(Object * exList)
{
    DEBUG_enter("FnPrev::FnPrev(Object * exList)")
    if ( 1!=List::cast(exList)->getSize() )
    { error("wrong number of parameters for next-function"); }
    myEx=Expression::cast(ListObject::cast(List::cast(exList)->at(1))->getObject());
    if ( !myEx->getType()->isKindOf(ATypeEnum::ClassName) )
    { error("argument of next() isn't of type enum"); }
    DEBUG_exit("FnPrev::FnPrev(Object * exList)")
}

AType * FnPrev::getType()
{
    AType * at;
    DEBUG_enter("AType * FnPrev::getType()")
    at=myEx->getType();
    DEBUG_exit("AType * FnPrev::getType()")
    return at;
}
    
AConstant * FnPrev::asConstant()
{
    AConstant * c;
    DEBUG_enter("AConstant * FnPrev::asConstant()")
    if (NULL!=myEx->asConstant())
    {
        ATypeEnum *ate;
        int pos;

        ate=ATypeEnum::cast(myEx->getType());
        pos=ate->positionOf(myEx->asConstant());

        if (0==pos) { pos=ate->asGroup()->getSize(); }

        c=AConEnum::cast(
          ListObject::cast(ate->asGroup()->at(pos))->getObject()
        );
    }
    else
    {
        c=NULL;
    }
    DEBUG_exit("AConstant * FnPrev::asConstant()")
    return c;
}


AVariable * FnPrev::logic()
{
    AVariable * v;

    DEBUG_enter("AVariable * FnPrev::logic()")
    
    if (NULL!=asConstant())
    { 
        v=new AVariable(new BString(""),getType()); 
        v->setConstant(asConstant());    
    }
    else
    {
        AGroup * g;
        AVariable * e;

        e=myEx->logic();

        v=generateTempVariable(getType());
        fprintf(fout,"; FnPrev:\n*FUNCTION-TABLE\n$HEADER:\n X %s : Y %s ;\n",
            e->getName()->content(),
            v->getName()->content()
        );

        g=ATypeEnum::cast(getType())->asGroup();

        for ( int i=g->getSize() ; i>1 ; i-- )
        {
            fprintf(fout," X %s : Y %s ;\n",
                getType()->codingOf(
                    AConEnum::cast(ListObject::cast(g->at(i))->getObject())
                )->content(),
                getType()->codingOf(
                    AConEnum::cast(ListObject::cast(g->at(i-1))->getObject())
                )->content()
            );
        }

        i=g->getSize();

            fprintf(fout," X %s : Y %s ;\n\n",
                getType()->codingOf(
                    AConEnum::cast(ListObject::cast(g->at(1))->getObject())
                )->content(),
                getType()->codingOf(
                    AConEnum::cast(ListObject::cast(g->at(i))->getObject())
                )->content()
            );
                                            
    }
    DEBUG_exit("AVariable * FnPrev::logic()")
    return v;
}
