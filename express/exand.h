/*
 * $Log$
 *
 */

/*$Id$*/

#ifndef EXPRESSION_AND_H
#define EXPRESSION_AND_H

#include "../express/express.h"

class ExAnd : public Expression
{
	private:
		Expression * ex1, * ex2;

	public:
		ExAnd();
		ExAnd( Object * exleft, Object * exright );
		virtual AType * getType();
		virtual AConstant * asConstant();
		virtual AVariable * logic();
		
	DEFAULT_CONCRETE_METHODS(ExAnd,Expression);
};

#endif
