/*
 * $Log$
 *
 */

/*$Id$*/

#ifndef EXFUNCTION_H
#define EXFUNCTION_H

#include "../express/express.h"
#include "../klassen/atype.h"

class ExFunction : public Expression
{
	protected:
	    Expression * myFunc;	    

	public:
		ExFunction();
		ExFunction( Object * name, Object * exList );
		virtual AType * getType();
		virtual AConstant * asConstant();
		virtual AVariable * logic();
		
	DEFAULT_CONCRETE_METHODS(ExFunction,Expression);
};

#endif
