/*
 * $Log$
 *
 */

/*$Id$*/

#ifndef FNPREV_H
#define FNPREV_H

#include "../express/express.h"
#include "../klassen/atype.h"

class FnPrev : public Expression
{
	protected:
		Expression * myEx;

	public:
		FnPrev();
		FnPrev( Object * exList );
		
		virtual AType     * getType();
		virtual AConstant * asConstant();
		virtual AVariable * logic();
		
	DEFAULT_CONCRETE_METHODS(FnPrev,Expression);
};

#endif
