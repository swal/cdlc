/*
 * $Log$
 *
 */

/*$Id$*/

#ifndef EXPRESSION_MULT_H
#define EXPRESSION_MULT_H

#include "../express/express.h"
#include "../klassen/atype.h"
#include "../klassen/aconstnt.h"

class ExMult : public Expression
{
	private:
		Expression * ex1, * ex2;

	public:
		ExMult();
		ExMult( Object * exleft, Object * exright );
		virtual AType * getType();
		virtual AConstant * asConstant();
		virtual AVariable * logic();
		
	DEFAULT_CONCRETE_METHODS(ExMult,Expression);
};

#endif
