/*
 * $Log$
 *
 */

/*$Id$*/

#ifndef EXPRESSION_ADD_H
#define EXPRESSION_ADD_H

#include "../express/express.h"
#include "../klassen/atype.h"

class ExAdd : public Expression
{
	private:
		Expression * ex1, * ex2;

	
	public:
		ExAdd();
		ExAdd( Object * exleft, Object * exright );
		virtual AType * getType();
		virtual AConstant * asConstant();
		virtual AVariable * logic();
		
	DEFAULT_CONCRETE_METHODS(ExAdd,Expression);
};

#endif
