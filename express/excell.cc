/*
 * $Log$
 *
 */

/* $Id$ */ 

#include "excell.h"
#include "../main/debug.h"
#include "../main/error.h"
#include "../main/main.h"
#include "../klassen/atbool.h"
#include "../klassen/atrecord.h"
#include "../klassen/atrange.h"
#include "../klassen/acbool.h"
#include "../klassen/acrecord.h"
#include "../klassen/acint.h"
#include "../library/bstring.h"
#include "../library/listobj.h"
#include "../grammar/parser.h"


#define SUPERCLASS Expression
INITIALIZE_CONCRETE_CLASS(ExCell, SUPERCLASS);


ExCell::ExCell()
{
    DEBUG_enter("ExCell::ExCell()")
    ex=NULL;
    DEBUG_exit("ExCell::ExCell()")
}

ExCell::ExCell( Object * expression )
{
    int i;

    DEBUG_enter("ExCell::ExCell( Object * expression )")

    if (NULL==expression)
    { error("internal error: ExCell::ExCell( Object * ex ): ex==NULL"); }
    
    if (!expression->isKindOf(Expression::ClassName) )
    { error("internal error: one of ExCell's Subexs isn't an Expr"); }
    
    if (!Expression::cast(expression)->getType()->isKindOf(ATypeRecord::ClassName))
    { semerror(NULL,"type conflict: argument for cellrefference isn't a record"); }

    setObligValues();

    if ( 2!=dimension )   { semerror(NULL,"const dimension isn't 2"); }

    if ( 1!=distance )    { semerror(NULL,"const distance isn't 1"); }

    for (i=1;i<=dimension;i++)
    {
        AType * subType;
        subType=ATypeRecord::cast(Expression::cast(expression)->getType())
                ->getTypeAt(i);

        if (NULL==subType)
        { semerror(NULL,"record argument for cellref has wrong dimension"); }

        if ( ! subType->isKindOf(ATypeRange::ClassName) )
        { semerror(NULL,"record member for cellref isn't integer"); }
    }

    if (NULL!=
        ATypeRecord::cast(Expression::cast(expression)->getType())
            ->getTypeAt(dimension+1)
    )
    { semerror(NULL,"record argument for cellref has wrong dimension"); }

        ex=Expression::cast(expression );
    
    DEBUG_exit("ExCell::ExCell( Object * expression )")
}

AType * ExCell::getType()
{
    DEBUG_enter("AType * ExCell::getType()")
    DEBUG_exit("AType * ExCell::getType()")
    return celltype;
}


static int x,y;

int ExCell::calcXY()
{
    AConstant * celladdr;

    DEBUG_enter("int ExCell::calcXY()")

    celladdr=ex->asConstant();
    if (NULL==celladdr)
    { 
        DEBUG_exit("int ExCell::calcXY()--error")
        return 0;
    }
    else
    {
        if (!celladdr->isKindOf(AConRecord::ClassName))
        { 
            semerror(NULL,"argument for cellref '*' isn't a record"); 
            DEBUG_exit("int ExCell::calcXY()--error")
            return 0;
        }
        else   // celladdr _is_ AConRecord
        {
            Object *index;

            DEBUG_msg("int ExCell::calcXY() - test indices of record")

            index=ListObject::cast(AConRecord::cast(celladdr)->at(1))->getObject();
            if (NULL==index) { semerror(NULL,"index of cellref isn't defined"); }
            if (!index->isKindOf(AConInt::ClassName))
            { 
                DEBUG_str( BString::cast(index->className())->content() );
                semerror(NULL,"index for cellref isn't integer");
            }
            x=AConInt::cast(index)->value();

            index=ListObject::cast(AConRecord::cast(celladdr)->at(2))->getObject();
            if (NULL==index) { semerror(NULL,"index of cellref isn't defined"); }
            if (!index->isKindOf(AConInt::ClassName))
            { 
                DEBUG_str( BString::cast(index->className())->content() );
                semerror(NULL,"index for cellref isn't integer");
            }
            y=AConInt::cast(index)->value();

            
            if (NULL!=AConRecord::cast(celladdr)->at(3) )
            { semerror(NULL,"argument for cellref has wrong dimension"); }

            DEBUG_exit("int ExCell::calcXY() - ok - get value of CN")
            return 1;
        }
    }    
}


AConstant * ExCell::asConstant()
{
    AConstant *c;
    Object * cn;

    DEBUG_enter("AConstant * ExCell::asConstant()")

    calcXY();

    if ( (0!=x) || (0!=y) )
    {
        DEBUG_exit("AConstant * ExCell::asConstant()")
        return NULL;
    }
    cn=identifier->getValueAt( new BString("_CN")); 
    if (NULL==cn) 
    {
        semerror(NULL,"center cell isn't defined");
    }
    c=Expression::cast(cn)->asConstant();

    DEBUG_exit("AConstant * ExCell::asConstant()")
    return c;
}

AVariable * ExCell::logic()
{
    AVariable *v;

    DEBUG_enter("AVariable * ExCell::logic()")
       
    if (!calcXY())
    {
        /* exp isn't constant .. so we have do code the whole table */ 

        AVariable * iv;
        ATypeRange *tx, *ty;
        
        iv=ex->logic();
        tx=ATypeRange::cast(ATypeRecord::cast(ex->getType())->getTypeAt(1));
        ty=ATypeRange::cast(ATypeRecord::cast(ex->getType())->getTypeAt(2));

        v=generateTempVariable(celltype);

        fprintf(fout,"; cell-ref expression\n*FUNCTION-TABLE\n$HEADER:\n");
        fprintf(fout," X %s : Y %s ;\n",
            iv->getName()->content(),
            v->getName()->content()
        );
        fprintf(fout," X  %s %s  : Y %s=%s ;\n",
                    tx->codingOf(new AConInt(0))->content(),
                    ty->codingOf(new AConInt(0))->content(),
                    v->getName()->content(),
                    AVariable( new BString("_CN"),celltype).getName()->content()
                );
        fprintf(fout," X  %s %s  : Y %s=%s ;\n",
                    tx->codingOf(new AConInt(0))->content(),
                    ty->codingOf(new AConInt(1))->content(),
                    v->getName()->content(),
                    AVariable( new BString("_NO"),celltype).getName()->content()
                );
        fprintf(fout," X  %s %s  : Y %s=%s ;\n",
                    tx->codingOf(new AConInt(1))->content(),
                    ty->codingOf(new AConInt(1))->content(),
                    v->getName()->content(),
                    AVariable( new BString("_NE"),celltype).getName()->content()
                );
        fprintf(fout," X  %s %s  : Y %s=%s ;\n",
                    tx->codingOf(new AConInt(1))->content(),
                    ty->codingOf(new AConInt(0))->content(),
                    v->getName()->content(),
                    AVariable( new BString("_EA"),celltype).getName()->content()
                );
        fprintf(fout," X  %s %s  : Y %s=%s ;\n",
                    tx->codingOf(new AConInt(1))->content(),
                    ty->codingOf(new AConInt(-1))->content(),
                    v->getName()->content(),
                    AVariable( new BString("_SE"),celltype).getName()->content()
                );
        fprintf(fout," X  %s %s  : Y %s=%s ;\n",
                    tx->codingOf(new AConInt(0))->content(),
                    ty->codingOf(new AConInt(-1))->content(),
                    v->getName()->content(),
                    AVariable( new BString("_SO"),celltype).getName()->content()
                );
        fprintf(fout," X  %s %s  : Y %s=%s ;\n",
                    tx->codingOf(new AConInt(-1))->content(),
                    ty->codingOf(new AConInt(-1))->content(),
                    v->getName()->content(),
                    AVariable( new BString("_SW"),celltype).getName()->content()
                );
        fprintf(fout," X  %s %s  : Y %s=%s ;\n",
                    tx->codingOf(new AConInt(-1))->content(),
                    ty->codingOf(new AConInt(0))->content(),
                    v->getName()->content(),
                    AVariable( new BString("_WE"),celltype).getName()->content()
                );
        fprintf(fout," X  %s %s  : Y %s=%s ;\n",
                    tx->codingOf(new AConInt(-1))->content(),
                    ty->codingOf(new AConInt(1))->content(),
                    v->getName()->content(),
                    AVariable( new BString("_NW"),celltype).getName()->content()
                );

        fprintf(fout," X   $REST  : Y  -  ;\n\n");


        DEBUG_exit("AVariable * ExCell::logic()")

        return v;
    }
    DEBUG_int(x)
    DEBUG_int(y)
    switch (x)
    {
        case -1: /* WEST */
            switch (y)
            {
                case -1:
                    v=new AVariable(new BString("_SW"),celltype);
                    break;
                case 0:
                    v=new AVariable(new BString("_WE"),celltype);
                    break;
                case 1:
                    v=new AVariable(new BString("_NW"),celltype);
                    break;
                default:
                    semerror(NULL,"index is out of distance");
            }
            break;
            
        case 0:
            switch (y)
            {
                case -1:
                    v=new AVariable(new BString("_SO"),celltype);
                    break;
                case 0:
                    v=new AVariable(new BString("_CN"),celltype);
                    break;
                case 1:
                    v=new AVariable(new BString("_NO"),celltype);
                    break;
                default:
                    semerror(NULL,"index is out of distance");
            }
            break;

        case 1: /* EAST */
            switch (y)
            {
                case -1:
                    v=new AVariable(new BString("_SE"),celltype);
                    break;
                case 0:
                    v=new AVariable(new BString("_EA"),celltype);
                    break;
                case 1:
                    v=new AVariable(new BString("_NE"),celltype);
                    break;
                default:
                    semerror(NULL,"index is out of distance");
            }
            break;
            
        default:
            semerror(NULL,"index is out of distance");
            
    }


    DEBUG_exit("AVariable * ExCell::logic()")
    return v;
}

