/*
 * $Log$
 *
 */

/*$Id$*/

#ifndef FNABS_H
#define FNABS_H

#include "../express/express.h"
#include "../klassen/atype.h"

class FnAbs : public Expression
{
	protected:
		Expression * myEx;

	public:
		FnAbs();
		FnAbs( Object * exList );
		
		virtual AType     * getType();
		virtual AConstant * asConstant();
		virtual AVariable * logic();
		
	DEFAULT_CONCRETE_METHODS(FnAbs,Expression);
};

#endif
