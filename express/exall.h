/*
 * $Log$
 *
 */

/*$Id$*/

#ifndef EX_ALL_H
#define EX_ALL_H

#include "../express/express.h"
#include "../klassen/atype.h"
#include "../klassen/aloopvar.h"

class ExAll : public Expression
{
    private:
        ALoopVarList * myList;
        Expression   * myEx;
        
	public:
		ExAll();
		ExAll(Object *loopVarList,Object * ex);
		
		virtual AType     * getType();
		virtual AConstant * asConstant();
		virtual AVariable * logic();
		
	DEFAULT_CONCRETE_METHODS(ExAll,Expression);
};

#endif
