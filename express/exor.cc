/*
 * $Log$
 *
 */

/* $Id$ */ 

#include "exor.h"
#include "../main/debug.h"
#include "../main/error.h"
#include "../main/main.h"
#include "../library/bstring.h"
#include "../klassen/atbool.h"
#include "../klassen/acbool.h"
#include "../grammar/parser.h"


#define SUPERCLASS Expression
INITIALIZE_CONCRETE_CLASS(ExOr, SUPERCLASS);


ExOr::ExOr()
{
    DEBUG_enter("ExOr::ExOr()")
    ex1=NULL;
    ex2=NULL;
    DEBUG_exit("ExOr::ExOr()")
}

ExOr::ExOr( Object * exleft, Object * exright )
{
    DEBUG_enter("ExOr::ExOr( Object * exleft, Object * exright )")
    if (!exleft ->isKindOf(Expression::ClassName))
    { error("internal error: left of ExOr isn't an Expr"); }
    if (!exright->isKindOf(Expression::ClassName))
    { error("internal error: right of ExOr isn't an Expr"); }
    if (!Expression::cast(exleft)->getType()->isKindOf(ATypeBool::ClassName))
    { semerror(NULL,"type conflict: left side of 'or' isn't bool"); }
    if (!Expression::cast(exright)->getType()->isKindOf(ATypeBool::ClassName))
    { semerror(NULL,"type conflict: right side of 'or' isn't bool"); }
   
    ex1=Expression::cast(exleft );
    ex2=Expression::cast(exright);
    DEBUG_exit("ExOr::ExOr( Object * exleft, Object * exright )")
}

AType * ExOr::getType()
{
    DEBUG_enter("AType * ExOr::getType()")
    DEBUG_exit("AType * ExOr::getType()")
    return new ATypeBool();
}

AConstant * ExOr::asConstant()
{
    AConstant * ac1, *ac2;
    AConstant * cache;
    
    DEBUG_enter("AConstant * ExOr::asConstant()")
    
    ac1=ex1->asConstant();
    ac2=ex2->asConstant();

    cache=NULL;
    
    if (NULL!=ac1) 
    { if ( AConBool::cast(ac1)->value() ) cache=new AConBool(1); }
    
    if ( (NULL==cache) && (NULL!=ac2) ) 
    { if ( AConBool::cast(ac2)->value() ) cache=new AConBool(1); }

    if ( (NULL==cache) && (NULL!=ac1) && (NULL!=ac2) )
    { 
        cache=new AConBool(
            AConBool::cast(ac1)->value() ||
            AConBool::cast(ac2)->value()
        ); 
    }

    DEBUG_exit("AConstant * ExOr::asConstant()")
    
    return cache;
}

AVariable * ExOr::logic()
{
    AVariable *v;
    AConstant *ac1,*ac2;

    DEBUG_enter("AVariable * ExOr::logic()")

    if (NULL!=asConstant())
    { 
        v=new AVariable(new BString(""),getType()); 
        v->setConstant(asConstant());
    }
    else
    {
        ac1=ex1->asConstant();
        ac2=ex2->asConstant();
        
        if (NULL!=ac1)
        { v=ex2->logic(); }
        else 
        {
            if (NULL!=ac2)
            { v=ex1->logic(); }
            else
            {
                /* neue Variable generieren und als *LOCAL bekannt machen */        
                v=generateTempVariable(getType()); 

                fprintf(fout,";  or-expression\n*BOOLEAN-EQUATIONS\n %s = %s + %s ;\n\n",
                    v           ->getName()->content(),
                    ex1->logic()->getName()->content(),
                    ex2->logic()->getName()->content()
                );
            }
        }
    }

    DEBUG_exit("AVariable * ExOr::logic()")

    return v;
}

