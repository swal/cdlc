/*
 * $Log$
 *
 */

/*$Id$*/

#ifndef EXPRESSION_NOT_H
#define EXPRESSION_NOT_H

#include "../express/express.h"
#include "../klassen/atype.h"
#include "../klassen/aconstnt.h"

class ExNot : public Expression
{
	private:
		Expression * myEx;

	
	public:
		ExNot();
		ExNot( Object * expression);
		virtual AType * getType();
		virtual AConstant * asConstant();
		virtual AVariable * logic();
		
	DEFAULT_CONCRETE_METHODS(ExNot,Expression);
};

#endif
