#include "fnrandom.h"
#include "../library/integer.h"
#include "../klassen/avariabl.h"
#include "../klassen/atrange.h"
#include "../klassen/atbool.h"
#include "../main/main.h"
#include "../main/logic.h"


#define SUPERCLASS Expression
INITIALIZE_CONCRETE_CLASS(FnRandom, SUPERCLASS);

int myNum=1;

FnRandom::FnRandom()
{
    DEBUG_enter("FnRandom::FnRandom()")
    max=-1;
    DEBUG_exit("FnRandom::FnRandom()")
}

FnRandom::FnRandom(Object * exList)
{
    Expression *myEx;
    AConInt *ac;
    DEBUG_enter("FnRandom::FnRandom(Object * exList)")
    if ( 1!=List::cast(exList)->getSize() )
    { error("random() should have one parameter"); }
    
    myEx=Expression::cast(ListObject::cast(List::cast(exList)->at(1))->getObject());
    
    if ( !myEx->getType()->isKindOf(ATypeRange::ClassName) )
    { error("argument of random() isn't of type enum"); }

    ac=AConInt::cast(myEx->asConstant());
    if (NULL==ac)
    { semerror(NULL,"argument of random() must be constant"); }

    max=ac->value();

    if ( 
         (max+1) != ( (1 << logd(max+1)) ) 
       )
    { error("not implemented: argument for random() must be power of 2 minus 1"); }
    
    num=myNum;
    myNum++;
    
    DEBUG_exit("FnRandom::FnRandom(Object * exList)")
}

AType * FnRandom::getType()
{
    AType * at;
    DEBUG_enter("AType * FnRandom::getType()")
    at=new ATypeRange(new Integer(0),new Integer(max));
    DEBUG_exit("AType * FnRandom::getType()")
    return at;
}
    
AConstant * FnRandom::asConstant()
{
    AConstant *ac;
    DEBUG_enter("AConstant * FnRandom::asConstant()")
    if (0==max)
    { ac=new AConInt(0); }
    else
    { ac=NULL; } /* is obviously never constant */
    DEBUG_exit("AConstant * FnRandom::asConstant()")
    return ac; 
}

AVariable * FnRandom::logic()
{
    AVariable * rnd, *out, *mark, *lowbit;
    char s[100];

    DEBUG_enter("AVariable * FnRandom::logic()")

    /* neue Variable generieren und als *LOCAL bekannt machen */

    /* hier nimmt man z.B. 31 Bit also 2^30..0 */

    if (NULL!=asConstant())
    {
        out=new AVariable(new BString(""),getType());
        out->setConstant(asConstant());
    }
    else
    {
        sprintf(s,"_%uRND",num);
        rnd=new AVariable(new BString(s),new ATypeBool());
        fprintf(fout,"*LOCAL\n %s[19..0];\n\n",rnd->getBaseName()->content());

        out =generateTempVariable(getType());
        mark=generateTempVariable(new ATypeBool());
        lowbit=generateTempVariable(new ATypeBool());


        fprintf(fout,"; random:\n");
        fprintf(fout,"*BOOLEAN-EQUATIONS\n");
    
// der catch, damit zu Anfang 1 gefuettert wird    
        fprintf(fout," %s := %s + %s19 ;\n",
            mark->getName()->content(),
            mark->getName()->content(),
            rnd->getBaseName()->content()
        );
        fprintf(fout," %s.clk = CLK_OUT ;\n",mark->getName()->content());

// calc low bit:
        fprintf(fout," %s = ( %s19 # %s16 ) + / %s ;\n",
            lowbit->getName()->content(),
            rnd->getBaseName()->content(),
            rnd->getBaseName()->content(),
            mark->getName()->content()
        );
    
        fprintf(fout," %s[19..0] := [ %s[18..0] , %s ] ;\n",
            rnd->getBaseName()->content(),
            rnd->getBaseName()->content(),
            lowbit->getName()->content()
        );
        fprintf(fout," %s[19..0].clk = CLK_OUT ;\n",rnd->getBaseName()->content());
    
        fprintf(fout," %s = %s[%u..0] ;\n\n",
            out->getName()->content(),
            rnd->getBaseName()->content(),
            ATypeRange::cast(getType())->getNumberOfBits()-1
        );
    }

    DEBUG_exit("AVariable * FnRandom::logic()")
    return out;
}
