/*
 * $Log$
 *
 */

/* $Id$ */ 

#include "exmod.h"
#include "../main/debug.h"
#include "../klassen/atenum.h"
#include "../klassen/atbool.h"
#include "../klassen/acint.h"
#include "../library/bstring.h"
#include "../main/error.h"
#include "../grammar/parser.h"

#include "../express/exdiv.h"


#define SUPERCLASS Expression
INITIALIZE_CONCRETE_CLASS(ExMod, SUPERCLASS);


ExMod::ExMod()
{
	DEBUG_enter("ExMod::ExMod()")
	ex1=NULL;
	ex2=NULL;
	DEBUG_exit("ExMod::ExMod()")
}

ExMod::ExMod( Object * exleft, Object * exright )
{
	DEBUG_enter("ExMod::ExMod( Object * exleft, Object * exright )")
	if (!exleft ->isKindOf(Expression::ClassName)) 
	{ error("internal error: one of ExMod's Subexs isn't an Expr"); }
	if (!exright->isKindOf(Expression::ClassName))
	{ error("internal error: one of ExMod's Subexs isn't an Expr"); }
	if (!Expression::cast(exleft)->getType()->isKindOf(ATypeRange::ClassName))
	{ semerror(NULL,"type conflict: left side of 'mod' isn't int"); }
	if (!Expression::cast(exright)->getType()->isKindOf(ATypeRange::ClassName))
	{ semerror(NULL,"type conflict: right side of 'mod' isn't int"); }
   
	ex1=Expression::cast(exleft );
	ex2=Expression::cast(exright);
	DEBUG_exit("ExMod::ExMod( Object * exleft, Object * exright )")
}

AType * ExMod::getType()
{
	int max1,max2;
	ATypeRange *t;
	DEBUG_enter("AType * ExMod::getType()")
	max1=Integer::cast(ATypeRange::cast(ex1->getType())->getUpper())->value();
	max2=Integer::cast(ATypeRange::cast(ex2->getType())->getUpper())->value()-1;
	t=new ATypeRange(new Integer(0),new Integer(max1<max2?max1:max2));
	DEBUG_exit("AType * ExMod::getType()")
	return t;
}

AConstant * ExMod::asConstant()
{
	AConstant * ac1, *ac2, *cache;

	DEBUG_enter("AConstant * ExMod::asConstant()")
	ac1=ex1->asConstant();
	ac2=ex2->asConstant();
	if ((NULL!=ac1) && (NULL!=ac2))
	{
		cache=new AConInt(AConInt::cast(ac1)->value() % AConInt::cast(ac2)->value() );
	}
	else
	{
		cache=NULL;
	}
	DEBUG_exit("AConstant * ExMod::asConstant()")
	return cache;
}

AVariable * ExMod::logic()
{

	AVariable * cache;
	AVariable * av1, *av2;

	int divByShift;
	
	
	DEBUG_enter("AVariable * ExMod::logic()")

	if (NULL!=asConstant())
	{
		cache=new AVariable(new BString(""),getType());
		cache->setConstant(asConstant());
	}
	else
	{
		av1=ex1->logic();
		av2=ex2->logic();
		
		cache=generateTempVariable(getType());
		divByShift=0;
		
		if (NULL!=av2->asConstant())
		{
			int c;
			c=AConInt::cast(av2->asConstant())->value();
			if ( c==(1<<logd(c)) )
			{
				fprintf(fout,"; mod:\n*BOOLEAN-EQUATIONS\n %s = %s[%u..0] ;\n\n",
					cache->getName()->content(),
					av1->getBaseName()->content(),
					getType()->getNumberOfBits()-1
				);
				divByShift=1;
			}
		}

		if (!divByShift)
		{
			DEBUG_msg("AVariable * ExMod::logic()-var/var")
		/*
		  Algorithmus aus:
		  R. Hoffmann: "Rechnerentwurf" Seite 132
		*/

			Dictionary * p; /* enthaelt aktuellen Zwischenwert */
			AVariable *qt; /* Quotient von +1 Korrektur */
			int n;  /* q=av2 [n..0] */
			int m;  /* p [m..0] */

			int i;

			n=av2->getType()->getNumberOfBits()-1;
			m=av1->getType()->getNumberOfBits()-1+n;

			p=new Dictionary();

			for ( i=0 ; i<=m ; i++ )
			{
				if (i<av1->getType()->getNumberOfBits())
				{
					p->addAssoc(
						new Integer(i),
						BString::cast(av1->getBitName(i))
					);
				}
				else
				{
					if (ATypeRange::cast(av1->getType())->isSigned())
					{
						p->addAssoc(
							new Integer(i),
							BString::cast(av1->getBitName(av1->getType()->getNumberOfBits()-1))
						);
					}
					else
					{
						p->addAssoc(
							new Integer(i),
							new BString("0")
						);

					}
				}
			}

			qt=generateTempVariable( this->getType() );

			AVariable * sa;
			AVariable *cin;

			BString * signP;

			signP=BString::cast( p->getValueAt(new Integer(m)) );

			sa=generateTempVariable(new ATypeBool());
			cin=sa;

			fprintf(fout,"*BOOLEAN-EQUATION\n %s = /(%s # %s)",
				sa->getName()->content(),
				signP->content(),  
				av2->getBitName(n)->content()
			);

			for (
				int step= (m-n) ;
				step>=0 ;
				step-- 
				)
			{
				/* baue ein Addier-Subtrahier-Werk */
				AVariable *s,*c;
				for ( int bit=0 ; bit<=n ; bit++ )
				{
					BString * x;
					s=generateTempVariable(new ATypeBool());
					c=generateTempVariable(new ATypeBool());

					x=BString::cast( 
						p->getValueAt( new Integer(bit+step) )
					);

					subAdd(
						x,
						av2->getBitName(bit),
						cin->getName(),
						sa->getName(),
						c->getName(),
						s->getName()
					);
					cin=c;
					p->addAssoc( new Integer(bit+step),BString::cast(s->getName()));
				}
				/* neues Sub-Add-Flag */
				sa=generateTempVariable(new ATypeBool());
				fprintf(fout,"*BOOLEAN-EQUATIONS\n %s=/(%s#%s);\n",
					sa->getName()->content(),
					av2->getBitName(n)->content(),
					s->getName()->content()
				);

				/* vorlaeufiger Quotient (vor increment) */
				fprintf(fout," %s=%s;\n",
					qt->getBitName(step)->content(),
					sa->getName()->content()
				);
			}

			sa=generateTempVariable(new ATypeBool());

			fprintf(fout,"*BOOLEAN-EQUATIONS\n %s=/(%s#%s);\n",
				sa->getName()->content(),
				BString::cast( p->getValueAt(new Integer(n)) )->content(),
				av2->getBitName(n)->content()
			);

			cin=sa;

			int bit;

			for ( bit=0 ; bit<=n ; bit++ )
			{
				AVariable *s,*c;

				s=generateTempVariable(new ATypeBool());
				c=generateTempVariable(new ATypeBool());

				subAdd(
					BString::cast( p->getValueAt(new Integer(bit)) ),
					av2->getBitName(bit),
					cin->getName(),
					sa->getName(),
					c->getName(),
					s->getName()
				);
				cin=c;
				p->addAssoc( new Integer(bit+step),BString::cast(s->getName()));
			}

			/* the specific modulo-part: */
			
			sa=generateTempVariable(new ATypeRange(new Integer(0),new Integer(1)));

			fprintf(fout,"*BOOLEAN-EQUATION\n %s= /%s&%s&(",
				sa->getName()->content(),
				av2->getBitName(n)->content(),
				signP->content()
			);

			cin=sa;

			for ( i=0 ; i<n ; i++ )
			{
				if (0!=i) { fprintf(fout,"+"); }

				fprintf(fout,"%s",
					BString::cast(p->getValueAt(new Integer(i)))->content()
				);
			}

			fprintf(fout,");\n\n");

			for ( bit=0 ; bit<=n ; bit++ )
			{
				AVariable *s,*c;

				s=generateTempVariable(new ATypeBool());
				c=generateTempVariable(new ATypeBool());

				subAdd(
					BString::cast( p->getValueAt(new Integer(bit)) ),
					av2->getBitName(bit),
					cin->getName(),
					sa->getName(),
					c->getName(),
					cache->getBitName(bit) /* sum */
				);
				cin=c;
				p->addAssoc( new Integer(bit+step),BString::cast(s->getName()));
			}
			
		} /* "AVariable * ExMod::logic()-var/var" */

	} /* (NULL==asConstant()) */

	DEBUG_exit("AVariable * ExMod::logic()")
	return cache;   
}

