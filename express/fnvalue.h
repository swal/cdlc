/*
 * $Log$
 *
 */

/*$Id$*/

#ifndef FNVALUE_H
#define FNVALUE_H

#include "../express/express.h"
#include "../klassen/atype.h"

class FnValue : public Expression
{
	protected:
	    Expression * myEx;

	public:
		FnValue();
		FnValue( Object * exList );
		
		virtual AType     * getType();
		virtual AConstant * asConstant();
		virtual AVariable * logic();
		
	DEFAULT_CONCRETE_METHODS(FnValue,Expression);
};

#endif
