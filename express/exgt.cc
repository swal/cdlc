/*
 * $Log$
 *
 */

/* $Id$ */ 

#include "exgt.h"
#include "../main/debug.h"
#include "../klassen/atbool.h"
#include "../klassen/atenum.h"
#include "../klassen/atrange.h"
#include "../klassen/acbool.h"
#include "../library/bstring.h"
#include "../main/error.h"
#include "../grammar/parser.h"


#define SUPERCLASS Expression
INITIALIZE_CONCRETE_CLASS(ExGt, SUPERCLASS);


ExGt::ExGt()
{
    DEBUG_enter("ExGt::ExGt()")
    ex1=NULL;
    ex2=NULL;
    DEBUG_exit("ExGt::ExGt()")
}

ExGt::ExGt( Object * exleft, Object * exright )
{
    DEBUG_enter("ExGt::ExGt( Object * exleft, Object * exright )")
    
    if (!exleft ->isKindOf(Expression::ClassName))
    { error("internal error: one of ExGt's Subexs isn't an Expr"); }
    if (!exright->isKindOf(Expression::ClassName))
    { error("internal error: one of ExGt's Subexs isn't an Expr"); }
    if (!(  (Expression::cast(exleft)->getType()->isKindOf(ATypeEnum::ClassName))
          ||(Expression::cast(exleft)->getType()->isKindOf(ATypeRange::ClassName))
        ) )
    { semerror(exleft,"left argument of relational operator has wrong type"); }
    if (!(  (Expression::cast(exright)->getType()->isKindOf(ATypeEnum::ClassName))
          ||(Expression::cast(exright)->getType()->isKindOf(ATypeRange::ClassName))
        ) )
    { semerror(exright,"right argument of relational operator has wrong type"); }


    ex1=Expression::cast(exleft );
    ex2=Expression::cast(exright);
    DEBUG_exit("ExGt::ExGt( Object * exleft, Object * exright )")
}

AType * ExGt::getType()
{
    DEBUG_enter("AType * ExGt::getType()")
    DEBUG_exit("AType * ExGt::getType()")
    return new ATypeBool();
}


AConstant * ExGt::asConstant()
{
    AConstant * ac1, *ac2, *cache;
    DEBUG_enter("AConstant * ExGt::asConstant()")
  
    ac1=ex1->asConstant();
    ac2=ex2->asConstant();
    if ((NULL!=ac1) && (NULL!=ac2))
    {
        if (ac1->getType()->isKindOf(ATypeRange::ClassName))
        {
            cache= new AConBool(
                AConInt::cast(ac1)->value() > AConInt::cast(ac2)->value()
            );
        }
        else
        {
            /* AConEnum */
            cache= new AConBool(
                ATypeEnum::cast(ac1->getType())->positionOf(ac1) <
                ATypeEnum::cast(ac1->getType())->positionOf(ac2)
            );
        }
    }
    else
    {
        cache= NULL;
    }
    DEBUG_exit("AConstant * ExGt::asConstant()")
    return cache;
}

AVariable * ExGt::logic()
{
    AVariable * cache;

    DEBUG_enter("AVariable * ExGt::logic()")

    if (NULL!=asConstant())
    { 
        cache=new AVariable(new BString(""),getType());
        cache->setConstant(asConstant());
    }
    else
    {
        cache=generateTempVariable(getType());
        AVariable *v1,*v2;
        v1=ex1->logic();
        v2=ex2->logic();
        fprintf(fout,"; ExGt:\n*BOOLEAN-EQUATIONS\n %s = %s > %s ;\n\n",
            cache->getName()->content(),
            v1->getName()->content(),
            v2->getName()->content()
        );
    }
    DEBUG_exit("AVariable * ExGt::logic()")
    return cache;
}

