/*
 * $Log$
 *
 */

/* $Id$ */ 

#include "exone.h"

#include "../library/bstring.h"
#include "../library/list.h"
#include "../library/listobj.h"
#include "../main/error.h"
#include "../main/debug.h"
#include "../klassen/acint.h"
#include "../klassen/atenum.h"
#include "../grammar/parser.h"
#include "../express/exor.h"

#define SUPERCLASS Expression
INITIALIZE_CONCRETE_CLASS(ExOne, SUPERCLASS);

ExOne::ExOne()
{
    DEBUG_enter("ExOne::ExOne()")
    myList=NULL;
    myEx  =NULL;
    DEBUG_exit("ExOne::ExOne()")
}

ExOne::ExOne(Object *loopVarList,Object * ex)
{
    DEBUG_enter("ExOne::ExOne(List *loopVarList,Expression * ex)")
    if (!loopVarList->isKindOf(ALoopVarList::ClassName))
    { error("internal error: first argument of ExOne isn't a loop-var-list"); }
    if (!ex->isKindOf(Expression::ClassName))
    { error("internal error: second argument of ExOne isn't an expression"); }
    myList=ALoopVarList::cast(loopVarList);
    myEx  =Expression::cast(ex);
    DEBUG_exit("ExOne::ExOne(List *loopVarList,Expression * ex)")
}

AType * ExOne::getType()
{
    DEBUG_enter("AType * ExOne::getType()")
    DEBUG_exit("AType * ExOne::getType()")
    return new ATypeBool();
}

AConstant * ExOne::asConstant()
{
    /* no shortcut defined: to implement */
    return NULL;
}

AVariable * ExOne::logic()
{
    AVariable *result;
    List * resList;
    int i;

    DEBUG_enter("AVariable * ExOne::logic()")
    
    resList=new List();

    i=1;


    /* calculate all conditions */
    
    while ( 0!=myList->setToIteration(i) )
    {
        AVariable *local ;
        
        local=myEx->logic();
        resList->add(new ListObject(local));
        i++;
    }


    /* 'or' all conditions */
    
    result=generateTempVariable(new ATypeBool());

    fprintf(fout,"; or all ExOne-conditions:\n*BOOLEAN-EQUATIONS\n %s = (",
        result->getName()->content()
    );

    for( Iterator j(resList) ; j ; j++ )
    {
        if ( j!=resList->first() ) { fprintf(fout,"+"); }
        fprintf(fout," %s ",
            AVariable::cast(ListObject::cast(j)->getObject())->getName()->content()
        );
    }
    fprintf(fout,") ;\n\n");


    /* set loop vars (side effect) */

    myList->assignResults(resList);

    DEBUG_exit("AVariable * ExOne::logic()")

    return result;
}

