/*
 * $Log$
 *
 */

/*$Id$*/

#ifndef EXPRESSION_IN_H
#define EXPRESSION_IN_H

#include "../express/express.h"
#include "../klassen/agroup.h"
#include "../library/bstring.h"
#include "../library/listobj.h"


class ExIn : public Expression
{
	private:
		Expression * ex;
		AGroup * gr;

	
	public:
		ExIn();
		ExIn( Object * expression, Object * group );
		virtual AType * getType();
		virtual AConstant * asConstant();       
		virtual AVariable * logic();

	DEFAULT_CONCRETE_METHODS(ExIn,Expression);
};

#endif
