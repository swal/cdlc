/*
 * $Log$
 *
 */

/* $Id$ */ 

#include "exnot.h"
#include "../main/debug.h"
#include "../klassen/atbool.h"
#include "../klassen/acbool.h"
#include "../library/bstring.h"
#include "../main/error.h"
#include "../grammar/parser.h"


#define SUPERCLASS Expression
INITIALIZE_CONCRETE_CLASS(ExNot, SUPERCLASS);


ExNot::ExNot()
{
    DEBUG_enter("ExNot::ExNot()")
    myEx=NULL;
    DEBUG_exit("ExNot::ExNot()")
}


ExNot::ExNot( Object * expression )
{
    DEBUG_enter("ExNot::ExNot( Object * expression )")
    
    if (!expression->isKindOf(Expression::ClassName) )
    { error("internal error: one of ExNot's Subexs isn't an Expr"); }
    if (!Expression::cast(expression)->getType()->isKindOf(ATypeBool::ClassName))
    { semerror(NULL,"type conflict: argument of 'not' isn't bool"); }
  
    myEx=Expression::cast(expression );
    DEBUG_exit("ExNot::ExNot( Object * expression )")
}

AType * ExNot::getType()
{
    AType * myType;
    DEBUG_enter("AType * ExNot::getType()")
    myType= new ATypeBool();
    DEBUG_exit("AType * ExNot::getType()")
    return myType;
}

AConstant * ExNot::asConstant()
{
    AConstant * ac;
    AConstant * cache;
    
    DEBUG_enter("AConstant * ExNot::asConstant()")
    
    
        ac=myEx->asConstant();
        if (NULL!=ac)
        {
            cache=new AConBool( ! AConBool::cast(ac)->value() );
        }
        else
        {
            cache=NULL;
        }
    
    DEBUG_exit("AConstant * ExNot::asConstant()")
    return cache;
}

AVariable * ExNot::logic()
{
    AVariable *vResult;
    AVariable *vex;

    DEBUG_enter("AVariable * ExNot::logic()")

        vex=myEx->logic();

        if (NULL!=asConstant())
        {
            vResult=new AVariable(new BString(""),getType());
        }
        else
        {
            vResult=generateTempVariable(getType());

            fprintf(fout,"; not-expression:\n*BOOLEAN-EQUATIONS\n %s = / %s ;\n\n",
                vResult ->getName()->content(),
                vex     ->getName()->content()
            );
        }

        vResult->setConstant(asConstant());

    DEBUG_exit("AVariable * ExNot::logic()")

    return vResult;
}

