/*
 * $Log$
 *
 */

/* $Id$ */ 


#include "exminus.h"
#include "../main/debug.h"
#include "../main/error.h"  
#include "../klassen/atenum.h"
#include "../klassen/atrange.h"
#include "../klassen/atbool.h"
#include "../klassen/acint.h"
#include "../klassen/acbool.h"
#include "../library/bstring.h"
#include "../library/integer.h"
#include "../grammar/parser.h"
#include "../express/exadd.h"
#include "../statmnt/stassign.h"


#define SUPERCLASS Expression
INITIALIZE_CONCRETE_CLASS(ExMinus, SUPERCLASS);


ExMinus::ExMinus()
{
    DEBUG_enter("ExMinus::ExMinus()")
    ex=NULL;
    DEBUG_exit("ExMinus::ExMinus()")
}


ExMinus::ExMinus( Object * expression )
{
    DEBUG_enter("ExMinus::ExMinus( Object * expression )")
  
    if (NULL==expression) 
    { error("internal error: ExMinus::ExMinus operand is NULL pointer"); }
    if (!expression->isKindOf(Expression::ClassName)) 
    { error("internal error: ExMinus's Subexs isn't an Expr"); }
    if (!Expression::cast(expression)->getType()->isKindOf(ATypeRange::ClassName))
    { semerror(NULL,"type conflict: operand for minus isn't a number"); }

    ex=Expression::cast(expression);
    DEBUG_exit("ExMinus::ExMinus( Object * expression )")
}


AType * ExMinus::getType()
{
    AType *cache;
    int upper,lower;

    DEBUG_enter("AType * ExMinus::getType()")
    upper=ATypeRange::cast(ex->getType())->getUpper()->value();
    lower=ATypeRange::cast(ex->getType())->getLower()->value();

    cache=new ATypeRange(new Integer(-upper),new Integer(-lower));

    DEBUG_exit("AType * ExMinus::getType()")
    return cache;
}


AConstant * ExMinus::asConstant()
{
    AConstant * ac, *cache;

    DEBUG_enter("AConstant * ExMinus::asConstant()")
    ac=ex->asConstant();

    if (NULL!=ac)
    {
        cache=new AConInt( - AConInt::cast(ac)->value() );
    }
    else
    {
        cache=NULL;
    }
    DEBUG_exit("AConstant * ExMinus::asConstant()")
    return cache;
}


AVariable * ExMinus::logic()
{
    AVariable * cache;
    DEBUG_enter("AVariable * ExMinus::logic()")

    if (NULL!=asConstant())
    {
        cache=new AVariable(new BString(""),getType());
        cache->setConstant(asConstant());
    }
    else
    {
        AType *zt;
        AVariable *log, *v, *vsum;

        if ( ATypeRange::cast(ex->getType())->getLower()->value() < 0 )
        {
            zt=ex->getType();
        }
        else
        {
            zt=new ATypeRange(
                new Integer(
                    -(ATypeRange::cast(ex->getType())->getUpper()->value())
                ),
                ATypeRange::cast(ex->getType())->getUpper()
            );
        }
        log=ex->logic();
        v=generateTempVariable(zt);
        fprintf(fout,"; unary minus:\n*BOOLEAN-EQUATIONS\n");
        for (
            int i=0;
            i<ATypeRange::cast(ex->getType())->getNumberOfBits();
            i++
        )
        {
            fprintf(fout," %s = / %s ;\n",
                v->getBitName(i)->content(),
                log->getBitName(i)->content()
            );
        }

        if (
            ATypeRange::cast(ex->getType())->getNumberOfBits() <
            ATypeRange::cast(zt)->getNumberOfBits()
        )
        {
            fprintf(fout," %s = 1 ;\n",
                v->getBitName(
                    ATypeRange::cast(zt)->getNumberOfBits()-1
                )->content()
            );
        }
        fprintf(fout,"\n");

        vsum=(new ExAdd(v,new AConInt(1)))->logic();

        cache=generateTempVariable(getType());

        fprintf(fout,"*BOOLEAN-EQUATIONS\n %s = %s ;\n\n",
            cache->getName()->content(),
            vsum->getName(getType())->content()
        );
    }

    DEBUG_exit("AVariable * ExMinus::logic()")

    return cache;
}

