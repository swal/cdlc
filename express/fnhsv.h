/*
 * $Log$
 *
 */

/*$Id$*/

#ifndef FNHSV_H
#define FNHSV_H

#include "../express/express.h"
#include "../klassen/atype.h"

class FnHsv : public Expression
{
	protected:
		Expression *myH, *myS, *myV;

	public:
		FnHsv();
		FnHsv( Object * exList );
		
		virtual AType     * getType();
		virtual AConstant * asConstant();
		virtual AVariable * logic();
		
	DEFAULT_CONCRETE_METHODS(FnHsv,Expression);
};

#endif
