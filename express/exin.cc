/*
 * $Log$
 *
 */

/* $Id$ */ 

#include "exin.h"
#include "../main/debug.h"
#include "../klassen/atbool.h"
#include "../klassen/acbool.h"
#include "../library/bstring.h"
#include "../main/error.h"
#include "../grammar/parser.h"

#define SUPERCLASS Expression
INITIALIZE_CONCRETE_CLASS(ExIn, SUPERCLASS);


ExIn::ExIn()
{ 
    DEBUG_enter("ExIn::ExIn()")
    ex=NULL;
    gr=NULL;  
    DEBUG_exit("ExIn::ExIn()")
}

ExIn::ExIn( Object * expression, Object * group )
{
    DEBUG_enter("ExIn::ExIn( Object * expression, Object * group )")
    if (!expression ->isKindOf(Expression::ClassName)) 
    { error("internal error: expression argument of 'in' has wrong type"); }
    if (!group->isKindOf(AGroup::ClassName))
    { error("internal error: group argument of 'in' has wrong type"); }
    ex=Expression::cast(expression );
    gr=AGroup::cast(group);
    DEBUG_exit("ExIn::ExIn( Object * expression, Object * group )")    
}

AType * ExIn::getType()
{
    DEBUG_enter("AType * ExIn::getType()")
    DEBUG_exit("AType * ExIn::getType()")
    return new ATypeBool();
}

AConstant * ExIn::asConstant()
{
    AConstant * ac, *cache;
    int tmp;
    
    DEBUG_enter("AConstant * ExIn::asConstant()")
    ac=ex->asConstant();
    if ((NULL!=ac) && (NULL!=gr))
    {
        tmp=0;
        for ( Iterator i(gr) ; i ; i++ )
        {
            if ( AConBool::cast( 
                   (*ac) == (ListObject::cast(i)->getObject()) 
                   )->value() ) 
              { tmp=1; }
        }
        cache= new AConBool(tmp);
    }
    else
    {
        cache= NULL;
    }
    DEBUG_exit("AConstant * ExIn::asConstant()")
    return cache;
}

AVariable * ExIn::logic()
{
    AVariable * vex;
    AVariable * result;
    int first;
    
    List * namelist;

    DEBUG_enter("AVariable * ExIn::logic()")

    namelist=new List();

    for(Iterator i(gr); i ; i++)
    {
        namelist->add(new ListObject(
            AConstant::cast(ListObject::cast(i)->getObject())->logic()
        ));
    }

    vex=ex->logic(); /* eine evtl. komplizierte variable, in der der Wert steht */

    result=generateTempVariable(new ATypeBool());
    fprintf(fout,"; in-expression\n");
    fprintf(fout,"*BOOLEAN-EQUATIONS\n %s = \n\t  ",result->getName()->content() );

    first=1;

    for (Iterator j(namelist); j ; j++)
    {
        // schade, dass LOG/iC eine Zeilenlaengenbegrenzung hat!
        if (!first) { fprintf(fout,"\n\t+ "); } 
        first=0;
        fprintf(fout,"(/(+(%s#%s)))",
            vex->getName()->content(),
            AVariable::cast( ListObject::cast(j)->getObject() )
                ->getName(vex->getType())
                ->content()
        );
    }

    fprintf(fout," ;\n\n");
    fprintf(fout,"; %s \n",BString::cast(vex->getType()->className())->content());

    result->setConstant(asConstant());

    DEBUG_exit("AVariable * ExIn::logic()")
    return result;
}

