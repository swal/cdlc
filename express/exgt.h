/*
 * $Log$
 *
 */

/*$Id$*/

#ifndef EXPRESSION_GREATERTHAN_H
#define EXPRESSION_GREATERTHAN_H

#include "../express/express.h"

class ExGt : public Expression
{
	private:
		Expression * ex1, * ex2;
	
	public:
		ExGt();
		ExGt( Object * exleft, Object * exright );

		virtual AType     * getType();
		virtual AConstant * asConstant();
		virtual AVariable * logic();
		
	DEFAULT_CONCRETE_METHODS(ExGt,Expression);
};

#endif
