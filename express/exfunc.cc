/*
 * $Log$
 *
 */

/* $Id$ */ 

#include "exfunc.h"
#include "../klassen/atenum.h"
#include "../library/bstring.h"
#include "../library/list.h"
#include "../library/listobj.h"
#include "../library/iterator.h"
#include "../main/error.h"
#include "../main/debug.h"
#include "../klassen/acint.h"
#include "../grammar/parser.h"

// add header file of function here:

#include "../express/fnrandom.h"
#include "../express/fnvalue.h"
#include "../express/fnhsv.h"
#include "../express/fnnext.h"
#include "../express/fnprev.h"
#include "../express/fnabs.h"



#define SUPERCLASS Expression
INITIALIZE_CONCRETE_CLASS(ExFunction, SUPERCLASS);


ExFunction::ExFunction()
{
    DEBUG_enter("ExFunction::ExFunction()")
    myFunc=NULL;
    DEBUG_exit("ExFunction::ExFunction()")
}


// add constructor of function here:

ExFunction::ExFunction( Object * name, Object * exList )
{
    DEBUG_enter("ExFunction::ExFunction( Object * name, Object * exList )")

    if (! name->isKindOf(BString::ClassName))
    { error("internal error: function name isn't a BString"); }

    if (! exList->isKindOf(List::ClassName))
    { error("internal error: function argument isn't a list"); }

    for (Iterator i(List::cast(exList));i;i++)
    {
        if ( !ListObject::cast(i)->getObject()->isKindOf(Expression::ClassName) )
        { error("internal error: one function argument isn't an expression"); }
    }    


    if ( BString::cast(name)->equal(new BString("random")) )
    { myFunc=new FnRandom(exList); return; }
    
    if ( BString::cast(name)->equal(new BString("value")) )
    { myFunc=new FnValue(exList); return; }

    if ( BString::cast(name)->equal(new BString("hsv")) )
    { myFunc=new FnHsv(exList); return; }
    
    if ( BString::cast(name)->equal(new BString("next")) )
    { myFunc=new FnNext(exList); return; }
        
    if ( BString::cast(name)->equal(new BString("prev")) )
    { myFunc=new FnPrev(exList); return; }

    if ( BString::cast(name)->equal(new BString("abs")) )
    { myFunc=new FnAbs(exList); return; }
        
    
    semerror(name,"function not defined"); 
    DEBUG_exit("ExFunction::ExFunction( Object * name, Object * exList )")
}

AType * ExFunction::getType()
{
    AType * at;
    DEBUG_enter("AType * ExFunction::getType()")
    at=myFunc->getType();
    DEBUG_exit("AType * ExFunction::getType()")
    return at;
}

AConstant * ExFunction::asConstant()
{
    AConstant *ac;
    DEBUG_enter("AConstant * ExFunction::asConstant()")
    ac=myFunc->asConstant();
    DEBUG_exit("AConstant * ExFunction::asConstant()")
    return ac;
}

AVariable * ExFunction::logic()
{
    AVariable *av;
    DEBUG_enter("AVariable * ExFunction::logic()")
    av=myFunc->logic();
    DEBUG_exit("AVariable * ExFunction::logic()")
    return av;
}

