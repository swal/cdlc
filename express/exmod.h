/*
 * $Log$
 *
 */

/*$Id$*/

#ifndef EXPRESSION_MOD_H
#define EXPRESSION_MOD_H

#include "../express/express.h"
#include "../klassen/atype.h"
#include "../klassen/aconstnt.h"

class ExMod : public Expression
{
	private:
		Expression * ex1, * ex2;

	public:
		ExMod();
		ExMod( Object * exleft, Object * exright );
		virtual AType * getType();
		virtual AConstant * asConstant();
		virtual AVariable * logic();
		
	DEFAULT_CONCRETE_METHODS(ExMod,Expression);
};

#endif
