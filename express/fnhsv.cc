#include "fnhsv.h"
#include "../library/integer.h"
#include "../klassen/avariabl.h"
#include "../klassen/atrange.h"
#include "../klassen/atrecord.h"
#include "../klassen/atenum.h"
#include "../klassen/atbool.h"
#include "../statmnt/stassign.h"
#include "../main/main.h"
#include "../main/logic.h"
#include "../express/exrecord.h"


#define SUPERCLASS Expression

INITIALIZE_CONCRETE_CLASS(FnHsv, SUPERCLASS);


FnHsv::FnHsv()
{
    DEBUG_enter("FnHsv::FnHsv()")
    DEBUG_exit("FnHsv::FnHsv()")
}

FnHsv::FnHsv(Object * exList)
{
    Expression * ex;
    ATypeRecord * at;
    DEBUG_enter("FnHsv::FnHsv(Object * exList)")
    
    if ( 1!=List::cast(exList)->getSize() )
    { error("wrong number of parameters for hsv-function"); }
    
    ex=Expression::cast(ListObject::cast(List::cast(exList)->at(1))->getObject());
    if ( !ex->getType()->isKindOf(ATypeRecord::ClassName) )
    { error("argument of hsv() isn't of type record"); }

    at=ATypeRecord::cast(ex->getType());

    for (int i=1; i<=3; i++)
    {
        if (! at->getTypeAt(i)->isKindOf(ATypeRange::ClassName))
        { error("sub-record argument of hsv() isn't of type int"); }
    }

    if (!ex->isKindOf(ExRecord::ClassName))
    { error("argument of hsv() isn't a record expression"); }
    
    myH=ExRecord::cast(ex)->getExpressionAt(1);
    myS=ExRecord::cast(ex)->getExpressionAt(2);
    myV=ExRecord::cast(ex)->getExpressionAt(3);
    
    DEBUG_exit("FnHsv::FnHsv(Object * exList)")
}

AType * FnHsv::getType()
{
    AType *at, *atr;
    List * l;

    DEBUG_enter("AType * FnHsv::getType()")

    l=new List();
    atr=new ATypeRange( new Integer(0), new Integer(255) );
    l->add(new ListObject(new Pair(new BString("r"),atr)));
    l->add(new ListObject(new Pair(new BString("g"),atr)));
    l->add(new ListObject(new Pair(new BString("b"),atr)));

    at=new ATypeRecord(l);
    DEBUG_exit("AType * FnHsv::getType()")
    return at;
}
    
AConstant * FnHsv::asConstant()
{
    AConstant * c;
    List *l;

    DEBUG_enter("AConstant * FnHsv::asConstant()")
    if ((NULL==myH->asConstant()) ||
        (NULL==myS->asConstant()) ||
        (NULL==myV->asConstant()) )
    {
        c=NULL;
    }
    else
    {
        float h,s,v;
        float r,g,b;
        int i;
        float f,p,q,t;

        h=AConInt::cast(myH->asConstant())->value();
        s=AConInt::cast(myS->asConstant())->value();
        v=AConInt::cast(myV->asConstant())->value();

        h=h*360/256;
        s=s/255;
        v=v/255;

        /* Algorithm taken from:
         * Foley, van Dam: Computer Graphics. p.593
         */

        if (0==s)
        {
            r=v; g=v; b=v;
        }
        else
        {
            h=h/60;
            i=(int) h;
            f=h-i;
            p=v*(1-s);
            q=v*(1-(s*f));
            t=v*(1-(s*(1-f)));
            switch (i)
            {
                case 0: r=v; g=t; b=p; break;
                case 1: r=q; g=v; b=p; break;
                case 2: r=p; g=v; b=t; break;
                case 3: r=p; g=q; b=v; break;
                case 4: r=t; g=p; b=v; break;
                case 5: r=v; g=p; b=q; 
            }
        }
        r=r*255;
        g=g*255;
        b=b*255;

        l=new List();

        l->add(new ListObject(new AConInt((int)r)));
        l->add(new ListObject(new AConInt((int)g)));
        l->add(new ListObject(new AConInt((int)b)));

        c=new AConRecord(l);
    }
    
    DEBUG_exit("AConstant * FnHsv::asConstant()")
    return c;
}

AVariable * FnHsv::logic()
{
    AVariable * v;

    DEBUG_enter("AVariable * FnHsv::logic()")
    
    if (NULL!=asConstant())
    { 
        v=new AVariable(new BString(""),getType()); 
        v->setConstant(asConstant());    
    }
    else
    {
        semerror(NULL,"hsv()-function should have constant arguments");
        v=NULL;
    }
 
    DEBUG_exit("AVariable * FnHsv::logic()")
    return v;
}
