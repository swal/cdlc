/*
 * $Log$
 *
 */

/* $Id$ */ 

#include "exborder.h"
#include "../main/debug.h"
#include "../main/error.h"
#include "../klassen/atrecord.h"
#include "../klassen/atrange.h"
#include "../klassen/acrecord.h"
#include "../library/list.h"
#include "../library/listobj.h"
#include "../library/pair.h"
#include "../library/bstring.h"
#include "../library/integer.h"
#include "../grammar/parser.h"

#define SUPERCLASS Expression
INITIALIZE_CONCRETE_CLASS(ExBorder, SUPERCLASS);


ExBorder::ExBorder()
{
    DEBUG_enter("ExBorder::ExBorder()")
    DEBUG_exit("ExBorder::ExBorder()")
}

AType * ExBorder::getType()
{
    AType *at;
    List * l;
    
    DEBUG_enter("AType * ExBorder::getType()")
    l=new List();
    l->add(new ListObject(new Pair(new BString("1"),new ATypeRange(new Integer(-1),new Integer(1)))));
    l->add(new ListObject(new Pair(new BString("2"),new ATypeRange(new Integer(-1),new Integer(1)))));
    at=new ATypeRecord(l);
    DEBUG_exit("AType * ExBorder::getType()")
    return at;
}

AConstant * ExBorder::asConstant()
{
    DEBUG_enter("AConstant * ExBorder::asConstant()")
    DEBUG_exit("AConstant * ExBorder::asConstant()")
    return NULL;
}

AVariable * ExBorder::logic()
{
    AVariable *v;

    DEBUG_enter("AVariable * ExBorder::logic()")

    /* neue Variable generieren und als *LOCAL bekannt machen */

    v=generateTempVariable(getType());

    fprintf(fout,"; border-expression:\n*FUNCTION-TABLE\n$HEADER :\n");
    fprintf(fout," X [PRR,PRL] : Y %s ;\n",v->getSubName(new BString("1"))->getName()->content());
    fprintf(fout," X   1   0   : Y 01 ;\n");
    fprintf(fout," X   0   1   : Y 11 ;\n");
    fprintf(fout," X   0   0   : Y 00 ;\n");
    fprintf(fout," X   $REST   : Y -- ;\n");
    
    fprintf(fout,"\n$HEADER :\n");
    fprintf(fout," X [PRO,PRU] : Y %s ;\n",v->getSubName(new BString("2"))->getName()->content());
    fprintf(fout," X   1   0   : Y 01 ;\n");
    fprintf(fout," X   0   1   : Y 11 ;\n");
    fprintf(fout," X   0   0   : Y 00 ;\n");
    fprintf(fout," X   $REST   : Y -- ;\n");


    DEBUG_exit("AVariable * ExBorder::logic()")

    return v;
}

