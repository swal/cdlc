/*
 * $Log$
 *
 */

/* $Id$ */ 

#include "excast.h"
#include "../main/debug.h"
#include "../klassen/atunion.h"
#include "../klassen/acunion.h"
#include "../library/bstring.h"
#include "../main/error.h"
#include "../grammar/parser.h"


#define SUPERCLASS Expression
INITIALIZE_CONCRETE_CLASS(ExCast, SUPERCLASS);


ExCast::ExCast()
{
    DEBUG_enter("ExCast::ExCast()")
    ex=NULL;
    subtype=NULL;
    DEBUG_exit("ExCast::ExCast()")
}

ExCast::ExCast( Object * subt, Object * expression )
{
    DEBUG_enter("ExCast::ExCast( Object * subt, Object * expression )")
    if (!subt ->isKindOf(BString::ClassName)) 
    { error("internal error: one of ExCast's Subexs isn't an Name"); }
    if (!expression->isKindOf(Expression::ClassName))
    { error("internal error: one of ExCast's Subexs isn't an Expr"); }
    subtype=BString::cast(subt);
    ex=Expression::cast(expression);
    DEBUG_exit("ExCast::ExCast( Object * subt, Object * expression )")
}

AType * ExCast::getType()
{
    DEBUG_enter("AType * ExCast::getType()")
    DEBUG_exit("AType * ExCast::getType()")
    return ex->getType();
}

AConstant * ExCast::asConstant()
{
    AConstant * ac, *cache;
    DEBUG_enter("AConstant * ExCast::asConstant()")
    ac=ex->asConstant();
    if (NULL!=ac)
    {
        cache= new AConUnion(subtype,ac);
    }
    else
    {
        cache= NULL;
    }
    DEBUG_exit("AConstant * ExCast::asConstant()")
    return cache;
}

AVariable * ExCast::logic()
{
    AVariable * v;
    DEBUG_enter("AVariable * ExCast::logic()")

    v=ex->logic();
    
    DEBUG_exit("AVariable * ExCast::logic()")
    return v;
}

