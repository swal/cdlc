/*
 * $Log$
 *
 */

/* $Id$ */ 

#include "exmult.h"
#include "../main/debug.h"
#include "../main/error.h"
#include "../klassen/atenum.h"
#include "../klassen/atbool.h"
#include "../klassen/acint.h"
#include "../klassen/avariabl.h"
#include "../library/bstring.h"
#include "../library/pair.h"
#include "../grammar/parser.h"
#include "../express/exadd.h"
#include "../statmnt/stassign.h"


#define SUPERCLASS Expression
INITIALIZE_CONCRETE_CLASS(ExMult, SUPERCLASS);


ExMult::ExMult()
{
	DEBUG_enter("ExMult::ExMult()")
	ex1=NULL;
	ex2=NULL;
	DEBUG_enter("ExMult::ExMult()")
}

ExMult::ExMult( Object * exleft, Object * exright )
{
	DEBUG_enter("ExMult::ExMult( Object * exleft, Object * exright )")
	if (!exleft ->isKindOf(Expression::ClassName))
	{ error("internal error: one of ExMult's Subexs isn't an Expr"); }
	if (!exright->isKindOf(Expression::ClassName))
	{ error("internal error: one of ExMult's Subexs isn't an Expr"); }
	if (!Expression::cast(exleft)->getType()->isKindOf(ATypeRange::ClassName))
	{ semerror(NULL,"type conflict: left side of multiplication isn't int"); }
	if (!Expression::cast(exright)->getType()->isKindOf(ATypeRange::ClassName))
	{ semerror(NULL,"type conflict: right side of multiplication isn't int"); }
  
	ex1=Expression::cast(exleft );
	ex2=Expression::cast(exright);
	DEBUG_exit("ExMult::ExMult( Object * exleft, Object * exright )")
}

AType * ExMult::getType()
{
	AType * t;
	int l1,u1,l2,u2;  /* lower und upper der beiden Argumente */
	int max,min;
	
	DEBUG_enter("AType * ExMult::getType()")
	l1=ATypeRange::cast(ex1->getType())->getLower()->value();
	u1=ATypeRange::cast(ex1->getType())->getUpper()->value();
	l2=ATypeRange::cast(ex2->getType())->getLower()->value();
	u2=ATypeRange::cast(ex2->getType())->getUpper()->value();

	max=l1*l2;
	min=max;
	if (l1*u2>max) {max=l1*u2; }
	if (l1*u2<min) {min=l1*u2; }
	if (u1*l2>max) {max=u1*l2; }
	if (u1*l2<min) {min=u1*l2; }
	if (u1*u2>max) {max=u1*u2; }
	if (u1*u2<min) {min=u1*u2; }
	
	t=new ATypeRange(new Integer(min),new Integer(max));

	DEBUG_exit("AType * ExMult::getType()")
	return t;
}

AConstant * ExMult::asConstant()
{
	AConstant * ac1, *ac2, *cache;

	DEBUG_enter("AConstant * ExMult::asConstant()")

	ac1=ex1->asConstant();
	ac2=ex2->asConstant();

	if (NULL!=ac1)
	{
		if (0==AConInt::cast(ac1)->value())
		{ 
			cache=new AConInt(0);
			DEBUG_exit("AConstant * ExMult::asConstant()")      
			return cache;
		}
	}

	if (NULL!=ac2)
	{
		if (0==AConInt::cast(ac2)->value())
		{ 
			cache=new AConInt(0);
			DEBUG_exit("AConstant * ExMult::asConstant()")      
			return cache;
		}
	}

	if ((NULL!=ac1) && (NULL!=ac2))
	{
		cache= new AConInt( AConInt::cast(ac1)->value() * AConInt::cast(ac2)->value() );
	}
	else
	{
		cache= NULL;
	}
	DEBUG_exit("AConstant * ExMult::asConstant()")      
	return cache;
}


AVariable * ExMult::logic()
{
	AVariable * cache;

	DEBUG_enter("AVariable * ExMult::logic()")
	
	if (NULL!=asConstant())
	{
		cache=new AVariable(new BString(""),getType());
		cache->setConstant(asConstant());
	}
	else
	{
		AConstant *ac;
		AVariable *av;
		AVariable *av2;

		ac=ex1->asConstant();


		if (NULL!=ac)
		{
			av=ex2->logic();
		}
		else
		{
			ac=ex2->asConstant();
			if (NULL!=ac)
			{
				av=ex1->logic();
			}
			else
			{
				av=ex1->logic();
				av2=ex2->logic(); 
			}
		}

//      cache=generateTempVariable(getType());

		if (NULL!=ac)
		{
			/*  a variable av ,and a constant ac != 0  */

			int c;
			int stellen;
			AVariable * zwischen=NULL;
			AVariable * geschoben;

			c=AConInt::cast(ac)->value();
			stellen=0;

			while (0!=c)                 /*  over all bits in c  */
			{
				if ( 0!=(c & 1) )          /*  if bit=1  */
				{
					/*  generate shifted var  */
					if (stellen>0)
					{
						geschoben=generateTempVariable(
							new ATypeRange(
								new Integer(ATypeRange::cast(av->getType())->getLower()->value()<<stellen),
								new Integer(ATypeRange::cast(av->getType())->getUpper()->value()<<stellen)
							)
						);

						fprintf(fout,"; mult:\n*BOOLEAN-EQUATIONS\n %s[%u..%u] = %s ;\n",
							geschoben->getBaseName()->content(),
							ATypeRange::cast(geschoben->getType())->getNumberOfBits()-1,
							stellen,
							av->getName()->content()
						);
						fprintf(fout," %s[%u..0] = 0 ;\n\n",
							geschoben->getBaseName()->content(),
							stellen-1
						);
					}
					else
					{ geschoben=av; }

					/*  add shifted var  */
					if (NULL==zwischen)
					{ zwischen=geschoben; }
					else
					{ zwischen=(new ExAdd(zwischen,geschoben))->logic(); }
				}

				c= c >> 1;
				stellen++;
			}
			cache=zwischen;
		}
		else
		{
			/*  two variables in av and av2  */

			int stellen;
			AVariable * zwischen;
			AVariable * geschoben;
			AVariable * vsum;


			stellen=0;
			zwischen=generateTempVariable(new ATypeRange(new Integer(0),new Integer(0)));
			zwischen->setConstant(new AConInt(0));

			while ( stellen < av2->getType()->getNumberOfBits() )   /* over all bits in av2 */
			{

				/* generate shifted var */
				if (stellen>0)
				{
					geschoben=generateTempVariable(
						new ATypeRange(
							new Integer(ATypeRange::cast(av->getType())->getLower()->value()<<stellen),
							new Integer(ATypeRange::cast(av->getType())->getUpper()->value()<<stellen)
						)
					);

					fprintf(fout,"; mult:\n*BOOLEAN-EQUATIONS\n %s[%u..%u] = %s ;\n",
						geschoben->getBaseName()->content(),
						ATypeRange::cast(geschoben->getType())->getNumberOfBits()-1,
						stellen,
						av->getName()->content()
					);
					fprintf(fout," %s[%u..0] = 0 ;\n\n",
						geschoben->getBaseName()->content(),
						stellen-1
					);
				}
				else
				{ geschoben=av; }

				/* add shifted var */

				vsum=(new ExAdd(zwischen,geschoben))->logic();
				
				{
					BString * ls;
					
					ls=zwischen->getName();
					zwischen->newInstance();

					fprintf(fout,"; assignment\n*BOOLEAN_EQUATIONS\n %s =\n\t  (  %s%u & (%s) )\n\t+ ( /%s%u & (%s) );\n\n",
						zwischen->getName()->content(),
						av2     ->getBaseName()->content(),
						stellen,
						vsum    ->getName( zwischen->getType() )->content(),
						av2     ->getBaseName()->content(),
						stellen,
						ls      ->content()
					 );
				}

				stellen++;
			}
			cache=zwischen;
		}
	}

	DEBUG_exit("AVariable * ExMult::logic()")
	return cache;
}

