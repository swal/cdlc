/*
 * $Log$
 *
 */

/*$Id$*/

#ifndef EXPRESSION_EQUAL_H
#define EXPRESSION_EQUAL_H

#include "../express/express.h"
#include "../klassen/avariabl.h"
#include "../klassen/atbool.h"

class ExEq : public Expression
{
	private:
		Expression * ex1, * ex2;

	public:
		ExEq();
		ExEq( Object * exleft, Object * exright );
		virtual AType * getType();
		virtual AConstant * asConstant();
		virtual AVariable * logic();
		
	DEFAULT_CONCRETE_METHODS(ExEq,Expression);
};

#endif
