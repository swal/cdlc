/*
 * $Log$
 *
 */

/*$Id$*/

#ifndef EX_NUM_H
#define EX_NUM_H

#include "../express/express.h"
#include "../klassen/atype.h"
#include "../klassen/aloopvar.h"
#include "../klassen/avariabl.h"
#include "../klassen/agroup.h"


class ExNum : public Expression
{
	protected:
	    ALoopVarList * myList;
//		AVariable    * myVariable;
//		AGroup       * myGroup;
		Expression   * myEx;
		
	public:
		ExNum();
//		ExNum(Object*,Object*,Object*);
		ExNum(Object *loopVarList,Object * ex);
 
		virtual AType     * getType();
		virtual AConstant * asConstant();
		virtual AVariable * logic();
		
	DEFAULT_CONCRETE_METHODS(ExNum,Expression);
};

#endif
