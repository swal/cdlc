/*
 * $Log$
 *
 */

/* $Id$ */ 

#include "exadd.h"
#include "../library/bstring.h"
#include "../main/error.h"
#include "../klassen/atenum.h"
#include "../klassen/atbool.h"
#include "../klassen/acint.h"
#include "../grammar/parser.h"


#define SUPERCLASS Expression
INITIALIZE_CONCRETE_CLASS(ExAdd, SUPERCLASS);


ExAdd::ExAdd()
{
    DEBUG_enter("ExAdd::ExAdd()")
    ex1=NULL;
    ex2=NULL;
    DEBUG_exit("ExAdd::ExAdd()")
}

ExAdd::ExAdd( Object * exleft, Object * exright )
{
    DEBUG_enter("ExAdd::ExAdd( Object * exleft, Object * exright )")
    if 
    (
        (!exleft ->isKindOf(Expression::ClassName)) ||
        (!exright->isKindOf(Expression::ClassName))
    )
    { error("internal error: one of ExAdd's Subexs isn't an Expr"); }
    if (!Expression::cast(exleft)->getType()->isKindOf(ATypeRange::ClassName))
    { semerror(NULL,"type conflict: left side of addition isn't int"); }
    if (!Expression::cast(exright)->getType()->isKindOf(ATypeRange::ClassName))
    { semerror(NULL,"type conflict: right side of addition isn't int"); }
  
    ex1=Expression::cast(exleft );
    ex2=Expression::cast(exright);
    DEBUG_exit("ExAdd::ExAdd( Object * exleft, Object * exright )")
}

AType * ExAdd::getType()
{
    ATypeRange * atr;
    DEBUG_enter("AType * ExAdd::getType()")
    atr=new ATypeRange( new Integer(
        ATypeRange::cast(ex1->getType())->getLower()->value()+
        ATypeRange::cast(ex2->getType())->getLower()->value()
        ), new Integer(
        ATypeRange::cast(ex1->getType())->getUpper()->value()+
        ATypeRange::cast(ex2->getType())->getUpper()->value()
        )
    );
    
    DEBUG_exit("AType * ExAdd::getType()")
    return atr;
}

AConstant * ExAdd::asConstant()
{
    AConstant * ac1, * ac2, *cache;

    DEBUG_enter("AConstant * ExAdd::asConstant()")
    ac1=ex1->asConstant();
    ac2=ex2->asConstant();
    if ((NULL!=ac1) && (NULL!=ac2))
    {
        cache=new AConInt( AConInt::cast(ac1)->value() + AConInt::cast(ac2)->value() );
    }
    else
    {
        cache= NULL;
    }
    DEBUG_exit("AConstant * ExAdd::asConstant()")
    return cache;
}

AVariable * ExAdd::logic()
{
    AVariable *cache;
    AVariable *op1, *op2;  
    
    DEBUG_enter("AVariable * ExAdd::logic()")

    if (NULL!=asConstant())
    {
        cache=new AVariable(new BString(""),getType());
    }
    else
    {
        int num1,num2;
        
        op1=ex1->logic();
        op2=ex2->logic();
        
        num1=ATypeRange::cast(op1->getType())->getNumberOfBits();
        num2=ATypeRange::cast(op2->getType())->getNumberOfBits();
        
        cache=generateTempVariable(getType());
        fprintf(fout,"; add:\n*BOOLEAN-EQUATIONS\n");
        fprintf(fout," %s = %s # %s ;\n",
            cache->getBitName(0)->content(),
            op1->getBitName(0)->content(),
            op2->getBitName(0)->content()
        );
        if ((1==num1) && (1==num2))
        {
            fprintf(fout," %s = %s & %s ;\n",
                cache->getBitName(1)->content(),
                op1->getBitName(0)->content(),
                op2->getBitName(0)->content()
            );
        }
        else
        {
            AVariable *hv, *carry, *carryNew;
            int i,hi;

            carry=generateTempVariable(new ATypeBool());
            fprintf(fout,"*BOOLEAN-EQUATIONS\n");
            fprintf(fout," %s = %s & %s ;\n",
                carry->getName()->content(),
                op1->getBitName(0)->content(),
                op2->getBitName(0)->content()
            );

            /* lowest bit is added now */

            if (num1<num2)
            {
                hv=op1 ;op1=op2  ;op2=hv;
                hi=num1;num1=num2;num2=hi;
            }
            
            /* now num2 is not longer than num1.
               alloc num2 full addres now
            */


            for ( i=1 ; i<num2 ; i++ )
            {
                fprintf(fout,"*BOOLEAN-EQUATIONS\n");
                fprintf(fout," %s = %s # %s # %s ;\n",
                    cache->getBitName(i)->content(),
                    op1->getBitName(i)->content(),
                    op2->getBitName(i)->content(),
                    carry->getName()->content()
                );
                carryNew=generateTempVariable(new ATypeBool());
                fprintf(fout,"*BOOLEAN-EQUATIONS\n");
                fprintf(fout," %s = (%s&%s)+(%s&(%s+%s)) ;\n",
                    carryNew->getName()->content(),
                    op1->getBitName(i)->content(),
                    op2->getBitName(i)->content(),
                    carry->getName()->content(),
                    op1->getBitName(i)->content(),
                    op2->getBitName(i)->content()
                );
                carry=carryNew;
            }

            /* we need num1-num2 half adders now */

            if (ATypeRange::cast(op2->getType())->getLower()->value()>=0)
            {
                for ( i=num2; i<num1 ; i++ )
                {
                    fprintf(fout," %s = %s # %s ;\n",
                        cache->getBitName(i)->content(),
                        op1->getBitName(i)->content(),
                        carry->getName()->content()
                    );
                    carryNew=generateTempVariable(new ATypeBool());
                    fprintf(fout,"*BOOLEAN-EQUATIONS\n");
                    fprintf(fout," %s = %s & %s ;\n",
                        carryNew->getName()->content(),
                        op1->getBitName(i)->content(),
                        carry->getName()->content()
                    );
                    carry=carryNew;
                }
            }
            else
            {
                /* op2 may be negative, so we need sign extension */
                char sign[20];

                strcpy(
                    sign,
                    op2->getBitName(
                        ATypeRange::cast(op2->getType())
                        ->getNumberOfBits()-1
                    )->content()
                );
                
                for ( i=num2; i<num1 ; i++ )
                {
                    fprintf(fout,"*BOOLEAN-EQUATIONS\n");
                    fprintf(fout," %s = %s # %s # %s ;\n",
                        cache->getBitName(i)->content(), 
                        op1->getBitName(i)->content(), 
                        sign,
                        carry->getName()->content()
                    );
                    carryNew=generateTempVariable(new ATypeBool());
                    fprintf(fout,"*BOOLEAN-EQUATIONS\n");
                    fprintf(fout," %s = (%s&%s)+(%s&(%s+%s)) ;\n",
                        carryNew->getName()->content(),
                        op1->getBitName(i)->content(),
                        sign,
                        carry->getName()->content(),
                        op1->getBitName(i)->content(),
                        sign
                    );
                    carry=carryNew;   
                }
            }

            /* if necessary connect most significant carry now */

            if ( (num1+1)==(ATypeRange::cast(getType())->getNumberOfBits()) )
            {
                fprintf(fout," %s = %s ;\n",
                    cache->getBitName(num1)->content(), 
                    carry->getName()->content()
                );
            }
        }
        fprintf(fout,"\n");
    }

    cache->setConstant(asConstant());

    DEBUG_exit("AVariable * ExAdd::logic()")
    return cache;
}

