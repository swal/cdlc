/*
 * $Log$
 *
 */

/* $Id$ */ 

#include "../main/debug.h"
#include "../main/error.h" 
#include "exrecord.h"
#include "../klassen/atrecord.h"
#include "../klassen/acrecord.h"
#include "../grammar/parser.h"
#include "../library/listobj.h"
#include "../library/bstring.h" 
#include "../library/iterator.h"
#include "../library/list.h"
#include "../main/logic.h"


#define SUPERCLASS Expression
INITIALIZE_CONCRETE_CLASS(ExRecord, SUPERCLASS);


ExRecord::ExRecord()
{
	DEBUG_enter("ExRecord::ExRecord()")
	li=NULL;
	DEBUG_exit("ExRecord::ExRecord()")
}

ExRecord::ExRecord( Object * list )
{
	DEBUG_enter("ExRecord::ExRecord( Object * list )")

	/* 'list' must be a List of Expressions */

	if(NULL==list)
	{ semerror(NULL,"empty records are not allowed"); }
	
	if (!list->isKindOf(List::ClassName) )
	{ error("internal error: ExRecord's argument isn't a List"); }
  
	li=List::cast(list);
	DEBUG_exit("ExRecord::ExRecord( Object * list )")
}

AType * ExRecord::getType()
{
	List * l;
	char s[100];
	int pos;
	
	DEBUG_enter("AType * ExRecord::getType()")
	l=new List();
	pos=1;
	for(Iterator i(li);i;i++)
	{
		sprintf(s,"%u",pos); pos++;
		l->add(new ListObject(new Pair(
			new BString(s),
			Expression::cast(ListObject::cast(i)->getObject())->getType()
		)));
	}
	
	DEBUG_exit("AType * ExRecord::getType()")
	return new ATypeRecord(l);
}

Expression * ExRecord::getExpressionAt(int pos)
{
	Object * o;
	Expression * ex;
	DEBUG_enter("Expression * ExRecord::getExpressionAt(int pos)")
	DEBUG_int(pos)
	o=li->at(pos);
	if (NULL==o)
	{
		ex=NULL;
	}
	else
	{
		ex=Expression::cast(ListObject::cast(o)->getObject());
	}
	DEBUG_exit("Expression * ExRecord::getExpressionAt(int pos)")
	return ex;
}

AConstant * ExRecord::asConstant()
{
	AConstant * cache;
	AConstant * ac;
	Expression * ex;
	ListObject * listobj;
	List * lo;


	DEBUG_enter("ExRecord::asConstant")

	lo=new List();

	if (NULL==li) error("internal error: ExRecord not initialised");

	for ( Iterator i(li) ; i ; i++ )
	{
		DEBUG_msg("for-loop")
  
		listobj=ListObject::cast(i);
		ex     =Expression::cast(listobj->getObject());
	
		if (NULL==ex) error("internal error: ExRecord: NULL pointer in expression-list");
		
		DEBUG_str( BString::cast(ex->className())->content() )

		ac     =ex->asConstant();
		if (ac==NULL) 
		{
			cache= NULL;
			DEBUG_exit("ExRecord::asConstant (NULL)")
			return cache;
		}
		lo->add( new ListObject(ac) );
	}
	cache=new AConRecord(lo);

	DEBUG_exit("ExRecord::asConstant (AConstant)")

	return cache;
  
}

AVariable * ExRecord::logic() 
{
	List * exVarList; /* variable list for the evaluated expressions */
	AVariable * v;
	int first;
	
	DEBUG_enter("AVariable * ExRecord::logic()")

	if (NULL!=asConstant())
	{
		v=new AVariable(new BString(""),getType());
	}
	else
	{

		exVarList= new List();
	
		for (Iterator i(li);i;i++)
		{
			exVarList->add(new ListObject(
				Expression::cast(ListObject::cast(i)->getObject())->logic()
			));
		}

		v=generateTempVariable(getType());
/* neu: */

		fprintf(fout,"; ExRecord:\n*BOOLEAN-EQUATIONS\n");

		first=1;

		for (Iterator j(exVarList);j;j++)
		{
			fprintf(fout," %s = %s ;\n",
				v->getSubName(ATypeRecord::cast(getType())->getNameAt(first))->getName()->content(),
				AVariable::cast(ListObject::cast(j)->getObject())->getName()->content()
			);
			first++;
		}
		fprintf(fout,"\n");

/*
		fprintf(fout,"; ExRecord:\n*BOOLEAN-EQUATIONS\n %s =\n\t[\n",v->getName()->content());

		first=1;

		for (Iterator j(exVarList);j;j++)
		{
			if (!first)
			{ fprintf(fout,",");}
			first=0;
			fprintf(fout,"\t\t%s\n ", AVariable::cast(ListObject::cast(j)->getObject())->getName()->content() );
		}
		fprintf(fout,"\t] ; \n\n");
*/

	}
	
	v->setConstant(asConstant());
	
	DEBUG_exit("AVariable * ExRecord::logic()")
	return v;
}

