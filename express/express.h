/*
 * $Log$
 *
 */

/*$Id$*/

#ifndef EXPRESSION_H
#define EXPRESSION_H

class AConstant;
class AType;
class AVariable;

#include "../klassen/syntaxel.h"

class Expression : public SyntaxElement
{

	public:
		virtual AType     * getType() = 0;
		virtual AConstant * asConstant() = 0;
		virtual AVariable * logic()=0;

	DEFAULT_ABSTRACT_METHODS(Expression,SyntaxElement);
};

#include "../klassen/aconstnt.h"  
#include "../main/main.h"
#include "../main/logic.h"


#endif
