/*
 * $Log$
 *
 */

/*$Id$*/

#ifndef FNNEXT_H
#define FNNEXT_H

#include "../express/express.h"
#include "../klassen/atype.h"

class FnNext : public Expression
{
	protected:
		Expression * myEx;

	public:
		FnNext();
		FnNext( Object * exList );
		
		virtual AType     * getType();
		virtual AConstant * asConstant();
		virtual AVariable * logic();
		
	DEFAULT_CONCRETE_METHODS(FnNext,Expression);
};

#endif
