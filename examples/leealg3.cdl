cellular automaton leealgo;

#include "vneumann.def"
#include "stdcolor.def"

type    zustaende = (frei, belegt, knoten, warten1, warten2, start, suchwelle,
			 welle_oben, welle_rechts, welle_unten, welle_links,
			 weg_oben, weg_rechts, weg_unten, weg_links,
			 marke_voben, marke_vrechts, marke_vunten, marke_vlinks,
			 marke_noben, marke_nrechts, marke_nunten, marke_nlinks,
			 loeschen);
	celltype = record
			zustand : zustaende;
			nummer  : 0..7;
		   end;

group   welle = {welle_oben, welle_rechts, welle_unten, welle_links};
	weg = {weg_oben, weg_rechts, weg_unten, weg_links};
	Xweg = {weg_unten, weg_links, weg_oben, weg_rechts};
	marke_von = {marke_voben, marke_vrechts, marke_vunten, marke_vlinks};
	Xmarke_von = {marke_vunten, marke_vlinks, marke_voben, marke_vrechts};
	marke_nach = {marke_noben, marke_nrechts, marke_nunten, marke_nlinks};
	marke = {marke_von, marke_nach};

color   black ~ *cell.zustand = frei;
	white ~ *cell.zustand = belegt;
	green ~ *cell.zustand = knoten;
	orange2 ~ *cell.zustand = warten1;
	orange1 ~ *cell.zustand = warten2;
	magenta ~ *cell.zustand = start;
	yellow ~ *cell.zustand = suchwelle;
	grey ~ *cell.zustand in welle;
	red ~ *cell.zustand in weg;
	cyan2 ~ *cell.zustand in marke_von;
	cyan1 ~ *cell.zustand in marke_nach;
	blue ~ *cell.zustand = loeschen;

var n   : celladdress;
	w,m : zustaende;

rule
case *cell.zustand of
 frei:
  if one(n in neighbours : *n.zustand in {start,suchwelle})
  then *cell := [suchwelle,*n.nummer];
 suchwelle:
  if one(n in neighbours :
	 *n.zustand in {weg,loeschen} and *n.nummer = *cell.nummer)
  then *cell.zustand := loeschen
  else
   if one(n in neighbours & w in welle:
	  (*n.zustand in {knoten,weg,marke} and *n.nummer != *cell.nummer) or
	  (*n.zustand in welle))
   then *cell.zustand := w;
 welle:
  if *north.zustand = weg_unten or *east.zustand = weg_links or
	 *south.zustand = weg_oben or *west.zustand = weg_rechts
  then *cell.zustand := element(zustaende,value(*cell.zustand)+4)
  else
   if one(n in neighbours :
	  *n.zustand in {weg,loeschen} and *n.nummer = *cell.nummer)
   then *cell.zustand := loeschen;
 knoten:
  if *cell.nummer = 1
  then *cell.zustand := start
  else
   if one(n in neighbours :
	  *n.zustand = loeschen and *n.nummer = *cell.nummer-1)
   then *cell.zustand := warten1;
 warten1:
  if all(n in neighbours : not(*n.zustand in weg))
  then *cell.zustand := warten2;
 warten2:
  if all(n in neighbours : *n.zustand in {frei,belegt,knoten,marke_nach})
  then *cell.zustand := start;
 start:
  if one(n in neighbours & w in weg : *n.zustand in {knoten,welle})
  then *cell.zustand := w;
 weg:
  if one(n in neighbours & w in weg & m in marke_von :
	 *n.zustand in {warten1,marke_von} and *cell.zustand = w)
  then *cell := [m,*n.nummer];
 marke_von:
  if all(n in neighbours & w in Xweg & m in Xmarke_von :
	 *n.zustand != w and *n.zustand != m)
  then *cell.zustand := element(zustaende,value(*cell.zustand)+4);
 marke_nach:
  if one(n in neighbours & m in marke_nach :
	 (*n.zustand in weg) and (*cell.zustand = m) )
  then *cell.zustand := element(zustaende,value(*cell.zustand)-8);
 loeschen:
  *cell := [frei,0];
end;  /* of case */
