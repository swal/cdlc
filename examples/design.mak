# Aufruf: xmake design.mak
#
# Erzeugt aus rand.xnf und der Zellbeschreibung logdes.xnf
# das nichtgeroutete design design.lca,
# das geroutete design designr.lca 
# sowie den raw bitstream designr.rbt
# rand.lca wird als guide fuer die Randzellen verwendet
#
# 
designr.rbt : designr.lca
	makebits -b designr.lca

designr.lca : design.map 
	map2lca -p3064PG132-100 design.map
	apr -w -y -g rand.lca design.lca designr.lca

design.map : design.xnf 
	xnfdrc -p3064PG132-100 design.xnf
	xnfmap -p3064PG132-100 design.xnf

design.xnf : logdes.xnf rand.xnf 
	xnfmerge -x -p3064PG132-100 rand.xnf design.xnf


