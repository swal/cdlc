###################################################
#
# This is the makefile for the CDLC compiler
#
# it has to be exectuted by _GNU-make_.  call: 
#     "gmake"  or  "make"
# for information.
#
###################################################
#
# $Id: makefile.%v 1.1 1994/06/13 21:04:13 waldsch Exp waldsch $
# $Log: makefile.%v $
# Revision 1.1  1994/06/13  21:04:13  waldsch
# Initial revision
#
# Revision 1.7  1994/05/28  18:47:02  waldsch
# make new
# make clobber erweitert
#
# Revision 1.6  1994/05/24  21:52:38  waldsch
# neu: trennung der Regeln nach DOS und UNIX
#
# Revision 1.5  1994/05/21  19:00:46  waldsch
# auto check out aus RCS
#
# Revision 1.4  1994/05/21  18:21:27  waldsch
# Das universelle makefile zum
# Zell-Compiler-Projekt
#
#
###################################################

MainVersion=v1.0

#
# High level targets are:
#

.PHONY : help
.PHONY : info

.PHONY : all
.PHONY : clean
.PHONY : clobber

.PHONY : error  
.PHONY : version

.PHONY : check
.PHONY : tar
.PHONY : dos
.PHONY : unix
.PHONY : depend
.PHONY : a
.PHONY : b


#
# default rule: info
#


help info:
	@echo "                                                                  "
	@echo " Makefile for $(PROJECT) under $(OperatingSystem)                 "
	@echo "                                                                  "
	@echo " Target rules:                                                    "
	@echo "                                                                  "
	@echo " help, info   : display this info                                 "
	@echo "                                                                  "
	@echo " new          : first installation or full remake                 "
	@echo " all          : use dependencies                                  "
	@echo "                                                                  "
	@echo " clean        : removes objects and executables                   "
	@echo " clobber      : removes everything but the source-files           "
	@echo "                                                                  "
	@echo " dos          : converts all sourcefiles to MS-DOS format         "
	@echo " unix         :                             UNIX format           "
	@echo " tar          : makes file 'cdlc.tar' with sources in UNIX format "
	@echo " check        : checks in the sources using RCS                   "
	@echo "                                                                  "
	@echo "                                                                  "
	@echo " usage: gmake [Target rule]                                       "
	@echo "                                                                  "


include makefile.cnf

error:
	@echo " An error occured ! 

all:
	$(MAKE) -C statmnt all
	$(MAKE) -C library all  
	$(MAKE) -C klassen all
	$(MAKE) -C express all
	$(MAKE) -C grammar all
	$(MAKE) -C main all
ifeq "$(OperatingSystem)" "MS-DOS"
	$(MAKE) cdlc.exe
else
	$(MAKE) cdlc
endif
	@echo "ALL is done now"

clean:
	- $(DeleteFile) cdlc
	- $(DeleteFile) cdlc.exe
	- $(DeleteFile) *.dcb
	- $(MAKE) -C library clean
	- $(MAKE) -C klassen clean
	- $(MAKE) -C express clean
	- $(MAKE) -C grammar clean
	- $(MAKE) -C main    clean
	- $(MAKE) -C statmnt clean
	
clobber: clean
	- $(DeleteFile) tmp.tmp
	- $(DeleteFile) *.o
	- $(DeleteFile) core
	- $(DeleteFile) a.out
	- $(DeleteFile) *.dcb
	- $(DeleteFile) *.bak
	- $(DeleteFile) *.tar
	- $(DeleteFile) *.taz
	- $(DeleteFile) *.tmp
	- $(DeleteFile) *.$$$$$$
	
check: clobber
	- for %i in (makefile.*) do ci -l %i
	- ci -l cdlc.lib
	$(MAKE) -C library check
	$(MAKE) -C klassen check
	$(MAKE) -C express check
	$(MAKE) -C grammar check
	$(MAKE) -C main check

dos:
	utod makefile
	utod makefile.cnf
	utod version.h
	utod cdlc.lib
	utod *.def
	$(MAKE) -C library dos
	$(MAKE) -C klassen dos
	$(MAKE) -C express dos
	$(MAKE) -C grammar dos
	$(MAKE) -C main dos
	$(MAKE) -C statmnt dos
	$(MAKE) -C doku dos

unix:
	dtou makefile
	dtou makefile.cnf
	dtou makefile.hp
	dtou version.h
	dtou cdlc.lib
	dtou *.def
	$(MAKE) -C library unix
	$(MAKE) -C klassen unix
	$(MAKE) -C express unix
	$(MAKE) -C grammar unix
	$(MAKE) -C main unix
	$(MAKE) -C statmnt unix
	$(MAKE) -C doku unix


# this will create a new version-label in file "version.h"
version:
	echo #define VERSION "$(MainVersion) \>version.h
ifeq "$(OperatingSystem)" "MS-DOS"
	version >> version.h
else
	date >> version.h
endif
	echo "  >> version.h

# this packs every thing into tar-file and zips this to "cldc.taz
tar:
ifeq "$(OperatingSystem)" "MS-DOS"
	$(MAKE) unix
endif
	tar -cf cdlc.tar cdlc.lib
	tar -rf cdlc.tar makefile.*
	tar -rf cdlc.tar version.h
	tar -rf cdlc.tar *.def
	$(MAKE) -C library tar
	$(MAKE) -C klassen tar
	$(MAKE) -C express tar
	$(MAKE) -C grammar tar
	$(MAKE) -C main tar
	$(MAKE) -C statmnt tar
	$(MAKE) -C examples tar
	$(MAKE) -C doku tar
	gzip -f cdlc.tar

touch:
	touch library/*.*
	touch klassen/*.*
	touch express/*.*
	touch grammar/*.*
	touch main/*.*
	touch statmnt/*.*


depend:
ifeq "$(OperatingSystem)" "MS-DOS"
	@echo "No makedepend available under MS-DOS"
else
	$(MAKE) -C library depend
	$(MAKE) -C klassen depend
	$(MAKE) -C express depend
	$(MAKE) -C grammar depend
	$(MAKE) -C main depend
	$(MAKE) -C statmnt depend
endif


#
# creating rules
#

# for MS-DOS:

ifeq "$(OperatingSystem)" "MS-DOS"

cdlc.exe:main/main.o grammar/libgram.a express/libexpr.a klassen/libclass.a \
	library/libbase.a main/error.o main/debug.o cdlc.lib statmnt/libstat.a \
	main/libmain.a
	$(CC) -o cdlc $(LDFLAGS) @cdlc.lib $(LOADLIBES)
	copy /b C:\DJGPP\BIN\GO32.EXE+cdlc cdlc.exe
else

#
# for UNIX or LINUX:
#

cdlc:main/main.o grammar/libgram.a statmnt/libstat.a express/libexpr.a klassen/libclass.a main/logic.o library/libbase.a main/error.o main/debug.o
	$(CXX) -o $@ $(LDFLAGS) $^ -lfl $(LOADLIBES)

endif

#
# Saving to disks under MS-DOS
#

#a: tar all
#       copy cdlc.taz       a:cdlc.taz
#       copy readme.        a:readme.
#       md   a:dosexec
#       cd   a:dosexec
#       copy cdlc.exe       a:cdlcexe.exe
#       copy cdlc.bat       a:cdlc.bat
#       copy split.exe      a:
#       copy xnf2rbt.bat    a:
#       copy examples\*.cdl a:
#       copy examples\*.dcf a:
#       copy examples\examples.cmd a:
#       copy *.def          a:
#       copy design.mak     a:
#       copy rand.lca       a:
#       copy rand.xnf       a:
#       utod examples.bat
#       copy examples.bat   a:


a: version all tar
	$(dosdisk)
	
b: version all tar
	$(dosdisk)

define dosdisk
	copy cdlc.taz       $@:cdlc.taz
	copy readme.        $@:readme.
	md   $@:dosexec
	cd   $@:dosexec
	copy cdlc.exe       $@:cdlcexe.exe
	copy cdlc.bat       $@:cdlc.bat
	copy split.exe      $@:
	copy xnf2rbt.bat    $@:
	copy examples\*.cdl $@:
	copy examples\*.dcf $@:
	copy examples\examples.cmd $@:
	copy doku\*.tx8 $@:
	copy *.def          $@:
	copy design.mak     $@:
	copy rand.lca       $@:
	copy rand.xnf       $@:
	utod examples.bat
	copy examples.bat   $@:
endef

