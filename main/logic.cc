/*
 * file logic.cc
 */

#include "logic.h"

#include "../main/main.h"
#include "../grammar/parser.h"
#include "../klassen/aconstnt.h"
#include "../klassen/atype.h"
#include "../klassen/atbool.h"
#include "../klassen/atrange.h"
#include "../klassen/atenum.h"
#include "../klassen/atrecord.h"
#include "../klassen/atunion.h"
#include "../library/listobj.h"


BString *binary(int dec,int size)
{
    BString * theString;
    theString=new BString("");
    if (dec<0)
    { dec= ( 1<<size ) + dec; }
    
    for ( int i=size ; i>0 ; i-- )
    {
        if ( 0!=( (1<<(i-1)) & dec) ) theString->append(new BString("1"));
        else                          theString->append(new BString("0"));
    }
    return theString;
}

int decimal(BString * bin)
{
    int sum=0;
    int val=1;
    
    for ( int i=strlen(bin->content())-1 ; i>=0 ; i-- )
    {
        if ( '1'==bin->content()[i] ) { sum=sum+val; }
        else { if ('0'!=bin->content()[i]) { error("int decimal(BString * bin)"); } }
        val=2*val;
    }

    return sum;
}

/*
 1  2  3  4  5  6  7  8  9 10 11 12 13 14 15 16 ...
 0  1  1  2  2  2  2  3  3  3  3  3  3  3  3  4 ...
*/

int logd(long int i)
{
    int ld;
    DEBUG_enter("logd()")
    if (i<1)
    {
        fatalError("fatal error: argument for logd() lower then 1");
    }
    ld=0;
    while  ((((long int)1)<<ld) <= i)
    {
        ld++;
        if (ld>300) fatalError("fatal error: logd() argument to big");
    }
    ld--;
    DEBUG_exit("logd")
    return ld;
}


void generateLocal(AVariable * v)
{
    DEBUG_enter("void generateLocal(AVariable * v)")

    fprintf(fout,"; %s:\n",BString::cast(v->getType()->className())->content() );
    
    if ((v->getType()->isKindOf( ATypeBool ::ClassName)) ||
        (v->getType()->isKindOf( ATypeRange::ClassName)) ||
        (v->getType()->isKindOf( ATypeUnion::ClassName)) ||
        (v->getType()->isKindOf( ATypeEnum ::ClassName))  )
    {
        fprintf(fout,"*LOCAL\n %s ;\n\n",v->getName()->content());
    }
    else if (v->getType()->isKindOf(ATypeRecord::ClassName))
    {
        char s[250];
        strcpy(s,v->getName()->content());
        s[strlen(s)-1]='\0';

        fprintf(fout,"*LOCAL\n %s ;\n\n",&s[1]);
    }
    else
    { error("generateLocal(): invalid data type");}     

    DEBUG_exit("void generateLocal(AVariable * v)")
}


AVariable * generateTempVariable(AType * type)
{
    static int num=1;
    char name[100];
    BString * bname;
    static AVariable * v = NULL;
    
    DEBUG_enter("AVariable * generateTempVariable(AType * type)")

    sprintf(name,"_%utmp",num);
    num++;
    bname=new BString(name);
    v=new AVariable(bname,type);

    generateLocal(v);
    
    DEBUG_exit("AVariable * generateTempVariable(AType * type)")
    return v;
}

/*
void assign(Object * a,Object * b)
{
    DEBUG_enter("assign")
    DEBUG_str(BString::cast(a->className())->content())
    DEBUG_str(BString::cast(b->className())->content())
    DEBUG_exit("assign")
}
*/
 
void writeInputTable(char * port)
{
    char name[250];
    AVariable * v;
    int code;
    AConstant * con;
    int ylength;
        
    DEBUG_enter("void writeInputTable(char * port)")

 
    strcpy(name,"_");
    strcat(name,port);

    v=new AVariable(new BString(name),celltype);
    generateLocal(v);
    
    if (
        (celltype->isKindOf(ATypeEnum::ClassName))||
        (celltype->isKindOf(ATypeBool::ClassName))||
        (celltype->isKindOf(ATypeRange::ClassName))
       )
    {
        fprintf(fout,"*BOOLEAN-EQUATIONS\n");
        fprintf(fout," %s = P%s[%u..0] ;\n\n",
            v->getName()->content(),
            port,
            logd(cellgroup->getSize()-1)
        );
    }
    else
    {
        if (
            (celltype->isKindOf(ATypeRecord::ClassName)) &&
            ( celltype->getNumberOfBits() <= 8 )
           )
        {
            /* use bitwise coding */
            fprintf(fout,"*BOOLEAN-EQUATIONS\n");
            fprintf(fout," %s = P%s[%u..0] ;\n\n",
                v->getName()->content(),
                port,
                celltype->getNumberOfBits()-1
            );
        }
        else
        {
            fprintf(fout,"*FUNCTION-TABLE\n");
            ylength=strlen(v->getName()->content());
            fprintf(fout,"$HEADER :\n X P%s[%u..0] : Y %s ; coding for input\n",
                port,
                logd(cellgroup->getSize()-1),
                v->getName()->content()
            );
            code=0;

            for ( Iterator i( cellgroup ) ; i ; i++ )
            {
                fprintf(fout," X %8uD : Y",code);
                con=AConstant::cast( ListObject::cast(i)->getObject() );
                fprintf(fout," %*s ;",ylength,celltype->codingOf(con)->content() ); 
                fprintf(fout," %s\n",con->asString()->content() );
                code++;
            }
            fprintf(fout," X     $REST : Y %*s ;\n\n",ylength,"-");

        }
    }
        
    DEBUG_exit("void writeInputTable(char * port)")
}


AVariable * writeOutputTable(char * port)
{
    char name[250];
    AVariable *v;
    int code;
    AConstant * con;
    int ylength;
    

    strcpy(name,"_");
    strcat(name,port);

    v=new AVariable(new BString(name),celltype);
    generateLocal(v);
     
    code=0;


    VERBOSE( printf("%u cell states\n",cellgroup->getSize()); )


    if (
        (celltype->isKindOf(ATypeEnum::ClassName))||
        (celltype->isKindOf(ATypeBool::ClassName))||
        (celltype->isKindOf(ATypeRange::ClassName))
       )
    {
        fprintf(fout,"*BOOLEAN-EQUATIONS\n");
        fprintf(fout," P%s[%u..0] = %s ;\n",
            port,
            logd(cellgroup->getSize()-1),
            v->getName()->content()
        );

        if (logd(cellgroup->getSize()-1)+1 <= 7)
        { 
            fprintf(fout," P%s[7..%u] = 0 ;\n\n",
                port,
                logd(cellgroup->getSize()-1)+1
            ); 
        }
    }
    else
    {
        if (
            (celltype->isKindOf(ATypeRecord::ClassName)) &&
            ( celltype->getNumberOfBits() <= 8 )
           )
        {
            /* use bitwise coding */
            fprintf(fout,"*BOOLEAN-EQUATIONS\n");
            fprintf(fout," P%s[%u..0] = %s ;\n",
                port,
                celltype->getNumberOfBits()-1,
                v->getName()->content()
            );
            
            if (celltype->getNumberOfBits() <= 7)
            { 
                fprintf(fout," P%s[7..%u] = 0 ;\n\n",
                    port,
                    celltype->getNumberOfBits()
                ); 
            }
        }
        else
        {

            fprintf(fout,"*FUNCTION-TABLE\n");
 
            ylength=strlen(v->getName()->content());
            fprintf(fout,"$HEADER :\n X %s : Y P%s[%u..0] ; coding for output\n",
                v->getName()->content(),
                port,
                logd(cellgroup->getSize()-1)
            );

            for ( Iterator i( cellgroup ) ; i ; i++ )
            {
                con=AConstant::cast( ListObject::cast(i)->getObject() );

                fprintf(fout," X %*s :",ylength,celltype->codingOf(con)->content() ); 
                fprintf(fout," Y %8uD ;",code);
                fprintf(fout," %s\n",con->asString()->content() );
                code++;
            }

            fprintf(fout," X%*s $REST : Y        -  ;\n\n",ylength-5," ");
       
            if ( logd(cellgroup->getSize()-1)+1 < 8 )
            fprintf(fout,"*BOOLEAN-EQUATIONS\n P%s[%u..7]= 0 ;\n\n",
                port,
                logd(cellgroup->getSize()-1)+1
            );
        }
    }

    return v;
}


AVariable * expand (AVariable * v, int toBits)
{
    AVariable * n;
    
    DEBUG_enter("AVariable * expand (AVariable * v, int toBits)")

    if (!v->getType()->isKindOf(ATypeRange::ClassName))
    { error("can't expand other than integer(sub)-Type"); }

    if (ATypeRange::cast(v->getType())->getLower()->value()<0)
    {
        /* v is signed */
/*        n=generateTempVariable(); */
        
    }
    else
    {
        /* v is unsigned */
/*        n=generateTempVariable(); */
    }
    DEBUG_exit("AVariable * expand (AVariable * v, int toBits)")
    return n;
}

