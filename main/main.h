#ifndef MAIN_H
#define MAIN_H

#include <stdio.h>
#include "../klassen/atype.h"
#include "../klassen/agroup.h"

extern AType  * celltype;
extern AGroup * cellgroup;
extern int dimension;
extern int distance;
extern int smallCoding;
extern int optimise;

extern FILE * fout;
extern void setObligValues(void);
extern void showIdentifier(void);


#endif
