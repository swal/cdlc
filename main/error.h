/* File: error.h */

/*
 * $Log: error.h%v $
# Revision 1.3  1994/05/28  18:42:19  waldsch
# no changes
#
# Revision 1.2  1994/05/24  21:46:44  waldsch
# neu: ???
#
 * Revision 1.1  1994/05/21  18:16:00  waldsch
 * Initial revision
 *
 */

/*
 * $Id: error.h%v 1.3 1994/05/28 18:42:19 waldsch Exp waldsch $
 */

#ifndef _error_h
#define _error_h

extern void  error    ( char* );   /* does _not_ return */
extern void  syserror ( char* );   /* does _not_ return */
extern void  warning  ( char* );


#endif /* error_h */
