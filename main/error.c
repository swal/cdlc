/* File error.c */

/*
 * $Log: error.c%v $
// Revision 1.3  1994/05/28  18:45:07  waldsch
// std.h wieder drin, hatte probleme auf den
// SUNs gemacht. notfalls wieder raus schmeissen
//
// Revision 1.2  1994/05/24  21:48:46  waldsch
//
 * Revision 1.1  1994/05/21  18:16:56  waldsch
 * Initial revision
 *
 */

/*
 * $Id: error.c%v 1.3 1994/05/28 18:45:07 waldsch Exp waldsch $
 */
 
#include <stdio.h>
#include <std.h>     /* does not exist on some SUNs (isasun22) */

#include "error.h"
#include "debug.h"

void error ( char * errMsg)
{

 /*
  * fflush(NULL);  do not do this. It will cause a core dump
  */

    printf("\n");
    fflush( stdout );
    fprintf ( stderr, errMsg );      
    exit ( 1 );
}

void syserror ( char * errMsg)
{
    printf("\n");
    fflush(stdout);
    perror ( errMsg ); /* also writes the message for errno */
    exit ( 1 );
}

void warning ( char * errMsg )
{
    if (verbose) fprintf( stderr,"\n");
    fflush(stdout);
    fprintf( stderr,"WARNING: ");
    fprintf( stderr,errMsg );
    fprintf( stderr,"\n" );
    fflush ( stderr );
}
