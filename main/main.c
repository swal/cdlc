
/*
 * $Log: main.c%v $
 * Revision 1.5  1994/05/28  18:42:53  waldsch
 * setzen der Variablen inputfilename
 *
 * Revision 1.4  1994/05/24  21:47:04  waldsch
 * neu: bearbeiteten Filenamen nicht anzeigen
 * (macht jetzt der Scanner selber)
 * Funktion parserinit ausgelagert nach parser
 *
 * Revision 1.3  1994/05/21  18:11:50  waldsch
 * debug, error und main.h neu
 *
 * Revision 1.2  1994/05/01  17:45:12  waldsch
 * now with log and id of RCS. Also of Parser
 *
 */
 
static char mainRCSID [] ="$Id: main.c%v 1.5 1994/05/28 18:42:53 waldsch Exp waldsch $";

#include "../version.h"

#include <stdio.h>
#include <stdlib.h> /* for getenv */
#include "error.h"
#include "debug.h"
#include "../grammar/parser.h"
#include "../grammar/scanner.h" /* main needs to set the input file yyin */
#include "../express/exrecord.h"
#include "../klassen/atype.h"
#include "../klassen/aconstnt.h"
#include "../klassen/acbool.h"
#include "../klassen/acint.h"
#include "../klassen/acrecord.h"
#include "../library/object.h"
#include "../library/iterator.h"
#include "../library/list.h"
#include "../library/bstring.h"
#include "../library/listobj.h"
#include "../library/pair.h"

#include "logic.h"

#ifdef MSDOS
#define CPP_COMMAND "cpp"
#else
#define CPP_COMMAND "/lib/cpp"
#endif

#define INPUTFILETYPE ".cdl"

AType  * celltype=NULL;
AGroup * cellgroup=NULL;
int dimension=0;
int distance=0;

int smallCoding;
int optimise;

FILE * fout;

void showIdentifier()
{
  printf("identifier dictionary:\n");
  for (Iterator i(identifier);i;i++)
  {
	Object * o;
	o=identifier->getValueAt(i);
	printf("%16s: %11s",
		   BString::cast(i)->content(),
		   BString::cast(o->className())->content()
		  );
	if (o->isKindOf(AConstant::ClassName))
	{
	  printf("= %s",AConstant::cast(o)->asString()->content());
	} else if (o->isKindOf(AGroup::ClassName))
	{
	  printf(":");
	  for (Iterator j(AGroup::cast(o));j;j++)
	  {
		printf(" %s",AConstant::cast(ListObject::cast(j)->getObject())->asString()->content() );
	  }
	}
	printf("\n");
  }
}


void setObligValues(void)
{
	if (NULL==celltype)
	{
		if (NULL==identifier->getValueAt( new BString("celltype") ))
		{ error("ERROR: no celltype declared"); }

		if  (
			! identifier->getValueAt( new BString("celltype") )->isKindOf(AType::ClassName)
			)
		{ error("identifier celltype isn't a type"); }

		celltype=AType::cast(identifier->getValueAt(new BString("celltype")));
	}

	if (NULL==cellgroup)
	{
		cellgroup=celltype->asGroup();
	}

	if (0==dimension)
	{
		if ( NULL==identifier->getValueAt(&BString("dimension")) )
		{ error("ERROR: no dimension declared"); }
	
		dimension=
			AConInt::cast(identifier->getValueAt(new BString("dimension")))
			->value();
	}

	if (0==distance)
	{
		if ( NULL==identifier->getValueAt(new BString("distance")) )
		{ error("ERROR: no distance defined"); }

		distance=
			AConInt::cast(identifier->getValueAt(new BString("distance")))
			->value();
	}
}



/*
 * The main procedure
 */

int main(int argc,char * argv[])
{
	char filenamehead [255];
	char filenametmp [255];
	char xcsFileName [255];
	char codingInFileName [255];
	int paramOK;         /* flag, if command line option is a valid one */
	int useCPP;
	List * include;      /* cpp -I directories */
	int cellCoding[256]; /* index= position in cellgroup */
	struct { unsigned char r , g, b; } colortable[256];
		


	BString * centername = new BString("_CN"); 

	printf("CDL-COMPILER  %s \n",VERSION);

	inputfilename[0]='\0';
	codingInFileName[0]='\0';

// set optional flags to default values
	
	verbose=0;          // verbose mode:    off
	useCPP=1;           // use cpp:         yes
	optimise=1;         // optimise output: yes
	
	include=new List();
	strcpy(codingInFileName,"");

// examine command line options:
	
	for (int i=1; i<argc ; i++)
	{
		paramOK=0;

// echo help text

		if (0==strcmp(argv[i],"-h"))
		{
			printf("\nHelp for CDLC\n");
			printf("This compiler translates cdl-program files to dcb-files.\n");
			printf("Valid command line options are:\n");
			printf(" -v[+|-]       verbose mode\n");
			printf(" -o[+|-]       optimise mode\n");
			printf(" -nocpp -cpp   (don't) use the C preprocessor (-cpp is default)\n");
			printf(" -h            display this text\n");
			printf("enter filename without '.cdl'\n");
			paramOK=1;
		}

// switch for verbose mode: verbose
		
		if (0==strcmp(argv[i],"-v"))
		{ verbose=1; paramOK=1; }
		if (0==strcmp(argv[i],"-v+"))
		{ verbose=1; paramOK=1; }
		if (0==strcmp(argv[i],"-v-"))
		{ verbose=0; paramOK=1; }

// switch for optimise mode: optimise

		if (0==strcmp(argv[i],"-o"))
		{ optimise=1; paramOK=1; }
		if (0==strcmp(argv[i],"-o+"))
		{ optimise=1; paramOK=1; }
		if (0==strcmp(argv[i],"-o-"))
		{ optimise=0; paramOK=1; }

// switch for cpp

		if (0==strcmp(argv[i],"-cpp"))
		{ useCPP=1;  paramOK=1; }
		if (0==strcmp(argv[i],"-nocpp"))
		{ useCPP=0;  paramOK=1; }

// cdl file name

		if (argv[i][0]!='-')
		{
			strcpy(inputfilename,argv[i]);
			strcpy(filenamehead,inputfilename);
			for ( int j=strlen(inputfilename)-1 ; ((j>=0)&&('.'!=inputfilename[j])) ; j-- ) {}
			if ((strlen(inputfilename)-j)<=4) {filenamehead[j]='\0';}
			paramOK=1;
		}

// cpp include directories

		if (0==strcmp(argv[i],"-I"))
		{
			i++;
			if (i>=argc) error("no path after -I");
			include->add(new ListObject(new BString(argv[i])));
			paramOK=1;
		}

// coding definition file

		if (0==strcmp(argv[i],"-c"))
		{
			i++;
			if (i>=argc) error("no file name after -c");
			strcpy(codingInFileName,argv[i]);
			paramOK=1;
		}

// parameter was recogniced
		
		if (!paramOK)
		{ printf("Unknown command line option %s ignored.\n For help use '-h'",argv[i]); }
		
	}   /* command line parameter loop */

	VERBOSE
	( 
		printf("\n        Version:%s\n",mainRCSID);   
		printf("Grammar Version:%s\nScanner Version:%s\n\n",
								parserRCSID,        scannerRCSID ); 
	)

// no inputfile specified?

	if (0==strlen(inputfilename))
	{
		error("no input file name\n\tusage:  cdlc [options] filename\n\thelp :  cdlc -h\n");
	}

// does the inputfile exist?

	{
		char tfilename[255];
		
		/* first try with .cdl appended */
		
		strcpy(tfilename,inputfilename);
		strcat(tfilename,INPUTFILETYPE);

		yyin=fopen(tfilename,"r");
		if (NULL!=yyin)
		{ strcpy(inputfilename,tfilename); }
		else
		{
			char noFile[80];

			strcpy(tfilename,inputfilename);
			yyin=fopen(tfilename,"r");
			if (NULL==yyin)
			{            
				sprintf(noFile,"can not open input file '%s' ",inputfilename);
				syserror(noFile);
			}
		}
		fclose(yyin);
	}

// call cpp

	if (useCPP)
	{
		char command[255];
	
		sprintf(command,"%s %s",
			CPP_COMMAND,
			inputfilename
		);

		for ( Iterator incIter(include) ; incIter ; incIter++ )
		{
			strcat(command," -I");
			strcat(command,
				BString::cast(ListObject::cast(incIter)->getObject())->content()
			);
		}

		strcat(command," > cdlc.tmp");
	
		VERBOSE( printf("%s\n",command); )

		if (0!=system(command))
		{
			printf("\nerror at execute of cpp - trying without!\n");
		}
		else
		{
			VERBOSE( printf("cpp ok.\n"); )
			strcpy(inputfilename,"cdlc.tmp");
		}
	}

// open input file 
	
	DEBUG_code( printf("\nReading from file: %s\n",inputfilename); )

	yyin=fopen(inputfilename,"r");
	if (yyin==NULL)
	{
		printf("\ncan't open file: %s\n",inputfilename);
		return(-1);
	}

// flex scanner - bison parser 
	
	parserinit();

	if ( 0!=yyparse() )    // start bison/yacc , flex/lex
	{
		printf("\nERROR: Parsing aborded\n");
		fclose(yyin);
		return(-1);
	}

	VERBOSE( printf("parsing successful\n"); )
	
// close input file
	
	fclose(yyin);

	VERBOSE( showIdentifier(); )
	
// do semantic checks 

	VERBOSE( printf("calc celltype\n"); )

	setObligValues();   // celltype, dimension, distances

// calc method of celltype coding

	VERBOSE( printf("validate celltype\n"); )

	if ( cellgroup->getSize()>256 )
	{ error("ERROR: more than 256 cell states required"); }

	if ( 0!=strcmp(codingInFileName,"") )
	{
		smallCoding=1;
	}
	else
	{
		if ( celltype->getNumberOfBits()<=8 )
		{
			smallCoding=0;
		}
		else
		{
			smallCoding=1;
		}
	}

	VERBOSE( 
		if (smallCoding) 
		{ 
			printf("..using small cell coding. %u states\n",
			cellgroup->getSize());
		}
		else 
		{ 
			printf("..using bit group cell coding. %u bits\n",
			celltype->getNumberOfBits()); 
		}
	)

// write xcs-File

	VERBOSE( printf("write xcs-file\n"); )

	{
		FILE * xcs;
		strcpy(xcsFileName,filenamehead);
		strcat(xcsFileName,".xcs");

		xcs=fopen(xcsFileName,"wb");
		if (NULL==xcs)
		{
			printf("unable to open .xcs output file %s\n",xcsFileName);
			return(-1);
		}

		fprintf(xcs,"dimension %u\n",
			AConInt::cast(identifier->getValueAt(&BString("dimension")))->value()
		);

		fprintf(xcs,"distance %u\n",
			AConInt::cast(identifier->getValueAt(&BString("distance")))->value()
		);

		fprintf(xcs,"celltype");

		(void) celltype->writeXCS(xcs,"",1);

		fclose(xcs);
	}

// calc cell coding

	VERBOSE( printf("calc cell coding\n"); )

	{
		int pos;
		int index;

		pos=0;

		for ( Iterator i(cellgroup) ; i ; i++ )
		{
			index=1;

			cellCoding[pos]=
				celltype->positionOfConstant(
					AConstant::cast(ListObject::cast(i)->getObject()),
					&index
				);

			pos++;
		}
	}

// calc colors

	VERBOSE( 
		printf("calc colors\n");
		printf("\n %25s | %10s | %15s\n",
			"Cell state",
			"Coding",
			"Color rgb"
		);
	)
	{        
		AConstant * cell;
		unsigned char coding;


		DEBUG_enter("create color table")


		{
			int i;
			for (i=0;i<=255;i++)
			{
				colortable[i].r=0;
				colortable[i].g=0;
				colortable[i].b=0;
			}
		}

		coding=0;

		for ( Iterator i( cellgroup ) ; i ; i++ )
		{
			int colorFound=0;
			
			DEBUG_enter("try cell constant")   

			cell=AConstant::cast( ListObject::cast(i)->getObject() );
			identifier->addAssoc(centername,cell);
		
		
			for ( Iterator j( color ) ; j && (!colorFound) ; j++ ) 
			{
				AConstant * condition;

				DEBUG_enter("try condition")   

				condition=
					Expression::cast( 
						Pair::cast( 
							ListObject::cast( j ) ->getObject() 
						)->getRight() 
					)->asConstant();
				
				if (NULL==condition) 
				{ 
					semerror(NULL,"can't evaluate color condition"); 
					return 0;
				}
				else
				{
					if (!condition->isKindOf(AConBool::ClassName))
					{ 
						semerror(NULL,"color condition isn't a boolean expression"); 
						return 0;
					}
					else
					{
						if ( AConBool::cast(condition)->value() )
						{
							AConRecord * colRecord;

							DEBUG_enter("setting color value")
							
							DEBUG_int(coding)

							colRecord=
								AConRecord::cast(
									Expression::cast(
										Pair::cast(
											ListObject::cast(j)->getObject()
										)->getLeft()
									)->asConstant()
								);

							DEBUG_msg("color record is evaluated")

							colortable[cellCoding[coding]].r=
								AConInt::cast(
									ListObject::cast(
										colRecord->at(1)
									)->getObject()
								)->value();

							DEBUG_int(colortable[cellCoding[coding]].r)

							colortable[cellCoding[coding]].g=
								AConInt::cast(
									ListObject::cast(
										colRecord->at(2)
									)->getObject()
								)->value();
							
							DEBUG_int(colortable[cellCoding[coding]].g)


							colortable[cellCoding[coding]].b=
								AConInt::cast(
									ListObject::cast(
											colRecord->at(3)
									)->getObject()
								)->value();

							DEBUG_int(colortable[cellCoding[coding]].b)


							DEBUG_exit("setting color value")
							
							VERBOSE
							(
								printf(
									" %25s | %10s | %15s\n",
									cell->asString()->content(),
									binary(cellCoding[coding],8)->content(),
									colRecord->asString()->content()
								);
							)
							colorFound=1;
						}
					}
				}
				DEBUG_exit("try condition")
			}
			DEBUG_exit("try cell constant") 
			if (1!=colorFound)
			{
				VERBOSE
				(
					printf(
						" %25s | %10s | %15s\n",
						cell->asString()->content(),
						binary(cellCoding[coding],8)->content(),
						"000000000000000000000000 (default)"
					);
				)                    
			}
			coding++;
  
		}
		
		VERBOSE(
			printf("writing color table\n");
		)
		
		{
			FILE * fout;
			int i;
			strcpy(filenametmp,filenamehead);
			strcat(filenametmp,".clt");

			fout=fopen(filenametmp,"wb");
			if (NULL==fout) 
			{
				printf("unable to open .clt output file %s\n",filenametmp);
				return(-1);
			}
			free(filenametmp);

			for (i=0;i<=255;i++)
			{
				fputc(colortable[i].r,fout);
				fputc(colortable[i].g,fout);
				fputc(colortable[i].b,fout);
			}
		}

		DEBUG_exit("create color table")
	} 

	/* end of color part */


	/*
	 * do the code generation for LOC/iC now
	 *
	 */

	VERBOSE(
		printf("start code generation\n");
	) 

	strcpy(filenametmp,filenamehead);
	strcat(filenametmp,".dcb");

	fout=fopen(filenametmp,"w");
	if (fout==NULL) 
	{
		printf("\n unable to open output file %s\n",filenametmp);
		return(-1);
	}
	free(filenametmp);
  
	fprintf(fout,"*IDENTIFICATION\n");
	fprintf(fout," translated by CDLC (%s) from:\n %s\n\n",VERSION,inputfilename);
  
	fprintf(fout,"*X-NAMES\n");
	fprintf(fout," PNO[0..7],PWE[0..7],PCN[0..7],PEA[0..7],PSO[0..7],\n");
	fprintf(fout," PSW[0..7],PSE[0..7],PNE[0..7],PNW[0..7],\n");
	fprintf(fout," PRO,PRU,PRR,PRL,\n");
	fprintf(fout," CLK_OUT;\n\n");

	fprintf(fout,"*Y-NAMES\n");
	fprintf(fout," PRS[0..7],PTP[0..7];\n\n");
	
	fprintf(fout,"*BOOLEAN-EQUATIONS\n");
	fprintf(fout," PTP[0..7] = - ;\n\n");

	writeInputTable("NO");
	writeInputTable("WE");
	writeInputTable("CN");
	writeInputTable("EA");
	writeInputTable("SO");
	writeInputTable("SW");
	writeInputTable("SE");
	writeInputTable("NE");
	writeInputTable("NW");

	AVariable * outVar;
	
	outVar=writeOutputTable("RS");
	
	fprintf(fout,"\n");
	
	DEBUG_msg("Interface tables are written")

// constant signals are necessary to build records with bits (like extended numbers)  [_0,_0,sum[3..0]]
// this is not very nice but [0,0,i] does not work with LOG/iC (ATTENTION: no warning in this case)

	fprintf(fout,"\n; *** define some usefull constants ***\n\n");
	fprintf(fout,"*LOCAL\n _0,_1;\n\n");
	fprintf(fout,"*BOOLEAN-EQUATIONS\n _0=0;\n _1=1;\n\n");
	
	fprintf(fout,"\n; *** by CDLC translated program starts here ***\n\n");
	fflush(fout);
	

// convert all cdl variables to *LOCAL signals and set to undefined
	
	
	for (Iterator var(identifier) ; var ; var++)
	{
		if (identifier->getValueAt(var)->isKindOf(AVariable::ClassName))
		{
			AVariable * theVariable=
				AVariable::cast(identifier->getValueAt(var));

			generateLocal(theVariable);
			fprintf(fout,
				"; set variable to undefined:\n*BOOLEAN-EQUATIONS\n %s = - ;\n\n",
				theVariable->getName()->content()
			);
		}
	}

//  create result var

	AVariable *rsVar, *cVar;

	rsVar=new AVariable(
		new BString("_OUT"),
		celltype
		);
	cVar =new AVariable(
		centername,         
		celltype
		);

	identifier->addAssoc(new BString("_OUT"),rsVar);
	identifier->addAssoc(centername,cVar);

	generateLocal(rsVar);

// assign old content to new result 

	fprintf(fout,"; default: do not change\n*BOOLEAN-EQUATIONS\n %s = %s ;\n\n",
		rsVar->getName()->content(),
		cVar ->getName()->content()
	);


// generate code:
	
	/* gRule ist ein Statement aus parser.y */
	gRule->logic((new AConBool(1))->logic());

// finish
	
	fprintf(fout,"\n; *** CDLC - postamble ***\n\n");

	fprintf(fout,"*BOOLEAN-EQUATIONS\n %s = %s ;\n\n",
		outVar->getName()->content(),
		rsVar->getName()->content()
	);

	fprintf(fout,"*END\n\n");
	fclose(fout);

	VERBOSE( printf("ready: %s\n",inputfilename); )
	return( 0 );
}

