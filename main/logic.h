#ifndef LOGIC_H
#define LOGIC_H

#include "../klassen/avariabl.h"
#include "../klassen/atype.h"
#include "../library/bstring.h"

extern void        generateLocal(AVariable * v);
extern AVariable * generateTempVariable(AType *);

                                // converts 'dec' to a BString of {0,1} of length 'size'
extern BString *   binary(int dec,int size);

                                // converts the binary number 'bin' to a decimal number
extern int         decimal(BString * bin);

                                // returns number of bits necessary to code 'i'>0
extern int logd(long int i);

                                // write in-/out-put coding tables
extern void        writeInputTable(char * port);
extern AVariable * writeOutputTable(char * port);

extern AVariable * expand (AVariable * v, int toBits); 

const maxLineLength=75;  // max number of characters per line in .dcb-file

#endif
