/* File debug.c */

/*
 * $Log$
 *
 */

/*
 * $Id$
 */

/*
 * c-file, used by debug.h, a general purpose debuging facility
 */

#include <stdio.h>

#include "debug.h"

int verbose=0;

int DEBUG_indent=1;
int DEBUG_level =0;

void debug_printmem( char *desc, char *str, char *lenname, int len )
{
	int i;

	for ( i=0 ; i<len ; i++ )
	{
		if ( 32 <= (unsigned char) str[i] && (unsigned char) str[i] <= 127 )
		{ printf( "%c", str[i] ); }
		else
		{ printf( "." ); }
	}
	printf( "' ( %s = %d )\n", lenname, len );
}
