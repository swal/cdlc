/* File debug.h */

/*
 * $Log: debug.h%v $
 *
 * Revision 1.2  1994/05/24  21:44:24  waldsch
 * neu: DEBUG_code
 *
 * Revision 1.1  1994/05/21  18:13:20  waldsch
 * Initial revision
 *
 */

/*
 * $Id: debug.h%v 1.3 1994/05/28 18:40:30 waldsch Exp waldsch $
 */

/*
 * debuging facility:
 *
 * - use defined functions
 * - " #define DEBUG "  will add them to the program
 * - " set DEBUG=n in environment "  will activate them
 *     n=0 : no DEBUG_enter / DEBUG_exit
 *     n>0 : depth of DEBUG_enter / DEBUG_exit
 *
 */

#ifndef DEBUG_H
#define DEBUG_H

extern int verbose;

#define VERBOSE(c)\
		{ if (verbose) {c fflush(stdout);} }


#ifdef DEBUG


#include <stdio.h>
#include <stdlib.h>  /* braucht man auf der Sun, macht manchmal Probleme */

extern int DEBUG_indent;
extern int DEBUG_level;

extern void debug_printmem( char *desc, char *str, char *lenname, int len );


#define DEBUG_indent_step 1

/* Schachtelung der Ausgabe */

#define DEBUG_enter(s)  \
		if (getenv("DEBUG")) { \
		if (atoi(getenv("DEBUG"))>DEBUG_level) \
		{printf("%*s>%s\n",DEBUG_indent,"",#s); \
		fflush(stdout); \
		DEBUG_indent+=DEBUG_indent_step;}DEBUG_level++;}
							  
#define DEBUG_exit(s)   \
		if(getenv("DEBUG")) { \
		DEBUG_level--; \
		if (atoi(getenv("DEBUG"))>DEBUG_level) { \
		DEBUG_indent-=DEBUG_indent_step; \
		printf("%*s<%s\n",DEBUG_indent,"",#s);\
		fflush(stdout); } }
							  


/* Ausgabe von Werten verschiedenen Typs        */
/*  (mit Ausgabe der Variablen bzw. des Ausdrucks)  */


#define DEBUG_long(l)   \
		if(getenv("DEBUG")) \
		{printf("%*s%s = %ld = 0x%lX \n", DEBUG_indent, "", #l, l, l ); fflush(stdout); }
	
#define DEBUG_ptr(p)   \
		if(getenv("DEBUG")) \
		{printf("%*s%s = %p = 0x%pX \n", DEBUG_indent, "", #p, p, p ); fflush(stdout); }
	
#define DEBUG_int(i)    \
		if(getenv("DEBUG")) \
		{printf("%*s%s = %d = 0x%X \n", DEBUG_indent, "", #i, i, i ); fflush(stdout);}
	
#define DEBUG_double(d) \
		if(getenv("DEBUG")) \
		{printf("%*s%s = %20.10g \n", DEBUG_indent, "", #d, d ); fflush(stdout);}
	
#define DEBUG_char(c)   \
		if(getenv("DEBUG")) \
		{printf("%*s%s = %d = 0x%X = '%c' \n", DEBUG_indent, "", #c, c, c, c ); fflush(stdout);}
	
#define DEBUG_str(s)    \
		if(getenv("DEBUG")) \
		{printf("%*s%s = \"%s\"\n", DEBUG_indent, "", #s, s ); fflush(stdout);}

	
#define DEBUG_mem(str,len)  \
		if(getenv("DEBUG")) \
		{printf("%*s%s = 0x%lx -> '", DEBUG_indent, "", #str, (long)str ); \
		  debug_printmem( #str, str, #len, len );  fflush(stdout);}



#define DEBUG_msg(p)    \
		if(getenv("DEBUG")) \
		{printf("%*s%s:\n", DEBUG_indent, "", #p ); fflush(stdout);}

#define DEBUG_code(c) \
		if (getenv("DEBUG")) { c  fflush(stdout); }
	

#else /* !DEBUG */


#define DEBUG_enter(s)  
#define DEBUG_exit(s)   
#define DEBUG_long(l)   
#define DEBUG_ptr(p)
#define DEBUG_int(i)    
#define DEBUG_double(d) 
#define DEBUG_char(c)   
#define DEBUG_str(s)    
#define DEBUG_mem(str,len)  
#define DEBUG_msg(p)    
#define DEBUG_code(c) 


#endif /* define DEBUG */

#endif /* DEBUG_H */
